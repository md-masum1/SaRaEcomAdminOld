﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class AuthenticationDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        public async Task<AuthModel> Login(string employeeId, string employeePassword)
        {
            AuthModel model = new AuthModel();
            OracleCommand objOracleCommand = new OracleCommand("pro_check_valid_user")
            {
                CommandType = CommandType.StoredProcedure
            };


            objOracleCommand.Parameters.Add("p_employee_id", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(employeeId) ? employeeId : null;
            objOracleCommand.Parameters.Add("p_employee_password", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(employeePassword) ? employeePassword : null;

            objOracleCommand.Parameters.Add("p_employee_name", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("p_employee_email", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("p_employee_role", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Int32).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Int32).Direction = ParameterDirection.Output;


            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    model.EmployeeId = objOracleCommand.Parameters["p_employee_id"].Value.ToString();
                    model.EmployeeName = objOracleCommand.Parameters["p_employee_name"].Value.ToString();
                    model.EmployeeEmail = objOracleCommand.Parameters["p_employee_email"].Value.ToString();
                    model.EmployeeRole = objOracleCommand.Parameters["p_employee_role"].Value.ToString();

                    model.HeadOfficeId = objOracleCommand.Parameters["p_head_office_id"].Value.ToString();
                    model.BranchOfficeId = objOracleCommand.Parameters["p_branch_office_id"].Value.ToString();

                    var strMsg = objOracleCommand.Parameters["p_message"].Value.ToString();

                    model.Message = strMsg == "TRUE";
                }

                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {

                    strConn.Close();
                }

            }
            return model;
        }

        public async Task<AuthModel> GetUser(string employeeId)
        {
            const string sql =  "SELECT " +
                                "EMPLOYEE_ID," +
                                "EMPLOYEE_NAME," +
                                "EMPLOYEE_EMAIL," +
                                "EMPLOYEE_PHONE," +
                                "EMPLOYEE_ROLE," +
                                "EMPLOYEE_DESIGNATION," +
                                "EMPLOYEE_IMAGE," +
                                "EMPLOYEE_AREA," +
                                "ACTIVE_YN," +
                                "HEAD_OFFICE_ID," +
                                "BRANCH_OFFICE_ID " +
                                "FROM VEW_USER_INFO WHERE EMPLOYEE_ID = :EMPLOYEE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":EMPLOYEE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = employeeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        AuthModel model = new AuthModel();
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                model.EmployeeName = objDataReader["EMPLOYEE_NAME"].ToString();
                                model.EmployeeId = objDataReader["EMPLOYEE_ID"].ToString();
                                model.EmployeeDesignation = objDataReader["EMPLOYEE_DESIGNATION"].ToString();
                                model.EmployeeEmail = objDataReader["EMPLOYEE_EMAIL"].ToString();
                                model.EmployeePhone = objDataReader["EMPLOYEE_PHONE"].ToString();
                                model.EmployeeArea = objDataReader["EMPLOYEE_AREA"].ToString();

                                model.EmployeeRole = objDataReader["EMPLOYEE_ROLE"].ToString();
                                model.EmployeeImage = objDataReader["EMPLOYEE_IMAGE"].ToString();

                                model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                            }

                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<TopReview>> GeTopReview()
        {
            const string sql =  "SELECT " +
                                "CUSTOMER_NAME," +
                                "SHORT_MESSAGE," +
                                "REVIEW_SCORE," +
                                "NOTIFICATION_DATE " +
                                "FROM VEW_REVIEW_NOTIFICATION ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<TopReview> objTopReview = new List<TopReview>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                TopReview model = new TopReview();

                                model.Name = objDataReader["CUSTOMER_NAME"].ToString();
                                model.Message = objDataReader["SHORT_MESSAGE"].ToString();
                                model.Message = model.Message.Substring(0, Math.Min(model.Message.Length, 20));
                                model.Score = objDataReader["REVIEW_SCORE"].ToString();
                                model.Time = Convert.ToDateTime(objDataReader["NOTIFICATION_DATE"].ToString());
                                TimeSpan dateTime = DateTime.Now - model.Time;
                                var days = dateTime.Days;
                                var hour = dateTime.Hours;
                                var min = dateTime.Minutes;

                                if (days > 0)
                                {
                                    model.TimeAlert = Convert.ToString(days) + " days";
                                }
                                else if(hour > 0)
                                {
                                    model.TimeAlert = Convert.ToString(hour) + " hours";
                                }
                                else if(min > 0)
                                {
                                    model.TimeAlert = Convert.ToString(min) + " mins";
                                }
                                else
                                {
                                    model.TimeAlert = "just now";
                                }

                                objTopReview.Add(model);
                            }
                            return objTopReview;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<TopMessage>> GeTopMessage()
        {
            const string sql = "SELECT " +
                                "CUSTOMER_NAME," +
                                "SHORT_MESSAGE," +
                                "NOTIFICATION_DATE " +
                                "FROM VEW_CONTACT_NOTIFICATION ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<TopMessage> objTopMessage = new List<TopMessage>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                TopMessage model = new TopMessage();

                                model.Name = objDataReader["CUSTOMER_NAME"].ToString();
                                model.Message = objDataReader["SHORT_MESSAGE"].ToString();
                                model.Message = model.Message.Substring(0, Math.Min(model.Message.Length, 20));
                                model.Time = Convert.ToDateTime(objDataReader["NOTIFICATION_DATE"].ToString());
                                TimeSpan dateTime = DateTime.Now - model.Time;
                                var days = dateTime.Days;
                                var hour = dateTime.Hours;
                                var min = dateTime.Minutes;

                                if (days > 0)
                                {
                                    model.TimeAlert = Convert.ToString(days) + " days";
                                }
                                else if (hour > 0)
                                {
                                    model.TimeAlert = Convert.ToString(hour) + " hours";
                                }
                                else if (min > 0)
                                {
                                    model.TimeAlert = Convert.ToString(min) + " mins";
                                }
                                else
                                {
                                    model.TimeAlert = "just now";
                                }

                                objTopMessage.Add(model);
                            }
                            return objTopMessage;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> EditOrUpdateUserInfo(AuthModel objAuthModel)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ADMIN_INFO_EDIT")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_EMPLOYEE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;

            objOracleCommand.Parameters.Add("P_EMPLOYEE_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeName) ? objAuthModel.EmployeeName : null;

            objOracleCommand.Parameters.Add("P_EMPLOYEE_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeEmail) ? objAuthModel.EmployeeEmail : null;
            objOracleCommand.Parameters.Add("P_EMPLOYEE_PHONE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeePhone) ? objAuthModel.EmployeePhone : null;
            objOracleCommand.Parameters.Add("P_EMPLOYEE_DESIGNATION", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeDesignation) ? objAuthModel.EmployeeDesignation : null;
            objOracleCommand.Parameters.Add("P_EMPLOYEE_AREA", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeArea) ? objAuthModel.EmployeeArea : null;


            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.HeadOfficeId) ? objAuthModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.BranchOfficeId) ? objAuthModel.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> EditOrUpdateUserImage(AuthModel objAuthModel)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ADMIN_IMAGE_EDIT")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_EMPLOYEE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;

            objOracleCommand.Parameters.Add("P_EMPLOYEE_IMAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeImage) ? objAuthModel.EmployeeImage : null;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.HeadOfficeId) ? objAuthModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.BranchOfficeId) ? objAuthModel.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> EditOrUpdateUserPassword(AuthModel objAuthModel)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ADMIN_PASSWORD_CHANGE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_EMPLOYEE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;

            objOracleCommand.Parameters.Add("P_EMPLOYEE_PASSWORD", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeePassword) ? objAuthModel.EmployeePassword : null;
            objOracleCommand.Parameters.Add("P_EMPLOYEE_NEW_PASSWORD", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.NewPassword) ? objAuthModel.NewPassword : null;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.EmployeeId) ? objAuthModel.EmployeeId : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.HeadOfficeId) ? objAuthModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objAuthModel.BranchOfficeId) ? objAuthModel.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }
    }
}
﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.Controllers
{
    public class AuthController : Controller
    {
        readonly AuthenticationDal _authentication = new AuthenticationDal();

        public ActionResult Index(bool? userExist)
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee != null)
            {
                return RedirectToAction("Index", "Home");
            }

            var b = !userExist;
            if (b != null && (bool) b)
            {
                ViewBag.Message = "WRONG INFORMATION, PLEASE TRY AGAIN !!";
            }
            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AuthModel authModel)
        {
            if (!string.IsNullOrWhiteSpace(authModel.EmployeeId) && !string.IsNullOrWhiteSpace(authModel.EmployeePassword))
            {
                authModel = await _authentication.Login(authModel.EmployeeId, authModel.EmployeePassword);

                if (authModel.Message)
                {
                    authModel = await _authentication.GetUser(authModel.EmployeeId);

                    Session["authentication"] = authModel;

                    return RedirectToAction("Index", "Home");
                }
                return RedirectToAction("Index", new { userExist = false });
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotePassword(AuthModel authModel)
        {
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> LoadTopMenue()
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee != null)
            {
                employee.TopReviews = await _authentication.GeTopReview();
                employee.TopMessages = await _authentication.GeTopMessage();

                return PartialView("_navbarPartial", employee);
            }
            return null;
        }

        public ActionResult UserProfile()
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee == null)
            {

                return RedirectToAction("Index", "Auth");
            }
            return View(employee);
        }

        public ActionResult EditUserAccount()
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee == null)
            {
                return RedirectToAction("Index", "Auth");
            }

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditOrUpdateUserInfo(AuthModel objAuthModel)
        {
            ModelState.Remove("EmployeePassword");
            ModelState.Remove("NewPassword");
            if (ModelState.IsValid)
            {
                var strMessage = await _authentication.EditOrUpdateUserInfo(objAuthModel);
                TempData["message"] = strMessage;

                if (strMessage == "UPDATED SUCCESSFULLY !!")
                {
                    objAuthModel = await _authentication.GetUser(objAuthModel.EmployeeId);

                    Session["authentication"] = objAuthModel;
                }
            }

            return RedirectToAction("EditUserAccount", "Auth");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditOrUpdateUserImage(AuthModel objAuthModel)
        {
            if (objAuthModel.Image != null)
            {
                string filePath = Server.MapPath("~/Files/EmployeeImage/");
                string vFileName = Guid.NewGuid().ToString();



                var imageExtension = Path.GetExtension(objAuthModel.Image.FileName);
                if (imageExtension != null)
                {
                    imageExtension = imageExtension.ToUpper();

                    if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                    {
                        if (objAuthModel.EmployeeId != null)
                        {
                            string imagePath = filePath + objAuthModel.EmployeeImage;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }
                        }
                        filePath += vFileName;
                        filePath += imageExtension.ToLower();

                        WebImage img = new WebImage(objAuthModel.Image.InputStream);
                        img.Resize(750, 750);
                        img.Save(filePath);

                        objAuthModel.EmployeeImage = vFileName + imageExtension.ToLower();
                    }
                }


                var strMessage = await _authentication.EditOrUpdateUserImage(objAuthModel);
                TempData["message"] = strMessage;

                if (strMessage == "UPDATED SUCCESSFULLY !!")
                {
                    objAuthModel = await _authentication.GetUser(objAuthModel.EmployeeId);

                    Session["authentication"] = objAuthModel;
                }
            }

            return RedirectToAction("EditUserAccount", "Auth");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditOrUpdateUserPassword(AuthModel objAuthModel)
        {
            if (!string.IsNullOrWhiteSpace(objAuthModel.EmployeePassword) && !string.IsNullOrWhiteSpace(objAuthModel.NewPassword))
            {
                var strMessage = await _authentication.EditOrUpdateUserPassword(objAuthModel);
                TempData["message"] = strMessage;
            }

            return RedirectToAction("EditUserAccount", "Auth");
        }

        public ActionResult RedirectActionResult()
        {
            Response.Redirect("~/Auth/Index");
            return null;
        }

        public ActionResult LogOut()
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee != null)
            {
                Session.Abandon();
            }
            return RedirectToAction("Index");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.DAL
{
    public class ProductInventoryDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        //Get List Data
        public IEnumerable<ProductGrid> GetProductGrids(ProductGrid objProductGrid)
        {
            

            var sql = "SELECT " +
                        "ProductId," +
                        "ProductName," +
                        "Sku," +
                        "CategoryName," +
                        "SubCategoryName," +
                        "SubSubCategoryName," +
                        "UnitName," +
                        "FabricName," +
                        "PurchasePrice," +
                        "SellPrice," +
                        "ProductImage," +
                        "ProductImageUrl," +
                        //"QUANTITY," + quantity goes here
                        "ACTIVE_YN," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_PRODUCT_GRID_INFO WHERE 1=1 ";


            if (!string.IsNullOrWhiteSpace(objProductGrid.SearchBy))
            {
                sql += "and ( (lower(ProductName) like lower(:SearchBy)  or upper(ProductName)like upper(:SearchBy) )" +
                          "or (lower(Sku) like lower(:SearchBy)  or upper(Sku)like upper(:SearchBy) )" +
                          "or (lower(PurchasePrice) like lower(:SearchBy)  or upper(PurchasePrice)like upper(:SearchBy) )" +
                          "or (lower(SellPrice) like lower(:SearchBy)  or upper(SellPrice)like upper(:SearchBy) )" +
                          "or (lower(CategoryName) like lower(:SearchBy)  or upper(CategoryName)like upper(:SearchBy) )" +
                          "or (lower(SubCategoryName) like lower(:SearchBy)  or upper(SubCategoryName)like upper(:SearchBy) )" +
                          "or (lower(SubSubCategoryName) like lower(:SearchBy)  or upper(SubSubCategoryName)like upper(:SearchBy) )" +
                          "or (lower(UnitName) like lower(:SearchBy)  or upper(UnitName)like upper(:SearchBy) )" +
                          "or (lower(FabricName) like lower(:SearchBy)  or upper(FabricName)like upper(:SearchBy) ) )";
            }

            //sql += "AND ROWNUM <= 100 ";

            if (objProductGrid.OrderByName != null && objProductGrid.OrderByDirection != null)
            {
                sql += "ORDER BY " + objProductGrid.OrderByName + " " + objProductGrid.OrderByDirection;
            }

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":SearchBy", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objProductGrid.SearchBy) ?
                        $"%{objProductGrid.SearchBy}%"
                        : null;

                    objConnection.Open();

                    using (OracleDataReader objDataReader = objCommand.ExecuteReader())
                    {
                        List<ProductGrid> grid = new List<ProductGrid>();
                        try
                        {
                            while (objDataReader.Read())
                            {
                                objProductGrid = new ProductGrid();

                                objProductGrid.ProductId = objDataReader["ProductId"].ToString();
                                objProductGrid.ProductName = objDataReader["ProductName"].ToString();
                                objProductGrid.Sku = objDataReader["Sku"].ToString();

                                objProductGrid.CategoryName = objDataReader["CategoryName"].ToString();
                                objProductGrid.SubCategoryName = objDataReader["SubCategoryName"].ToString();
                                objProductGrid.SubSubCategoryName = objDataReader["SubSubCategoryName"].ToString();
                                objProductGrid.UnitName = objDataReader["UnitName"].ToString();
                                objProductGrid.FabricName = objDataReader["FabricName"].ToString();

                                objProductGrid.PurchasePrice = objDataReader["PurchasePrice"].ToString();
                                objProductGrid.SellPrice = objDataReader["SellPrice"].ToString();

                                //objProductGrid.ProductImage = UtilityClass.GetBase64ImageCompressed((byte[]) objDataReader["ProductImage"]);

                                

                                //string[] files = System.IO.Directory.GetFiles(_mainImagepath2, objDataReader["ProductImage"].ToString());

                                objProductGrid.ProductImage = Path.Combine("/Files/ImageProductMain/", objDataReader["ProductImage"].ToString());
                                objProductGrid.ProductImageUrl = objDataReader["PRODUCTIMAGEURL"].ToString();

                                objProductGrid.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";

                                //ProductQuantity = objDataReader.GetValue(5).ToString(),
                                objProductGrid.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                objProductGrid.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();

                                grid.Add(objProductGrid);
                            }

                            return grid;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }
        public async Task<List<ColorList>> GetProductColor(int id, string headOfficeId, string branchOfficeId)
        {
            List<ColorList> colorList = new List<ColorList>();

            var sql = "SELECT " +
                      "PRODUCT_COLOR_ID," +
            "PRODUCT_ID," +
            "COLOR_ID," +
            "COLOR_NAME," +
            "HEAD_OFFICE_ID," +
            "BRANCH_OFFICE_ID " +
                         " FROM VEW_PRODUCT_COLOR WHERE PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = " + headOfficeId + " AND BRANCH_OFFICE_ID = " + branchOfficeId + " ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        ColorList model = new ColorList
                        {
                            ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString()),
                            ColorName = objDataReader["COLOR_NAME"].ToString()
                        };
                        colorList.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return colorList;
        }
        public async Task<List<SizeList>> GetProductSizeAndQuantity(int productId, int colorId, string headOfficeId, string branchOfficeId)
        {
            List<SizeList> sizeAndQuantityList = new List<SizeList>();

            var sql = "SELECT " +
                           "PRODUCT_ID, " +
                           "COLOR_ID, " +
                           "SIZE_ID, " +
                           "SIZE_NAME, " +
                           "QUANTITY, " +
                           "PRICE, " +
                           "OLD_PRICE, " +
                           "HEAD_OFFICE_ID, " +
                           "BRANCH_OFFICE_ID " +
                         " FROM VEW_PRODUCT_INVENTORY WHERE PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID AND HEAD_OFFICE_ID = " + headOfficeId + " AND BRANCH_OFFICE_ID = " + branchOfficeId + " ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", productId);
            objCommand.Parameters.Add("COLOR_ID", colorId);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        SizeList model = new SizeList
                        {
                            SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString()),
                            SizeName = objDataReader["SIZE_NAME"].ToString(),
                            Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString()),
                            Price = Convert.ToDouble(objDataReader["OLD_PRICE"].ToString())
                        };
                        sizeAndQuantityList.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return sizeAndQuantityList;
        }
        public async Task<List<ProductImageGrid>> GetImageGrids(int id)
        {
            List<ProductImageGrid> grid = new List<ProductImageGrid>();

            var sql = "SELECT " +
                      "PRODUCT_IMAGE_ID," +
            "PRODUCT_ID," +
            "PRODUCT_NAME," +
            "SKU," +
            "COLOR_ID," +
            "COLOR_NAME," +
            "IMAGE_1," +
            "IMAGE_2," +
            "IMAGE_3," +
            "IMAGE_4," +
            "DISPLAY_ORDER," +
            "UPDATE_BY," +
            "UPDATE_DATE," +
            "HEAD_OFFICE_ID," +
            "BRANCH_OFFICE_ID " +
                         " FROM VEW_PRODUCT_IMAGE WHERE PRODUCT_ID = :PRODUCT_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        ProductImageGrid model = new ProductImageGrid();

                        model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                        model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                        model.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                        model.ColorName = objDataReader["COLOR_NAME"].ToString();
                        model.Sku = objDataReader["SKU"].ToString();
                        model.ProductImageId = Convert.ToInt32(objDataReader["PRODUCT_IMAGE_ID"].ToString());

                        model.DisplayOrder = !string.IsNullOrWhiteSpace(objDataReader["DISPLAY_ORDER"].ToString()) ? Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()) : 0;

                        model.Image1 = Path.Combine("/Files/ImageProductColor/", objDataReader["IMAGE_1"].ToString());
                        model.Image2 = Path.Combine("/Files/ImageProductColor/", objDataReader["IMAGE_2"].ToString());
                        model.Image3 = Path.Combine("/Files/ImageProductColor/", objDataReader["IMAGE_3"].ToString());
                        model.Image4 = Path.Combine("/Files/ImageProductColor/", objDataReader["IMAGE_4"].ToString());

                        grid.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return grid;
        }
        public async Task<List<RatioList>> GetRatioLists(int id, string headOfficeId, string branchOfficeId)
        {
            List<RatioList> ratioLists = new List<RatioList>();

            var sql = "SELECT DISTINCT " +
                           "PRODUCT_ID, " +
                           "RATIO_ID, " +
                           "RATIO_NAME, " +
                           "HEAD_OFFICE_ID, " +
                           "BRANCH_OFFICE_ID " +
                         " FROM VEW_PRODUCT_RATIO WHERE PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = " + headOfficeId + " AND BRANCH_OFFICE_ID = " + branchOfficeId + " ORDER BY RATIO_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        RatioList model = new RatioList
                        {
                            RatioId = Convert.ToInt32(objDataReader["RATIO_ID"].ToString()),
                            RatioName = objDataReader["RATIO_NAME"].ToString()
                        };
                        ratioLists.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return ratioLists;
        }
        public async Task<List<RatioSizeList>> GetRatioSizeLists(int id, int ratioId, string headOfficeId, string branchOfficeId)
        {
            List<RatioSizeList> sizeList = new List<RatioSizeList>();

            var sql = "SELECT " +
                           "SIZE_ID, " +
                           "SIZE_NAME, " +
                           "SIZE_VALUE, " +
                           "HEAD_OFFICE_ID, " +
                           "BRANCH_OFFICE_ID " +
                         " FROM VEW_PRODUCT_RATIO_SIZE_VALUE WHERE PRODUCT_ID = " + id + " AND RATIO_ID = " + ratioId + " AND HEAD_OFFICE_ID = " + headOfficeId + " AND BRANCH_OFFICE_ID = " + branchOfficeId + " ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        RatioSizeList model = new RatioSizeList
                        {
                            SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString()),
                            SizeName = objDataReader["SIZE_NAME"].ToString(),
                            SizeValue = (float)Convert.ToDouble(objDataReader["SIZE_VALUE"].ToString())
                        };
                        sizeList.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return sizeList;
        }


        //Get Single Data
        public async Task<ProductInventoryModel> GetProductInventory(int id, string headOfficeId, string branchOfficeId)
        {
            ProductInventoryModel objProductInventoryModel = new ProductInventoryModel();

            var sql = "SELECT " +
                        "PRODUCT_ID," +
                        "PRODUCT_NAME," +
                        "SKU," +
                        "CATEGORY_ID," +
                        "SUB_CATEGORY_ID," +
                        "SUB_SUB_CATEGORY_ID," +
                        "UNIT_ID," +
                        "FABRIC_ID," +
                        "PRIMARY_IMAGE," +
                        "SHORT_DESCRIPTION," +
                        "LONG_DESCRIPTION," +
                        "PURCHASE_AMOUNT," +
                        "SELL_AMOUNT," +
                        "INCLUDE_VAT," +
                        "DESIGNER_ID," +
                        "MARCHANDISER_ID," +
                        "ACTIVE_YN," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +

                         " FROM VEW_PRODUCT_INFO where PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = '" + headOfficeId + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId + "' ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {

                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {

                        objProductInventoryModel.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                        objProductInventoryModel.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                        objProductInventoryModel.Sku = objDataReader["SKU"].ToString();
                        objProductInventoryModel.ShortDescription = objDataReader["SHORT_DESCRIPTION"].ToString();
                        objProductInventoryModel.FullDescription = objDataReader["LONG_DESCRIPTION"].ToString();
                        objProductInventoryModel.PrimaryImageString = objDataReader["PRIMARY_IMAGE"].ToString();
                        objProductInventoryModel.FabricId = objDataReader["FABRIC_ID"].ToString();
                        objProductInventoryModel.MeasurementUnitId = objDataReader["UNIT_ID"].ToString();
                        objProductInventoryModel.CategoryId = objDataReader["CATEGORY_ID"].ToString();
                        objProductInventoryModel.SubCategoryId = objDataReader["SUB_CATEGORY_ID"].ToString();
                        objProductInventoryModel.SubSubCategoryId = objDataReader["SUB_SUB_CATEGORY_ID"].ToString();
                        objProductInventoryModel.PurchasePrice = (float?)Convert.ToDouble(objDataReader["PURCHASE_AMOUNT"].ToString());
                        objProductInventoryModel.SellingPrice = (float?)Convert.ToDouble(objDataReader["SELL_AMOUNT"].ToString());

                        objProductInventoryModel.MerchandiserId = objDataReader["MARCHANDISER_ID"].ToString();
                        objProductInventoryModel.DesignerId = objDataReader["DESIGNER_ID"].ToString();

                        objProductInventoryModel.ActiveStatus = objDataReader["ACTIVE_YN"].ToString() == "Y";

                        objProductInventoryModel.IncludeVat = objDataReader["INCLUDE_VAT"].ToString() == "Y";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objProductInventoryModel;
        }

        public async Task<List<MapList>> GetProductMap(int productId)
        {
            const string sql = "SELECT " +
            "MAPPING_ID," +
            "PRODUCT_ID," +
            "CATEGORY_ID," +
            "CATEGORY_NAME," +
            "SUB_CATEGORY_ID," +
            "SUB_CATEGORY_NAME," +
            "SUB_SUB_CATEGORY_ID," +
            "SUB_SUB_CATEGORY_NAME " +
            "FROM VEW_PRODUCT_MAP WHERE PRODUCT_ID = :PRODUCT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<MapList> mapList = new List<MapList>();
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                MapList model = new MapList();

                                model.MapId = objDataReader["CATEGORY_ID"] + ", " + objDataReader["SUB_CATEGORY_ID"] + ", " + objDataReader["SUB_SUB_CATEGORY_ID"];

                                model.MapName = objDataReader["CATEGORY_NAME"] + " >> " + objDataReader["SUB_CATEGORY_NAME"] + " >> " + objDataReader["SUB_SUB_CATEGORY_NAME"];

                                mapList.Add(model);
                            }

                            return mapList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<PeoductInventoryDisplay> GetProductInventorySummary(int id, string headOfficeId, string branchOfficeId)
        {
            PeoductInventoryDisplay objProductInventoryModel = new PeoductInventoryDisplay();

            var sql = "SELECT " +
                           "PRIMARY_IMAGE, " +
                           "PRODUCT_ID, " +
                           "PRODUCT_NAME, " +
                           "SKU, " +
                           "TOTAL_COLOR, " +
                           "TOTAL_SIZE, " +
                           "TOTAL_QUANTITY, " +
                           "UPDATE_BY, " +
                           "HEAD_OFFICE_ID, " +
                           "BRANCH_OFFICE_ID " +
                         " FROM VEW_INVENTORY_SUMMARY where PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = '" + headOfficeId + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId + "' ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objProductInventoryModel.ImageString = Path.Combine("/Files/ImageProductMain/", objDataReader["PRIMARY_IMAGE"].ToString());
                        objProductInventoryModel.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                        objProductInventoryModel.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                        objProductInventoryModel.Sku = objDataReader["SKU"].ToString();
                        objProductInventoryModel.TotalColor = objDataReader["TOTAL_COLOR"].ToString();
                        objProductInventoryModel.TotalSize = objDataReader["TOTAL_SIZE"].ToString();
                        objProductInventoryModel.TotalQuantity = objDataReader["TOTAL_QUANTITY"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objProductInventoryModel;
        }

        public RatioChartDisplay GetProductForRatioChart(int id, string headOfficeId, string branchOfficeId)
        {
            RatioChartDisplay objRatioChartDisplay = new RatioChartDisplay();

            var sql = "SELECT " +
                           "PRIMARY_IMAGE, " +
                           "PRODUCT_ID, " +
                           "PRODUCT_NAME, " +
                           "SKU, " +
                           "HEAD_OFFICE_ID, " +
                           "BRANCH_OFFICE_ID " +
                         " FROM VEW_INVENTORY_SUMMARY where PRODUCT_ID = '" + id + "' AND HEAD_OFFICE_ID = '" + headOfficeId + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                strConn.Open();
                var objDataReader = objCommand.ExecuteReader();
                try
                {
                    while (objDataReader.Read())
                    {
                        objRatioChartDisplay.ImageString = Path.Combine("/Files/ImageProductMain/", objDataReader["PRIMARY_IMAGE"].ToString());
                        objRatioChartDisplay.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                        objRatioChartDisplay.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                        objRatioChartDisplay.Sku = objDataReader["SKU"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objRatioChartDisplay;
        }


        //Color counter
        public async Task<int> ColorCount(int id)
        {
            int counter = 0;

            var sql = "SELECT " +
                        "TOTAL_COLOR " +
                        " FROM VEW_COLOR_COUNT where PRODUCT_ID = :PRODUCT_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        counter = Convert.ToInt32(objDataReader["TOTAL_COLOR"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return counter;
        }


        //Size Counter
        public async Task<int> SizeCount(int id)
        {
            int counter = 0;

            var sql = "SELECT " +
                        "TOTAL_SIZE" +
                        " FROM VEW_SIZE_COUNT where PRODUCT_ID = :PRODUCT_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        counter = Convert.ToInt32(objDataReader["total_size"].ToString());
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return counter;
        }


        //Save
        public async Task<Tuple<string, int>> SaveProductInfo(ProductInventoryModel productInventoryModel)
        {
            string strMsg;
            int productId = 0;

            OracleCommand objOracleCommand = new OracleCommand("PRO_product_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.ProductId != 0 ? productInventoryModel.ProductId.ToString() : null;
            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.CategoryId) ? productInventoryModel.CategoryId : null;
            objOracleCommand.Parameters.Add("p_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.SubCategoryId) ? productInventoryModel.SubCategoryId : null;
            objOracleCommand.Parameters.Add("p_sub_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.SubSubCategoryId) ? productInventoryModel.SubSubCategoryId : null;
            objOracleCommand.Parameters.Add("p_fabric_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.FabricId) ? productInventoryModel.FabricId : null;
            objOracleCommand.Parameters.Add("p_measurement_unit_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.MeasurementUnitId) ? productInventoryModel.MeasurementUnitId : null;
            objOracleCommand.Parameters.Add("p_product_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ProductName) ? productInventoryModel.ProductName : null;
            
            objOracleCommand.Parameters.Add("p_sku", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.Sku) ? productInventoryModel.Sku : null;

            objOracleCommand.Parameters.Add("p_purchase_price", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.PurchasePrice.ToString()) ? productInventoryModel.PurchasePrice.ToString() : null;
            objOracleCommand.Parameters.Add("p_sell_price", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.SellingPrice.ToString()) ? productInventoryModel.SellingPrice.ToString() : null;
            objOracleCommand.Parameters.Add("p_include_vat", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.IncludeVat ? "Y" : "N";
            
            objOracleCommand.Parameters.Add("p_image", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.PrimaryImageString) ? productInventoryModel.PrimaryImageString : null;
            objOracleCommand.Parameters.Add("p_image_url", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.PrimaryImageUrl) ? productInventoryModel.PrimaryImageUrl : null;

            objOracleCommand.Parameters.Add("p_active_yn", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.ActiveStatus ? "Y" : "N";

            objOracleCommand.Parameters.Add("p_short_description", OracleDbType.NVarchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ShortDescription) ? productInventoryModel.ShortDescription : null;
            objOracleCommand.Parameters.Add("p_long_description", OracleDbType.NVarchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.FullDescription) ? productInventoryModel.FullDescription : null;

            objOracleCommand.Parameters.Add("p_merchandiser_id", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.MerchandiserId) ? productInventoryModel.MerchandiserId : null;
            objOracleCommand.Parameters.Add("p_designer_id", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.DesignerId) ? productInventoryModel.DesignerId : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.UpdateBy) ? productInventoryModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.HeadOfficeId) ? productInventoryModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.InputOutput).Value = !string.IsNullOrWhiteSpace(productInventoryModel.BranchOfficeId) ? productInventoryModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;
            objOracleCommand.Parameters.Add("P_OUT_PRODUCT_ID", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                    productId = Convert.ToInt32(objOracleCommand.Parameters["P_OUT_PRODUCT_ID"].Value.ToString());
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return new Tuple<string, int>(strMsg, productId);
        }

        public async Task<string> SaveProductMapping(MapList mapList)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRODUCT_MAPPING_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_MAPPING_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = mapList.MapId;
            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = mapList.ProductId > 0 ? mapList.ProductId : (object) null;
            objOracleCommand.Parameters.Add("P_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = mapList.CategoryId > 0 ? mapList.CategoryId : (object) null;
            objOracleCommand.Parameters.Add("P_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = mapList.SubCategoryId > 0 ? mapList.SubCategoryId : (object) null;
            objOracleCommand.Parameters.Add("P_SUB_SUB_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = mapList.SubSubCategoryId > 0 ? mapList.SubSubCategoryId : (object) null;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(mapList.UpdateBy)? mapList.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(mapList.HeadOfficeId) ? mapList.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(mapList.BranchOfficeId) ? mapList.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMessage;
        }

        public async Task<string> DeleteProductMapping(int productId, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRODUCT_MAPPING_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId > 0 ? productId : (object)null;

            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMessage;
        }

        public async Task<string> SaveProductImageAndColor(ProductInventoryModel productInventoryModel)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRODUCT_IMAGE_COLOR_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.ProductId != 0 ? productInventoryModel.ProductId.ToString() : null;
            objOracleCommand.Parameters.Add("P_PRODUCT_COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.ColorId != 0 ? productInventoryModel.ColorId.ToString() : null;
            objOracleCommand.Parameters.Add("P_PRODUCT_IMAGE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.ImageId != 0 ? productInventoryModel.ImageId.ToString() : null;

            objOracleCommand.Parameters.Add("P_IMAGE_1", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ImageString1) ? productInventoryModel.ImageString1 : null;
            objOracleCommand.Parameters.Add("P_IMAGE_2", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ImageString2) ? productInventoryModel.ImageString2 : null;
            objOracleCommand.Parameters.Add("P_IMAGE_3", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ImageString3) ? productInventoryModel.ImageString3 : null;
            objOracleCommand.Parameters.Add("P_IMAGE_4", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.ImageString4) ? productInventoryModel.ImageString4 : null;

            objOracleCommand.Parameters.Add("P_DISPLAY_ORDER", OracleDbType.Varchar2, ParameterDirection.Input).Value = productInventoryModel.DisplayOrder != null && productInventoryModel.DisplayOrder != 0? productInventoryModel.DisplayOrder: 0;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.UpdateBy) ? productInventoryModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.HeadOfficeId) ? productInventoryModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(productInventoryModel.BranchOfficeId) ? productInventoryModel.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMsg;
        }

        public async Task<string> DeleteProductImage(int productId, int colorId, string headOfficeId, string branchOfficeId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRODUCT_IMAGE_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
            objOracleCommand.Parameters.Add("p_color_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<IEnumerable<string>> GetImageName(int productId, int colorId, string headOfficeId, string branchOfficeId)
        {
            const string sql = "SELECT " +
                               "PRODUCT_ID," +
                               "COLOR_ID," +
                               "IMAGE_1," +
                               "IMAGE_2," +
                               "IMAGE_3," +
                               "IMAGE_4 " +
                               "FROM VEW_PRODUCT_IMAGE WHERE PRODUCT_ID = :PRODUCT_ID AND COLOR_ID = :COLOR_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<string> imageList = new List<string>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                imageList.Add(objDataReader["IMAGE_1"].ToString());
                                imageList.Add(objDataReader["IMAGE_2"].ToString());
                                imageList.Add(objDataReader["IMAGE_3"].ToString());
                                imageList.Add(objDataReader["IMAGE_4"].ToString());
                            }
                            return imageList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> DeleteProductSize(int productId, int colorId, int sizeId, string headOfficeId, string branchOfficeId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRODUCT_SIZE_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
            objOracleCommand.Parameters.Add("p_color_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorId;
            objOracleCommand.Parameters.Add("p_size_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = sizeId;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> SizeSave(SizeAndQuantitySave objSizeSave)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_product_size_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_size_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSizeSave.ProductId != 0 ? objSizeSave.ProductId.ToString() : null;

            objOracleCommand.Parameters.Add("p_size_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSizeSave.SizeId != 0 ? objSizeSave.SizeId.ToString() : null;


            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeSave.UpdateBy) ? objSizeSave.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeSave.HeadOfficeId) ? objSizeSave.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeSave.BranchOfficeId) ? objSizeSave.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMsg;
        }

        public async Task<string> SaveProductInventory(SizeAndQuantitySave inventorySave)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_product_inventory_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_INVENTORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_INVENTORY_ENTRY_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(inventorySave.EntryDate) ? inventorySave.EntryDate : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = inventorySave.ProductId != 0 ? inventorySave.ProductId.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_COLOR_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = inventorySave.ColorId != 0 ? inventorySave.ColorId.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = inventorySave.SizeId != 0 ? inventorySave.SizeId.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_INVENTORY_VALUE", OracleDbType.Varchar2, ParameterDirection.Input).Value = inventorySave.Quantity != 0 ? inventorySave.Quantity.ToString() : null;

            objOracleCommand.Parameters.Add("P_PRODUCT_PRICE", OracleDbType.Varchar2, ParameterDirection.Input).Value = inventorySave.Price > 0 ? (object) inventorySave.Price : null;


            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(inventorySave.UpdateBy) ? inventorySave.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(inventorySave.HeadOfficeId) ? inventorySave.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(inventorySave.BranchOfficeId) ? inventorySave.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMsg;
        }
        public async Task<string> SaveProductSizeRatio(SizeAndRatioSave objSizeAndRatioSave)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_product_ratio_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_RATIO_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.ProductId.ToString()) ? objSizeAndRatioSave.ProductId.ToString() : null;

            objOracleCommand.Parameters.Add("P_RATIO_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.RatioId.ToString()) ? objSizeAndRatioSave.RatioId.ToString() : null;

            objOracleCommand.Parameters.Add("P_SIZE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.SizeId.ToString()) ? objSizeAndRatioSave.SizeId.ToString() : null;

            objOracleCommand.Parameters.Add("P_SIZE_VALUE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.SizeValue.ToString(CultureInfo.InvariantCulture)) ? objSizeAndRatioSave.SizeValue.ToString(CultureInfo.InvariantCulture) : null;

            objOracleCommand.Parameters.Add("P_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.UpdateBy) ? objSizeAndRatioSave.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.HeadOfficeId) ? objSizeAndRatioSave.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeAndRatioSave.BranchOfficeId) ? objSizeAndRatioSave.BranchOfficeId : null;



            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMsg;
        }


        //For Drop Down List
        public async Task<DataTable> GetSize()
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "SIZE_ID, " +
                         "SIZE_NAME " +
                         "FROM L_SIZE order by SIZE_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetRatioList()
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "RATIO_ID, " +
                         "RATIO_NAME " +

                         "FROM L_RATIO order by RATIO_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetProductColorList(int id, string headOfficeId, string branchOfficeId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "PRODUCT_ID, " +
                         "COLOR_ID, " +
                         "COLOR_NAME, " +
                         "HEAD_OFFICE_ID, " +
                         "BRANCH_OFFICE_ID " +

                         "FROM VEW_PRODUCT_COLOR where PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID order by COLOR_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId);
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetProductSizeList(int id, string headOfficeId, string branchOfficeId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "PRODUCT_ID, " +
                         "SIZE_ID, " +
                         "SIZE_NAME, " +
                         "HEAD_OFFICE_ID, " +
                         "BRANCH_OFFICE_ID " +

                         "FROM VEW_PRODUCT_SIZE where PRODUCT_ID = :PRODUCT_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID  order by SIZE_NAME  ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("PRODUCT_ID", id);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId);
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetFabric()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "FABRIC_ID, " +
                               "FABRIC_NAME " +
                               "FROM L_FABRIC order by FABRIC_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetMeasurementUnit()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "UNIT_ID, " +
                               "UNIT_NAME " +
                               "FROM L_MEASUREMENT_UNIT order by UNIT_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetCategory()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "CATEGORY_ID, " +
                               "CATEGORY_NAME " +
                               "FROM L_CATEGORY order by CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        public async Task<DataTable> GetMerchandiser()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "EMPLOYEE_ID, " +
                               "EMPLOYEE_NAME " +
                               "FROM VEW_MERCHANDISER ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetDesigner()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "EMPLOYEE_ID, " +
                               "EMPLOYEE_NAME " +
                               "FROM VEW_DESIGNER ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        public async Task<DataTable> GetSubCategoryList(int categoryId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "CATEGORY_ID, " +
                         "SUB_CATEGORY_ID, " +
                         "SUB_CATEGORY_NAME " +
                         "FROM L_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID  order by SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetSubSubCategoryList(int categoryId, int subCategoryId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "CATEGORY_ID, " +
                         "SUB_CATEGORY_ID, " +
                         "SUB_SUB_CATEGORY_ID, " +
                         "SUB_SUB_CATEGORY_NAME " +
                         "FROM L_SUB_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID  order by SUB_SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);
            objCommand.Parameters.Add("SUB_CATEGORY_ID", subCategoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        public async Task<DataTable> GetColor()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "COLOR_ID, " +
                               "COLOR_NAME " +
                               "FROM L_COLOR order by COLOR_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
    }
}
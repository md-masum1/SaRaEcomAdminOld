﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class DiscountAndOfferDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        //Get List Data
        public async Task<IEnumerable<PrivilegeCardModel>> GetPrivilegeCardList()
        {
            var sql = "SELECT * FROM VEW_PRIVILEGE_CARD ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    await objConnection.OpenAsync();

                    using (OracleDataReader objDataReader = (OracleDataReader) await objCommand.ExecuteReaderAsync())
                    {
                        List<PrivilegeCardModel> grid = new List<PrivilegeCardModel>();
                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                var model = new PrivilegeCardModel
                                {
                                    CardId = Convert.ToInt32(objDataReader["CARD_ID"].ToString()),
                                    CardNumber = objDataReader["CARD_NUMBER"].ToString(),
                                    StartDate = objDataReader["START_DATE"].ToString(),
                                    EndDate = objDataReader["END_DATE"].ToString(),
                                    Discount = Convert.ToDouble(objDataReader["DISCOUNT_PERCENTAGE"].ToString()),
                                    IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y",
                                    UpdateBy = objDataReader["UPDATE_BY"].ToString()
                                };


                                grid.Add(model);
                            }
                            return grid;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        //Get Single Data
        public async Task<PrivilegeCardModel> GetPrivilegeCard(int cardId)
        {
            var sql = "SELECT * FROM VEW_PRIVILEGE_CARD WHERE CARD_ID = :CARD_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CARD_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = cardId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            PrivilegeCardModel model = new PrivilegeCardModel();
                            while (await objDataReader.ReadAsync())
                            {
                                model.CardId = Convert.ToInt32(objDataReader["CARD_ID"].ToString());
                                model.CardNumber = objDataReader["CARD_NUMBER"].ToString();
                                model.StartDate = objDataReader["START_DATE"].ToString();
                                model.EndDate = objDataReader["END_DATE"].ToString();
                                model.Discount = Convert.ToDouble(objDataReader["DISCOUNT_PERCENTAGE"].ToString());
                                model.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";
                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        //Save and Update Data
        public async Task<string> SavePrivilegeCard(PrivilegeCardModel model)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRIVILEGE_CARD_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_CARD_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = model.CardId;
            objOracleCommand.Parameters.Add("P_CARD_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.CardNumber) ? model.CardNumber : null;
            objOracleCommand.Parameters.Add("P_DISCOUNT_PERCENTAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = model.Discount > 0 ? model.Discount : (object)null;
            objOracleCommand.Parameters.Add("P_START_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.StartDate) ? model.StartDate : null;
            objOracleCommand.Parameters.Add("P_END_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.EndDate) ? model.EndDate : null;
            objOracleCommand.Parameters.Add("P_ACTIVE_YN", OracleDbType.Varchar2, ParameterDirection.Input).Value = model.IsActive ? "Y" : "N";

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.UpdateBy) ? model.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.HeadOfficeId) ? model.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(model.BranchOfficeId) ? model.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMessage;
        }

        //Delete Data
        public async Task<string> DeletePrivilegeCard(int cardId, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRIVILEGE_CARD_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_CARD_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = cardId;

            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMessage;
        }
    }
}
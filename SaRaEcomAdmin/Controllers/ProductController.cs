﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductDal _productDal = new ProductDal();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> AddOrEditProduct(ProductModel objProductModel)
        {
            ModelState.Clear();
            objProductModel.IncludeVat = true;

            ViewBag.FabricList = UtilityClass.GetSelectListByDataTable(await _productDal.GetFabric(), "FABRIC_ID", "FABRIC_NAME");
            ViewBag.UnitList = UtilityClass.GetSelectListByDataTable(await _productDal.GetMeasurementUnit(), "UNIT_ID", "UNIT_NAME");
            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _productDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _productDal.GetColor(), "COLOR_ID", "COLOR_NAME");

            if (objProductModel.ProductId != 0)
            {
                ViewBag.SubCategoryList = UtilityClass.GetSelectListByDataTable(await _productDal.GetSubCategory(objProductModel.CategoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
                ViewBag.SubSubCategoryList = UtilityClass.GetSelectListByDataTable(await _productDal.GetSubSubCategory(objProductModel.CategoryId, objProductModel.SubCategoryId), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");
            }

            return View(objProductModel);
        }

        public async Task<JsonResult> GetSubCategory(int categoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productDal.GetSubCategory(categoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetSubSubCategory(int categoryId, int subCategoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productDal.GetSubSubCategory(categoryId, subCategoryId), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProduct(ProductModel objProductModel)
        {
            return View("Index");
        }
    }
}
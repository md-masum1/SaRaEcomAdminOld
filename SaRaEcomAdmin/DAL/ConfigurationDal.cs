﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;
using System.Web.Mvc;

namespace SaRaEcomAdmin.DAL
{
    public class ConfigurationDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Slider Section
        public async Task<List<SliderModel>> GetSliderList(string headOfficeId, string branchOfficeId)
        {
            List<SliderModel> objSliderModel = new List<SliderModel>();

            var sql = "SELECT " +
                        "SLIDER_ID," +
                        "SLIDER_TITLE," +
                        "SLIDER_DESCRIPTION," +
                        "SLIDER_URL," +
                        "DISPLAY_ORDER," +
                        "SLIDER_IMAGE," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_HOMEPAGE_SLIDER where HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY SLIDER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) {CommandType = CommandType.Text};

            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        SliderModel model = new SliderModel
                        {
                            SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString()),
                            SliderTitle = objDataReader["SLIDER_TITLE"].ToString(),
                            SliderDescription = objDataReader["SLIDER_DESCRIPTION"].ToString(),
                            SliderUrl = objDataReader["SLIDER_URL"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            SliderImageSiring = objDataReader["SLIDER_IMAGE"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSliderModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSliderModel;
        }
        public async Task<SliderModel> GetSlider(int sliderId, string headOfficeId, string branchOfficeId)
        {
            SliderModel objSliderModel = new SliderModel();

            var sql = "SELECT " +
                          "SLIDER_ID," +
                          "SLIDER_TITLE," +
                          "SLIDER_DESCRIPTION," +
                          "SLIDER_URL," +
                          "DISPLAY_ORDER," +
                          "SLIDER_IMAGE," +
                          "UPDATE_BY," +
                          "HEAD_OFFICE_ID," +
                          "BRANCH_OFFICE_ID " +
                          "FROM VEW_HOMEPAGE_SLIDER where SLIDER_ID = :SLIDER_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY SLIDER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("SLIDER_ID", sliderId);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSliderModel.SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString());
                        objSliderModel.SliderTitle = objDataReader["SLIDER_TITLE"].ToString();
                        objSliderModel.SliderDescription = objDataReader["SLIDER_DESCRIPTION"].ToString();
                        objSliderModel.SliderUrl = objDataReader["SLIDER_URL"].ToString();
                        objSliderModel.SliderImageSiring = objDataReader["SLIDER_IMAGE"].ToString();
                        objSliderModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objSliderModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSliderModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSliderModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSliderModel;
        }
        public async Task<string> SaveSlider(SliderModel objSliderModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_slider_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_slider_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSliderModel.SliderId;
            objOracleCommand.Parameters.Add("p_slider_title", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.SliderTitle) ? objSliderModel.SliderTitle : null;
            objOracleCommand.Parameters.Add("p_slider_description", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.SliderDescription) ? objSliderModel.SliderDescription : null;
            objOracleCommand.Parameters.Add("p_slider_url", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.SliderUrl) ? objSliderModel.SliderUrl : null;
            objOracleCommand.Parameters.Add("p_slider_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSliderModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_slider_image", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.SliderImageSiring) ? objSliderModel.SliderImageSiring : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.UpdateBy) ? objSliderModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.HeadOfficeId) ? objSliderModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSliderModel.BranchOfficeId) ? objSliderModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteSlider(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_slider_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_slider_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region Offer Section
        public async Task<List<OfferModel>> GetOfferList(string headOfficeId, string branchOfficeId)
        {
            List<OfferModel> objOfferModel = new List<OfferModel>();

            const string sql = "SELECT " +
                               "OFFER_ID," +
                               "OFFER_URL," +
                               "OFFER_IMAGE," +
                               "DISPLAY_ORDER," +
                               "UPDATE_BY," +
                               "HEAD_OFFICE_ID," +
                               "BRANCH_OFFICE_ID " +
                               "FROM VEW_HOMEPAGE_OFFER where HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY DISPLAY_ORDER ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        OfferModel model = new OfferModel
                        {
                            OfferId = Convert.ToInt32(objDataReader["OFFER_ID"].ToString()),
                            OfferUrl = objDataReader["OFFER_URL"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            OfferImageSiring = objDataReader["OFFER_IMAGE"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objOfferModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objOfferModel;
        }
        public async Task<OfferModel> GetOffer(int offerId, string headOfficeId, string branchOfficeId)
        {
            OfferModel objSliderModel = new OfferModel();

            var sql = "SELECT " +
                      "OFFER_ID," +
                      "OFFER_URL," +
                      "OFFER_IMAGE," +
                      "DISPLAY_ORDER," +
                      "UPDATE_BY," +
                      "HEAD_OFFICE_ID," +
                      "BRANCH_OFFICE_ID " +
                      "FROM VEW_HOMEPAGE_OFFER where OFFER_ID = :OFFER_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("OFFER_ID", offerId);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSliderModel.OfferId = Convert.ToInt32(objDataReader["OFFER_ID"].ToString());
                        objSliderModel.OfferUrl = objDataReader["OFFER_URL"].ToString();
                        objSliderModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objSliderModel.OfferImageSiring = objDataReader["OFFER_IMAGE"].ToString();
                        objSliderModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSliderModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSliderModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSliderModel;
        }
        public async Task<string> SaveSlider(OfferModel objOfferModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_HOMEPAGE_OFFER_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_OFFER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objOfferModel.OfferId != 0 ? (object) objOfferModel.OfferId : null;

            objOracleCommand.Parameters.Add("P_OFFER_URL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objOfferModel.OfferUrl) ? objOfferModel.OfferUrl : null;
            objOracleCommand.Parameters.Add("P_OFFER_DISPLAY_ORDER", OracleDbType.Varchar2, ParameterDirection.Input).Value = objOfferModel.DisplayOrder;
            objOracleCommand.Parameters.Add("P_OFFER_IMAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objOfferModel.OfferImageSiring) ? objOfferModel.OfferImageSiring : null;

            objOracleCommand.Parameters.Add("P_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objOfferModel.UpdateBy) ? objOfferModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objOfferModel.HeadOfficeId) ? objOfferModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objOfferModel.BranchOfficeId) ? objOfferModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteOffer(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_HOMEPAGE_OFFER_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_OFFER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region Banner Section
        public async Task<List<BannerModel>> GetBannerList(string headOfficeId, string branchOfficeId)
        {
            List<BannerModel> objBannerModel = new List<BannerModel>();

            var sql = "SELECT " +
                        "BANNER_ID," +
                        "BANNER_TITLE," +
                        "BANNER_DESCRIPTION," +
                        "BANNER_URL," +
                        "DISPLAY_ORDER," +
                        "BANNER_IMAGE," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_HOMEPAGE_BANNER where HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY BANNER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        BannerModel model = new BannerModel
                        {
                            BannerId = Convert.ToInt32(objDataReader["BANNER_ID"].ToString()),
                            BannerTitle = objDataReader["BANNER_TITLE"].ToString(),
                            BannerDescription = objDataReader["BANNER_DESCRIPTION"].ToString(),
                            BannerUrl = objDataReader["BANNER_URL"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            BannerImageSiring = objDataReader["BANNER_IMAGE"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objBannerModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objBannerModel;
        }
        public async Task<BannerModel> GetBanner(int bannerId, string headOfficeId, string branchOfficeId)
        {
            BannerModel objBannerModel = new BannerModel();

            var sql = "SELECT " +
                          "BANNER_ID," +
                          "BANNER_TITLE," +
                          "BANNER_DESCRIPTION," +
                          "BANNER_URL," +
                          "DISPLAY_ORDER," +
                          "BANNER_IMAGE," +
                          "UPDATE_BY," +
                          "HEAD_OFFICE_ID," +
                          "BRANCH_OFFICE_ID " +
                          "FROM VEW_HOMEPAGE_BANNER where BANNER_ID = :BANNER_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY BANNER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("BANNER_ID", bannerId);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objBannerModel.BannerId = Convert.ToInt32(objDataReader["BANNER_ID"].ToString());
                        objBannerModel.BannerTitle = objDataReader["BANNER_TITLE"].ToString();
                        objBannerModel.BannerDescription = objDataReader["BANNER_DESCRIPTION"].ToString();
                        objBannerModel.BannerUrl = objDataReader["BANNER_URL"].ToString();
                        objBannerModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objBannerModel.BannerImageSiring = objDataReader["BANNER_IMAGE"].ToString();
                        objBannerModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objBannerModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objBannerModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objBannerModel;
        }
        public async Task<string> SaveBanner(BannerModel objBannerModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_banner_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_banner_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objBannerModel.BannerId;
            objOracleCommand.Parameters.Add("p_banner_title", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.BannerTitle) ? objBannerModel.BannerTitle : null;
            objOracleCommand.Parameters.Add("p_banner_description", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.BannerDescription) ? objBannerModel.BannerDescription : null;
            objOracleCommand.Parameters.Add("p_banner_url", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.BannerUrl) ? objBannerModel.BannerUrl : null;
            objOracleCommand.Parameters.Add("p_banner_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = objBannerModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_banner_image", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.BannerImageSiring) ? objBannerModel.BannerImageSiring : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.UpdateBy) ? objBannerModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.HeadOfficeId) ? objBannerModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objBannerModel.BranchOfficeId) ? objBannerModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region Trending Product Section

        public async Task<List<TrendingProductModel>> GetTrendingProductList(string headOfficeId, string branchOfficeId)
        {
            List<TrendingProductModel> objTrendingProduct = new List<TrendingProductModel>();

            var sql = "SELECT " +
                    "TRENDING_PRODUCT_ID," +
                    "PRODUCT_ID," +
                    "PRODUCT_NAME," +
                    "SKU," +
                    "PRIMARY_IMAGE " +
                    "FROM VEW_TRENDING_PRODUCT ORDER BY TRENDING_PRODUCT_ID DESC ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        TrendingProductModel model = new TrendingProductModel
                        {
                            ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString()),
                            ProductName = objDataReader["PRODUCT_NAME"].ToString(),
                            Sku = objDataReader["SKU"].ToString(),
                            ProductImage = objDataReader["PRIMARY_IMAGE"].ToString()
                        };
                        objTrendingProduct.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objTrendingProduct;
        }
        public async Task<string> SaveTrendingProduct(TrendingProductModel trendingProductModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_TRENDING_PRODUCT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_trending_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = trendingProductModel.ProductId != 0 ? trendingProductModel.ProductId.ToString() : null;

            

            objOracleCommand.Parameters.Add("p_product_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trendingProductModel.ProductName) ? trendingProductModel.ProductName : null;
            objOracleCommand.Parameters.Add("p_product_sku", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trendingProductModel.Sku) ? trendingProductModel.Sku : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trendingProductModel.UpdateBy) ? trendingProductModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trendingProductModel.HeadOfficeId) ? trendingProductModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(trendingProductModel.BranchOfficeId) ? trendingProductModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteTrendingProduct(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_trending_pro_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public DataTable GetProductListDropdown()
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                      "PRODUCT_ID, " +
                      "PRODUCT_CODE," +
                      "SKU " +
                      "FROM VEW_PRODUCT_INFO ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    strConn.Open();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        #endregion

        #region Promotional Product

        public DataTable ProductListBySku()
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                      "PRODUCT_ID, " +
                      "product_code " +
                      "FROM VEW_PRODUCT_INFO ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    strConn.Open();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        public async Task<string> SavePromotionalProduct(PromotionalProductModel objPromotionalProduct)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PROMOTIONAL_PRODUCTS_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PROMOTION_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objPromotionalProduct.ProductId != 0 ? objPromotionalProduct.ProductId.ToString() : null;


            if (objPromotionalProduct.DisplayOrder != 0)
                objOracleCommand.Parameters.Add("P_DISPLAY_ORDER", OracleDbType.Varchar2, ParameterDirection.Input)
                    .Value = objPromotionalProduct.DisplayOrder.ToString();
            else
                objOracleCommand.Parameters.Add("P_DISPLAY_ORDER", OracleDbType.Varchar2, ParameterDirection.Input)
                    .Value = 0;
            objOracleCommand.Parameters.Add("P_START_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objPromotionalProduct.StartDate) ? objPromotionalProduct.StartDate : null;
            objOracleCommand.Parameters.Add("P_END_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objPromotionalProduct.EndDate) ? objPromotionalProduct.EndDate : null;
            if (objPromotionalProduct.PromotionPercentage != 0)
                objOracleCommand.Parameters
                        .Add("P_PROMOTION_PERCENTAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value =
                    objPromotionalProduct.PromotionPercentage.ToString();
            else
                objOracleCommand.Parameters
                    .Add("P_PROMOTION_PERCENTAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = 0;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objPromotionalProduct.UpdateBy) ? objPromotionalProduct.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objPromotionalProduct.HeadOfficeId) ? objPromotionalProduct.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objPromotionalProduct.BranchOfficeId) ? objPromotionalProduct.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        public async Task<PromotionalProductModel> GetPromotionalProduct(int productId)
        {
            const string sql = "SELECT " +
                               "PROMOTION_ID," +
                               "PRODUCT_ID," +
                               "DISPLAY_ORDER," +
                               "START_DATE," +
                               "END_DATE," +
                               "PROMOTION_PERCENTAGE," +
                               "UPDATE_BY," +
                               "HEAD_OFFICE_ID," +
                               "BRANCH_OFFICE_ID " +
                               "FROM VEW_PROMOTIONAL_PRODUCTS WHERE PRODUCT_ID = :PRODUCT_ID ";


            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) {CommandType = CommandType.Text})
                {
                    objCommand.Parameters.Add(":PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        PromotionalProductModel objPromotionalProduct = new PromotionalProductModel();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objPromotionalProduct.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                objPromotionalProduct.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                                objPromotionalProduct.StartDate = objDataReader["START_DATE"].ToString();
                                objPromotionalProduct.EndDate = objDataReader["END_DATE"].ToString();
                                objPromotionalProduct.PromotionPercentage = Convert.ToDouble(objDataReader["PROMOTION_PERCENTAGE"].ToString());
                                objPromotionalProduct.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                objPromotionalProduct.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                objPromotionalProduct.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                            }
                            return objPromotionalProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<PromotionalProductModel>> GetPromotionalProducts()
        {
            const string sql = "SELECT " +
                               "PROMOTION_ID," +
                               "PRODUCT_ID," +
                               "PRODUCT_NAME," +
                               "SKU," +
                               "IMAGE," +
                               "DISPLAY_ORDER," +
                               "START_DATE," +
                               "END_DATE," +
                               "PROMOTION_PERCENTAGE," +
                               "PREVIOUS_PRICE," +
                               "NEW_PRICE," +
                               "STATUS," +
                               "UPDATE_BY," +
                               "HEAD_OFFICE_ID," +
                               "BRANCH_OFFICE_ID " +
                               "FROM VEW_PROMOTIONAL_PRODUCTS ";


            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<PromotionalProductModel> objPromotionalProduct = new List<PromotionalProductModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                PromotionalProductModel model = new PromotionalProductModel();

                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.ProductImage = objDataReader["IMAGE"].ToString();
                                model.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                                model.StartDate = objDataReader["START_DATE"].ToString();
                                model.EndDate = objDataReader["END_DATE"].ToString();
                                model.PromotionPercentage = Convert.ToDouble(objDataReader["PROMOTION_PERCENTAGE"].ToString());
                                model.PreviousPrice = Convert.ToDouble(objDataReader["PREVIOUS_PRICE"].ToString());
                                model.NewPrice = Convert.ToDouble(objDataReader["NEW_PRICE"].ToString());
                                model.Status = objDataReader["STATUS"].ToString();
                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();

                                objPromotionalProduct.Add(model);
                            }
                            return objPromotionalProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> DeletePromotionalProduct(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PROMOTIONAL_PRO_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        #endregion

        #region Occasional Product Section

        public async Task<List<OccasionalProduct>> GetOccasionalProductList(string headOfficeId, string branchOfficeId)
        {
            List<OccasionalProduct> objOccasionalProduct = new List<OccasionalProduct>();

            var sql = "SELECT " +
                      "OCCASIONAL_PRODUCT_ID," +
                      "PRODUCT_ID," +
                      "PRODUCT_NAME," +
                      "SKU," +
                      "PRIMARY_IMAGE " +
                      "FROM VEW_OCCASIONAL_PRODUCT ORDER BY OCCASIONAL_PRODUCT_ID DESC ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        OccasionalProduct model = new OccasionalProduct
                        {
                            ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString()),
                            ProductName = objDataReader["PRODUCT_NAME"].ToString(),
                            Sku = objDataReader["SKU"].ToString(),
                            ProductImage = objDataReader["PRIMARY_IMAGE"].ToString()
                        };
                        objOccasionalProduct.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objOccasionalProduct;
        }

        public async Task<string> SaveOccasionalProduct(OccasionalProduct occasionalProductModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_OCCASIONAL_PRODUCT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_occasional_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = occasionalProductModel.ProductId != 0 ? occasionalProductModel.ProductId.ToString() : null;



            objOracleCommand.Parameters.Add("p_product_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(occasionalProductModel.ProductName) ? occasionalProductModel.ProductName : null;
            objOracleCommand.Parameters.Add("p_product_sku", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(occasionalProductModel.Sku) ? occasionalProductModel.Sku : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(occasionalProductModel.UpdateBy) ? occasionalProductModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(occasionalProductModel.HeadOfficeId) ? occasionalProductModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(occasionalProductModel.BranchOfficeId) ? occasionalProductModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        public async Task<string> DeleteOccasionalProduct(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_OCCASIONAL_PRO_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_product_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        #endregion

        #region Occasional Slider Section
        public async Task<List<OccasionalSliderModel>> GetOccasionalSliderList(string headOfficeId, string branchOfficeId)
        {
            List<OccasionalSliderModel> objSliderModel = new List<OccasionalSliderModel>();

            var sql = "SELECT " +
                        "SLIDER_ID," +
                        "SLIDER_TITLE," +
                        "SLIDER_URL," +
                        "DISPLAY_ORDER," +
                        "SLIDER_IMAGE," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_OCCASIONAL_SLIDER where HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY SLIDER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        OccasionalSliderModel model = new OccasionalSliderModel
                        {
                            SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString()),
                            SliderTitle = objDataReader["SLIDER_TITLE"].ToString(),
                            SliderUrl = objDataReader["SLIDER_URL"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            SliderImageSiring = objDataReader["SLIDER_IMAGE"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSliderModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSliderModel;
        }
        public async Task<OccasionalSliderModel> GetOccasionalSlider(int sliderId, string headOfficeId, string branchOfficeId)
        {
            OccasionalSliderModel objSliderModel = new OccasionalSliderModel();

            var sql = "SELECT " +
                          "SLIDER_ID," +
                          "SLIDER_TITLE," +
                          "SLIDER_URL," +
                          "DISPLAY_ORDER," +
                          "SLIDER_IMAGE," +
                          "UPDATE_BY," +
                          "HEAD_OFFICE_ID," +
                          "BRANCH_OFFICE_ID " +
                          "FROM VEW_OCCASIONAL_SLIDER where SLIDER_ID = :SLIDER_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY SLIDER_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("SLIDER_ID", sliderId);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSliderModel.SliderId = Convert.ToInt32(objDataReader["SLIDER_ID"].ToString());
                        objSliderModel.SliderTitle = objDataReader["SLIDER_TITLE"].ToString();
                        objSliderModel.SliderUrl = objDataReader["SLIDER_URL"].ToString();
                        objSliderModel.SliderImageSiring = objDataReader["SLIDER_IMAGE"].ToString();
                        objSliderModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objSliderModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSliderModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSliderModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSliderModel;
        }
        public async Task<string> SaveOccasionalSlider(OccasionalSliderModel sliderModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_OCCASIONAL_SLIDER_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_slider_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = sliderModel.SliderId;
            objOracleCommand.Parameters.Add("p_slider_title", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.SliderTitle) ? sliderModel.SliderTitle : null;
            objOracleCommand.Parameters.Add("p_slider_url", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.SliderUrl) ? sliderModel.SliderUrl : null;
            objOracleCommand.Parameters.Add("p_slider_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = sliderModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_slider_image", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.SliderImageSiring) ? sliderModel.SliderImageSiring : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.UpdateBy) ? sliderModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.HeadOfficeId) ? sliderModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(sliderModel.BranchOfficeId) ? sliderModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteOccasionalSlider(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_OCCASIONAL_SLIDER_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_slider_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region Homepage Popup

        public async Task<List<PopupModel>> GetPopupList(string headOfficeId, string branchOfficeId)
        {
            List<PopupModel> objPopupModel = new List<PopupModel>();

            var sql = "SELECT " +
                      "POPUP_ID," +
                        "POPUP_NAME," +
                        "POPUP_IMAGE," +
                        "ACTIVE_YN," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_HOMEPAGE_POPUP where HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        PopupModel model = new PopupModel();

                        model.PopupId = Convert.ToInt32(objDataReader["POPUP_ID"].ToString());
                        model.PopupName = objDataReader["POPUP_NAME"].ToString();
                        model.PopupImageSiring = objDataReader["POPUP_IMAGE"].ToString();

                        model.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";

                        model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                      
                        objPopupModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objPopupModel;
        }

        public async Task<PopupModel> GetPopup(int popupId, string headOfficeId, string branchOfficeId)
        {
            PopupModel objPopupModel = new PopupModel();

            var sql = "SELECT " +
                          "POPUP_ID," +
                          "POPUP_NAME," +
                          "POPUP_IMAGE," +
                          "ACTIVE_YN," +
                          "UPDATE_BY," +
                          "HEAD_OFFICE_ID," +
                          "BRANCH_OFFICE_ID " +
                          "FROM VEW_HOMEPAGE_POPUP where POPUP_ID = :POPUP_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };

            objCommand.Parameters.Add("POPUP_ID", popupId);
            objCommand.Parameters.Add("HEAD_OFFICE_ID", headOfficeId.Trim());
            objCommand.Parameters.Add("BRANCH_OFFICE_ID", branchOfficeId.Trim());

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objPopupModel.PopupId = Convert.ToInt32(objDataReader["POPUP_ID"].ToString());
                        objPopupModel.PopupName = objDataReader["POPUP_NAME"].ToString();
                        objPopupModel.PopupImageSiring = objDataReader["POPUP_IMAGE"].ToString();

                        objPopupModel.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";

                        objPopupModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objPopupModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objPopupModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objPopupModel;
        }

        public async Task<string> SavePopupImage(PopupModel popupModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_HOMEPAGE_POPUP_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_POPUP_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = popupModel.PopupId;
            objOracleCommand.Parameters.Add("P_POPUP_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(popupModel.PopupName) ? popupModel.PopupName : null;
            objOracleCommand.Parameters.Add("P_POPUP_IMAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(popupModel.PopupImageSiring) ? popupModel.PopupImageSiring : null;

            objOracleCommand.Parameters.Add("P_ACTIVE_YN", OracleDbType.Varchar2, ParameterDirection.Input).Value = popupModel.IsActive ? "Y" : "N";            

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(popupModel.UpdateBy) ? popupModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(popupModel.HeadOfficeId) ? popupModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(popupModel.BranchOfficeId) ? popupModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        public async Task<string> DeletePopupImage(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_HOMEPAGE_POPUP_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_POPUP_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        #endregion
    }
}
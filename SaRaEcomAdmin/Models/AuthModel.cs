﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class AuthModel
    {
        [Required]
        public string EmployeeId { get; set; }

        [Required]
        [Display(Name = "Old Password")]
        [DataType(DataType.Password)]
        public string EmployeePassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Re-type password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string RetypePassword { get; set; }

        [Required]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Invalid email format.")]
        [Display(Name = "Email")]
        public string EmployeeEmail { get; set; }

        [Required]
        [Display(Name = "Phone")]
        public string EmployeePhone { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Role")]
        public string EmployeeRole { get; set; }

        [Display(Name = "Designation")]
        public string EmployeeDesignation { get; set; }

        public string EmployeeImage { get; set; }
        public HttpPostedFileBase Image { get; set; }
        [Display(Name = "Area")]
        public string EmployeeArea { get; set; }

        public bool Message { get; set; }

        public IEnumerable<TopReview> TopReviews { get; set; }
        public IEnumerable<TopMessage> TopMessages { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class TopReview
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public string Score { get; set; }
        public DateTime Time { get; set; }
        public string TimeAlert { get; set; }
    }

    public class TopMessage
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public DateTime Time { get; set; }
        public string TimeAlert { get; set; }
    }
}

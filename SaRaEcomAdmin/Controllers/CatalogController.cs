﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.Controllers
{
    public class CatalogController : Controller
    {
        private readonly CatalogDal _catalogDal = new CatalogDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        public async Task<ActionResult> ProductReview()
        {
            CatalogModel model = new CatalogModel();

            model.ApprovedReview = await _catalogDal.GetApprovedReviews();
            model.PendingReview = await _catalogDal.GetPendingReviews();
            model.RejectedReview = await _catalogDal.GetRejectedReviews();

            return View(model);
        }

        public async Task<ActionResult> GetSingleReview(int productReviewId)
        {
            ProductReviewModel model = new ProductReviewModel();
            if (productReviewId != 0)
            {
                model = await _catalogDal.GetProductReview(productReviewId);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ApprovedReview(int productReviewId)
        {
            string message = "";
            if (productReviewId != 0)
            {
                LoadSession();

                message = await _catalogDal.ApproveReview(productReviewId, _strEmployeeId);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> RejectReview(int productReviewId)
        {
            string message = "";
            if (productReviewId != 0)
            {
                LoadSession();

                message = await _catalogDal.RejectReview(productReviewId, _strEmployeeId);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #region Upcoming Product

        public async Task<ActionResult> UpcomingProduct(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            UpcomingProduct model = new UpcomingProduct();

            var objUpcomingProducts = await _catalogDal.GetUpcomingProducts(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.UpcomingProducts = objUpcomingProducts;

            if (id != null && id != 0)
            {
                model = await _catalogDal.GetUpcomingProduct((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveUpcomingProduct(UpcomingProduct upcomingProduct)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                upcomingProduct.UpdateBy = _strEmployeeId;
                upcomingProduct.HeadOfficeId = _strHeadOfficeId;
                upcomingProduct.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/UpcomingProduct/");
                string vFileName = Guid.NewGuid().ToString();

                if (upcomingProduct.ProductId == 0)
                {
                    if (upcomingProduct.Image != null)
                    {
                        var imageExtension = Path.GetExtension(upcomingProduct.Image.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                            {
                                string imagePath = filePath + upcomingProduct.ImageString;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                upcomingProduct.Image.SaveAs(filePath);

                                upcomingProduct.ImageString = vFileName + imageExtension.ToLower();
                                string strMessage = await _catalogDal.SaveUpcomingProduct(upcomingProduct);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            }
                        }
                    }
                    else
                    {
                        TempData["message"] = "Image is required";
                    }
                }
                else
                {
                    var imageExtension = Path.GetExtension(upcomingProduct.Image?.FileName);
                    if (imageExtension != null)
                    {
                        imageExtension = imageExtension.ToUpper();
                        if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                        {
                            string imagePath = filePath + upcomingProduct.ImageString;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }

                            filePath += vFileName;
                            filePath += imageExtension.ToLower();

                            upcomingProduct.Image.SaveAs(filePath);

                            upcomingProduct.ImageString = vFileName + imageExtension.ToLower();
                        }
                        else
                        {
                            TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            return RedirectToAction("UpcomingProduct");
                        }
                    }
                    string strMessage = await _catalogDal.SaveUpcomingProduct(upcomingProduct);
                    TempData["message"] = strMessage;
                }
            }
            return RedirectToAction("UpcomingProduct");
        }

        public async Task<ActionResult> DeleteUpcomingProduct(int id)
        {
            LoadSession();
            string message = await _catalogDal.DeleteUpcomingProduct(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("UpcomingProduct");
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class SalesController : Controller
    {
        private readonly SalesDal _objSalesDal = new SalesDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        #region Orders Section

        public async Task<ActionResult> Orders(string orderNumber)
        {
            var orders = await _objSalesDal.GetOrdersList();

            return View(orders);
        }

        public async Task<ActionResult> OrderDetails(string orderNumber)
        {
            Orders orders = new Orders {OrderedProducts = new List<OrderedProduct>()};

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                orders = await _objSalesDal.GetSingleOrder(orderNumber);
                if (string.IsNullOrWhiteSpace(orders.TotalAmount))
                    orders.TotalAmount = "0";

                if (!string.IsNullOrWhiteSpace(orders.OrderNumber))
                {
                     orders.OrderedProducts = await _objSalesDal.GetOrderedProduct(orders.OrderNumber);
                }
            }
            
            return PartialView("Sales/_OrderDetailsPartial", orders);
        }

        public async Task<ActionResult> OrderConfirm(string orderNumber, string email, string phoneNo)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                message = await _objSalesDal.OrderConfirm(orderNumber);

                new Thread(() =>
                {
                  string body = "Order Confirmation: Thanks for shopping with SaRa! " +
                                "Your order is confirmed, and will be delivered within two working days. " +
                                "Check your status here: (https://www.saralifestyle.com.bd/CustomerProfile/Dashboard), " +
                                "Order Number (" + orderNumber + ")";

                    UtilityClass.SendEmail(email, "Order Confirmation", body, null);

                    UtilityClass.SendSms(phoneNo,body);

                }).Start();
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteOrderItem(string orderNumber, string orderId)
        {
            LoadSession();
            if (string.IsNullOrWhiteSpace(orderId) && string.IsNullOrWhiteSpace(orderNumber))
            {
                return Json(new { success = false, message = "Uuups, something went wrong!" });
            }

            var message = await _objSalesDal.DeleteOrderItem(orderNumber, orderId, _strEmployeeId);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> OrderDelete(string orderNumber, string remarks, string email, string phoneNo)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                LoadSession();
                message = await _objSalesDal.OrderDelete(orderNumber, remarks, _strEmployeeId);

                if (!string.IsNullOrWhiteSpace(message) && !string.IsNullOrWhiteSpace(email))
                {
                    new Thread(() =>
                    {
                        string body = remarks;
                        UtilityClass.SendEmail(email, "Your Order (" + orderNumber + ") has been Cancel", body, null);

                        UtilityClass.SendSms(phoneNo, "Your Order (" + orderNumber + ") has been Cancel on " + DateTime.Now.ToString("f") +", Message:"+ remarks);

                    }).Start();
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Pre - Order Section

        public async Task<ActionResult> PreOrders()
        {
            var preOrders = await _objSalesDal.GetPreOrdersList();

            return View(preOrders);
        }

        public async Task<ActionResult> PreOrderDetails(string orderNumber)
        {
            PreOrders orders = new PreOrders();

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                orders = await _objSalesDal.GetSinglePreOrder(orderNumber);
            }

            return PartialView("Sales/_PreOrderDetailsPartial", orders);
        }

        private string PreOrderConfirmEmail(string image, string date, string name, string sku, string amount, string remarks)
        {
            string body = string.Empty; 
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/PreOrderConfirm.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{image}", image); //replacing the required things  
            body = body.Replace("{date}", Convert.ToDateTime(date).ToString("D"));
            body = body.Replace("{name}", name);
            body = body.Replace("{sku}", sku);
            body = body.Replace("{price}", amount);
            body = body.Replace("{remarks}", remarks);

            return body;
        }

        private string PreOrderRejectEmail(string image, string date, string name, string sku, string amount, string remarks)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/PreOrderReject.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{image}", image); //replacing the required things  
            body = body.Replace("{date}", Convert.ToDateTime(date).ToString("D"));
            body = body.Replace("{name}", name);
            body = body.Replace("{sku}", sku);
            body = body.Replace("{price}", amount);
            body = body.Replace("{remarks}", remarks);

            return body;
        }

        public async Task<ActionResult> PreOrderConfirm(string orderNumber, string remarks, string deliveryDate)
        {
            string message = "";

            LoadSession();

            if (!string.IsNullOrWhiteSpace(orderNumber) && !string.IsNullOrWhiteSpace(remarks) && !string.IsNullOrWhiteSpace(deliveryDate) && !string.IsNullOrWhiteSpace(_strEmployeeId))
            {
                message = await _objSalesDal.PreOrderConfirm(orderNumber, deliveryDate, remarks, _strEmployeeId);

                var orderedProduct = await _objSalesDal.GetSinglePreOrder(orderNumber);

                string mailBody = PreOrderConfirmEmail(Path.Combine("http://saralifestyle.com.bd/Files/UpcomingProduct/", orderedProduct.ProductImage), deliveryDate, orderedProduct.ProductName, orderedProduct.Sku, orderedProduct.ProductPrice, remarks);

                new Thread(() =>
                {
                    UtilityClass.SendEmail(orderedProduct.CustomerEmail, "pre-order success", mailBody, null);

                }).Start();
            }
            else
            {
                message = "lack of information, Please try again";
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> PreOrderReject(string orderNumber, string remarks)
        {
            string message = "";

            LoadSession();

            if (!string.IsNullOrWhiteSpace(orderNumber) && !string.IsNullOrWhiteSpace(remarks) && !string.IsNullOrWhiteSpace(_strEmployeeId))
            {
                message = await _objSalesDal.PreOrderDelete(orderNumber,  remarks, _strEmployeeId);

                var orderedProduct = await _objSalesDal.GetSinglePreOrder(orderNumber);

                string mailBody = PreOrderRejectEmail(Path.Combine("http://saralifestyle.com.bd/Files/UpcomingProduct/", orderedProduct.ProductImage), DateTime.Now.ToString("D"), orderedProduct.ProductName, orderedProduct.Sku, orderedProduct.ProductPrice, remarks);

                new Thread(() =>
                {
                    UtilityClass.SendEmail(orderedProduct.CustomerEmail, "pre-order Reject", mailBody, null);

                }).Start();
            }
            else
            {
                message = "lack of information, Please try again";
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delivery Section

        public async Task<ActionResult> DeliveredProducts()
        {
            var deliveredProduct = await _objSalesDal.GetDeliveryList();

            return View(deliveredProduct);
        }

        public async Task<ActionResult> DeliveryDetails(string orderNumber)
        {
            Orders delivery = new Orders {OrderedProducts = new List<OrderedProduct>()};

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                delivery = await _objSalesDal.GetSingleDelivery(orderNumber);
                if (string.IsNullOrWhiteSpace(delivery.TotalAmount))
                    delivery.TotalAmount = "0";

                if (!string.IsNullOrWhiteSpace(delivery.OrderNumber))
                {
                    delivery.OrderedProducts = await _objSalesDal.GetOrderedProduct(delivery.OrderNumber);
                }
            }

            return PartialView("Sales/_DeliveryDetailsPartial", delivery);
        }

        public async Task<ActionResult> ReturnConfirm(string orderNumber, string email, string phoneNo)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                message = await _objSalesDal.ReturnConfirm(orderNumber);

                new Thread(() =>
                {
                    string body = "Your order (" + orderNumber + ") has been returned on " + DateTime.Now.ToString("f");
                    UtilityClass.SendEmail(email, "Your Order has been returned", body, null);

                    UtilityClass.SendSms(phoneNo, "Your Order (" + orderNumber + ") has been returned on " + DateTime.Now.ToString("f"));

                }).Start();
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Return Section

        public async Task<ActionResult> ReturnProducts()
        {
            var deliveredProduct = await _objSalesDal.GetReturnList();

            return View(deliveredProduct);
        }

        public async Task<ActionResult> ReturnDetails(string orderNumber)
        {
            Orders returnProduct = new Orders {OrderedProducts = new List<OrderedProduct>()};

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                returnProduct = await _objSalesDal.GetSingleReturn(orderNumber);

                if (string.IsNullOrWhiteSpace(returnProduct.TotalAmount))
                    returnProduct.TotalAmount = "0";

                if (!string.IsNullOrWhiteSpace(returnProduct.OrderNumber))
                {
                    returnProduct.OrderedProducts = await _objSalesDal.GetOrderedProduct(returnProduct.OrderNumber);
                }
            }

            return PartialView("Sales/_ReturnDetailsPartial", returnProduct);
        }

        #endregion

        #region Return Request Section

        public async Task<ActionResult> ReturnRequest()
        {
            var returnRequestList = await _objSalesDal.GetReturnRequestList();

            return View(returnRequestList);
        }

        public async Task<ActionResult> ReturnRequestDetails(string returnRequestId)
        {
            Orders orders = new Orders {OrderedProducts = new List<OrderedProduct>()};

            if (!string.IsNullOrWhiteSpace(returnRequestId))
            {
                orders = await _objSalesDal.GetSingleReturnRequest(returnRequestId);

                if (orders.ReturnRequestId > 0)
                {
                    orders.OrderedProducts = await _objSalesDal.GetReturnRequestedProduct(orders.ReturnRequestId);
                }
            }

            return PartialView("Sales/_returnRequestPartial", orders);
        }

        public async Task<ActionResult> ReturnRequestConfirm(string returnRequestId)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(returnRequestId))
            {
                LoadSession();
                message = await _objSalesDal.ReturnRequestConfirm(returnRequestId, _strEmployeeId);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ReturnRequestReject(string returnRequestId)
        {
            string message = "";

            if (!string.IsNullOrWhiteSpace(returnRequestId))
            {
                LoadSession();
                message = await _objSalesDal.ReturnRequestReject(returnRequestId, _strEmployeeId);

                //if (!string.IsNullOrWhiteSpace(message) && !string.IsNullOrWhiteSpace(email))
                //{
                //    new Thread(() =>
                //    {
                //        string body = remarks;
                //        UtilityClass.SendEmail(email, "Your Order (" + orderNumber + ") is Cancel", body, null);

                //    }).Start();
                //}
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Cancel Order

        public async Task<ActionResult> CancelOrder()
        {
            var orders = await _objSalesDal.GetCancelList();

            return View(orders);
        }

        public async Task<ActionResult> RejectDetails(string orderNumber)
        {
            Orders rejectProduct = new Orders { OrderedProducts = new List<OrderedProduct>() };

            if (!string.IsNullOrWhiteSpace(orderNumber))
            {
                rejectProduct = await _objSalesDal.GetSingleReject(orderNumber);

                if (string.IsNullOrWhiteSpace(rejectProduct.TotalAmount))
                    rejectProduct.TotalAmount = "0";

                if (!string.IsNullOrWhiteSpace(rejectProduct.OrderNumber))
                {
                    rejectProduct.OrderedProducts = await _objSalesDal.GetOrderedProduct(rejectProduct.OrderNumber);
                }
            }

            return PartialView("Sales/_RejectDetailsPartial", rejectProduct);
        }

        #endregion
    }
}
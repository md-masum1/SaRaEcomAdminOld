﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class AccountModel
    {
        public IEnumerable<InboxModel> GetMessageList { get; set; }
        public IEnumerable<InboxModel> GetTrashList { get; set; }
        public IEnumerable<Compose> GetSentMessage { get; set; }
    }

    public class InboxModel
    {
        public int ContactId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string Message { get; set; }
        public string ShortMessage { get; set; }
        public string Date { get; set; }
        public string ReadStatus { get; set; }
    }

    public class Compose
    {
        public string MessageId { get; set; }

        public string ToEmail { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }

        public string ContactId { get; set; }
        public string CustomerId { get; set; }

        public string SentDate { get; set; }
        public string Sender { get; set; }
        public string ShortMessage { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
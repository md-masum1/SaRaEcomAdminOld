﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class SetupModel
    {
    }

    public class CategoryModel
    {
        public int CategoryId { get; set; }

        [Required]
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        public string UpdateBy { get; set; }

        public string HeadOfficeId { get; set; }

        public string BranchOfficeId { get; set; }
    }

    public class SubCategoryModel
    {
        [Required]
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }

        public int SubCategoryId { get; set; }
        [Required]
        [DisplayName("Sub Category Name")]
        public string SubCategoryName { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        public string UpdateBy { get; set; }

        public string HeadOfficeId { get; set; }

        public string BranchOfficeId { get; set; }
    }

    public class SubSubCategoryModel
    {
        [Required]
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }

        [Required]
        [DisplayName("Sub Category")]
        public int SubCategoryId { get; set; }
        [DisplayName("Sub Category Name")]
        public string SubCategoryName { get; set; }

        public int SubSubCategoryId { get; set; }
        [Required]
        [DisplayName("Sub Sub Category Name")]
        public string SubSubCategoryName { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        public string UpdateBy { get; set; }

        public string HeadOfficeId { get; set; }

        public string BranchOfficeId { get; set; }
    }

    public class SizeModel
    {
        public int SizeId { get; set; }

        [Required]
        [DisplayName("Size Name")]
        public string SizeName { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class RatioModel
    {
        public int RatioId { get; set; }

        [Required]
        [DisplayName("Ratio Name")]
        public string RatioName { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class ColorModel
    {
        public int ColorId { get; set; }

        [Required]
        [DisplayName("Color Name")]
        public string ColorName { get; set; }

        [Required]
        [DisplayName("Color")]
        public string ColorCode { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class FabricModel
    {
        public int FabricId { get; set; }

        [Required]
        [DisplayName("Fabric Name")]
        public string FabricName { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class MeasurementUnitModel
    {
        public int UnitId { get; set; }

        [Required]
        [DisplayName("Measurement Unit Name")]
        public string UnitName { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaRaEcomAdmin.Controllers
{
    public class WebContentController : Controller
    {
        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        public ActionResult CookiePolicy()
        {
            return View();
        }

        public ActionResult ReturnPolicy()
        {
            return View();
        }

        public ActionResult Cancellation()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        public ActionResult HowToOrder()
        {
            return View();
        }

        public ActionResult BillingAndPayments()
        {
            return View();
        }

        public ActionResult ShippingAndDelivery()
        {
            return View();
        }

        public ActionResult TrackYourOrders()
        {
            return View();
        }

        public ActionResult ReturnAndExchanges()
        {
            return View();
        }

        public ActionResult AboutSaRa()
        {
            return View();
        }

        public ActionResult OurPeople()
        {
            return View();
        }

        public ActionResult OurValues()
        {
            return View();
        }

        public ActionResult NewsAndEvents()
        {
            return View();
        }

        public ActionResult PhotoGallery()
        {
            return View();
        }

        public ActionResult VideoGallery()
        {
            return View();
        }
    }
}
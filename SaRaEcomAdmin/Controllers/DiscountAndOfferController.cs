﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.Controllers
{
    public class DiscountAndOfferController : Controller
    {
        private readonly DiscountAndOfferDal _dal = new DiscountAndOfferDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public ActionResult LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
                return null;
            }
            return RedirectToAction("Index", "Auth");
        }
        #endregion

        public async Task<ActionResult> PrivilegeCard(int? id)
        {
            PrivilegeCardModel models = new PrivilegeCardModel();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"] as string;
            }

            if (id != null && id > 0)
            {
                models = await _dal.GetPrivilegeCard((int)id);
            }

            ViewBag.CardList = await _dal.GetPrivilegeCardList();
            return View(models);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PrivilegeCardSave(PrivilegeCardModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["message"] = "All Field Is required.";
                return RedirectToAction("PrivilegeCard", "DiscountAndOffer");
            }

            LoadSession();
            model.UpdateBy = _strEmployeeId;
            model.HeadOfficeId = _strHeadOfficeId;
            model.BranchOfficeId = _strBranchOfficeId;

            var message = await _dal.SavePrivilegeCard(model);
            TempData["message"] = message;

            return RedirectToAction("PrivilegeCard", "DiscountAndOffer");
        }

        public async Task<ActionResult> PrivilegeCardDelete(int id)
        {
            if (id > 0)
            {
                LoadSession();
                var message = await _dal.DeletePrivilegeCard(id, _strHeadOfficeId, _strBranchOfficeId);
                TempData["message"] = message;
                return RedirectToAction("PrivilegeCard", "DiscountAndOffer");
            }
            TempData["message"] = "Invalid action, Please try again.";
            return RedirectToAction("PrivilegeCard", "DiscountAndOffer");
        }
    }
}
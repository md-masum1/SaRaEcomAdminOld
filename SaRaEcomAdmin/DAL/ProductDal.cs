﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;

namespace SaRaEcomAdmin.DAL
{
    public class ProductDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Dropdown
        public async Task<DataTable> GetFabric()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "FABRIC_ID, " +
                               "FABRIC_NAME " +
                               "FROM L_FABRIC order by FABRIC_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetMeasurementUnit()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "UNIT_ID, " +
                               "UNIT_NAME " +
                               "FROM L_MEASUREMENT_UNIT order by UNIT_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetCategory()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "CATEGORY_ID, " +
                               "CATEGORY_NAME " +
                               "FROM L_CATEGORY order by CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetSubCategory(int categoryId)
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "SUB_CATEGORY_ID, " +
                               "SUB_CATEGORY_NAME " +
                               "FROM L_SUB_CATEGORY where CATEGORY_ID = :CATEGORY_ID order by SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetSubSubCategory(int categoryId, int subCategoryId)
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "SUB_SUB_CATEGORY_ID, " +
                               "SUB_SUB_CATEGORY_NAME " +
                               "FROM L_SUB_SUB_CATEGORY where CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID order by SUB_SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);
            objCommand.Parameters.Add("SUB_CATEGORY_ID", subCategoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetColor()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "COLOR_ID, " +
                               "COLOR_NAME " +
                               "FROM L_COLOR order by COLOR_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        #endregion
    }
}
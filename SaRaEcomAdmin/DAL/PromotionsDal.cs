﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class PromotionsDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Discount
        public async Task<IEnumerable<Discount>> GetDiscountList(string headOfficeId, string branchOfficeId)
        {
            const string sql = "SELECT " +
                                "DISCOUNT_ID," +
                                "DISCOUNT_NAME," +
                                "DISCOUNT_TYPE," +
                                "CATEGORY_ID," +
                                "CATEGORY_NAME," +
                                "COUPONE_CODE," +
                                "TO_CHAR(START_DATE, 'dd/mm/yyyy') START_DATE," +
                                "TO_CHAR(END_DATE, 'dd/mm/yyyy') END_DATE," +
                                "DISCOUNT_PERCENTAGE," +
                                "DISCOUNT_AMOUNT," +
                                "ACTIVE_YN," +
                                "UPDATE_BY," +
                                "HEAD_OFFICE_ID," +
                                "BRANCH_OFFICE_ID " +
                                "FROM VEW_DISCOUNT WHERE HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY DISCOUNT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = headOfficeId;
                    objCommand.Parameters.Add(":BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = branchOfficeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Discount> objDiscount = new List<Discount>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Discount model = new Discount();

                                model.DiscountId = Convert.ToInt32(objDataReader["DISCOUNT_ID"].ToString());
                                model.DiscountName = objDataReader["DISCOUNT_NAME"].ToString();
                                model.DiscountType = objDataReader["DISCOUNT_TYPE"].ToString();
                                model.CategoryId = objDataReader["CATEGORY_ID"].ToString();
                                model.CategoryName = objDataReader["CATEGORY_NAME"].ToString();
                                model.DiscountPercentage = Convert.ToInt32(objDataReader["DISCOUNT_PERCENTAGE"].ToString());
                                model.DiscountAmount = Convert.ToDouble(objDataReader["DISCOUNT_AMOUNT"].ToString());
                                model.CouponCode = objDataReader["COUPONE_CODE"].ToString();
                                model.StartDate = objDataReader["START_DATE"].ToString();
                                model.EndDate = objDataReader["END_DATE"].ToString();
                                model.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";

                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();

                                objDiscount.Add(model);
                            }
                            return objDiscount;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Discount> GetDiscount(int discountId, string headOfficeId, string branchOfficeId)
        {
            const string sql = "SELECT " +
                                "DISCOUNT_ID," +
                                "DISCOUNT_NAME," +
                                "DISCOUNT_TYPE," +
                                "CATEGORY_ID," +
                                "COUPONE_CODE," +
                                "TO_CHAR(START_DATE, 'dd/mm/yyyy') START_DATE," +
                                "TO_CHAR(END_DATE, 'dd/mm/yyyy') END_DATE," +
                                "DISCOUNT_PERCENTAGE," +
                                "DISCOUNT_AMOUNT," +
                                "ACTIVE_YN," +
                                "UPDATE_BY," +
                                "HEAD_OFFICE_ID," +
                                "BRANCH_OFFICE_ID " +
                                "FROM VEW_DISCOUNT WHERE DISCOUNT_ID = :DISCOUNT_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":DISCOUNT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = discountId;
                    objCommand.Parameters.Add(":HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = headOfficeId;
                    objCommand.Parameters.Add(":BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = branchOfficeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            Discount model = new Discount();

                            while (await objDataReader.ReadAsync())
                            {
                                model.DiscountId = Convert.ToInt32(objDataReader["DISCOUNT_ID"].ToString());
                                model.DiscountName = objDataReader["DISCOUNT_NAME"].ToString();
                                model.DiscountType = objDataReader["DISCOUNT_TYPE"].ToString();
                                model.CategoryId = objDataReader["CATEGORY_ID"].ToString();
                                model.DiscountPercentage = Convert.ToInt32(objDataReader["DISCOUNT_PERCENTAGE"].ToString());
                                model.DiscountAmount = Convert.ToDouble(objDataReader["DISCOUNT_AMOUNT"].ToString());
                                model.CouponCode = objDataReader["COUPONE_CODE"].ToString();
                                model.StartDate = objDataReader["START_DATE"].ToString();
                                model.EndDate = objDataReader["END_DATE"].ToString();
                                model.IsActive = objDataReader["ACTIVE_YN"].ToString() == "Y";

                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<DataTable> GetCategory()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "CATEGORY_ID, " +
                               "CATEGORY_NAME " +
                               "FROM L_CATEGORY order by CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }


        public async Task<string> DiscountSave(Discount objDiscount)
        {
            string returnMessage = "";
            OracleCommand objOracleCommand = new OracleCommand("PRO_DISCOUNT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_DISCOUNT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = objDiscount.DiscountId;

            objOracleCommand.Parameters.Add("P_DISCOUNT_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.DiscountName) ? objDiscount.DiscountName : null;

            objOracleCommand.Parameters.Add("P_DISCOUNT_TYPE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.DiscountType) ? objDiscount.DiscountType : null;
            objOracleCommand.Parameters.Add("P_CATEGORY_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.CategoryId) ? objDiscount.CategoryId : null;
            objOracleCommand.Parameters.Add("P_COUPONE_CODE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.CouponCode) ? objDiscount.CouponCode : null;
            objOracleCommand.Parameters.Add("P_START_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.StartDate) ? objDiscount.StartDate : null;
            objOracleCommand.Parameters.Add("P_END_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.EndDate) ? objDiscount.EndDate : null;
            objOracleCommand.Parameters.Add("P_DISCOUNT_PERCENTAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value =objDiscount.DiscountPercentage != null && objDiscount.DiscountPercentage != 0? objDiscount.DiscountPercentage: 0;
            objOracleCommand.Parameters.Add("P_DISCOUNT_AMOUNT", OracleDbType.Varchar2, ParameterDirection.Input).Value = objDiscount.DiscountAmount != null && objDiscount.DiscountAmount > 0? objDiscount.DiscountAmount: 0;
            objOracleCommand.Parameters.Add("P_ACTIVE_YN", OracleDbType.Varchar2, ParameterDirection.Input).Value = objDiscount.IsActive ? "Y" : "N";

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.UpdateBy) ? objDiscount.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.HeadOfficeId) ? objDiscount.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objDiscount.BranchOfficeId) ? objDiscount.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    returnMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return returnMessage;
        }
        #endregion
    }
}
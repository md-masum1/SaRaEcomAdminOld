﻿$(document).ready(function ($) {

    //Cascading Dropdown Ajax Request Start
    //For SubCategory Data
    $("#CategoryId").change(function () {
        $("#SubCategoryId").empty();
        $("#SubSubCategoryId").empty();
        $("#mappingSave").prop("disabled", true);
        var catId = $("#CategoryId").val();
        if (!isNaN(catId) && catId) {
            $("#SubCategoryId").prop("disabled", false);
            $.ajax({
                type: 'POST',
                url: '/ProductInventory/GetSubCategory/',
                dataType: 'json',
                data: { categoryId: $("#CategoryId").val() },
                success: function (subCategoryId) {
                    $.each(subCategoryId, function (i, subCategoryId) {
                        $("#SubCategoryId").append('<option value="' + subCategoryId.Value + '">' + subCategoryId.Text + '</option>');
                    });
                },
                error: function (ex) { $.alert('Failed to retrieve SubCategory List.' + ex); }
            });
        } else {
            $("#SubCategoryId").prop("disabled", true);
            $("#SubSubCategoryId").prop("disabled", true);
            $("#mappingSave").prop("disabled", true);
        }
    });

    //For SubSubCategory Data
    $("#SubCategoryId").change(function () {
        $("#SubSubCategoryId").empty();
        var subCatId = $("#SubCategoryId").val();
        $("#mappingSave").prop("disabled", true);
        if (!isNaN(subCatId) && subCatId) {
            $("#SubSubCategoryId").prop("disabled", false);
            $.ajax({
                type: 'POST',
                url: '/ProductInventory/GetSubSubCategory/',
                dataType: 'json',
                data: { categoryId: $("#CategoryId").val(), subCategoryId: $("#SubCategoryId").val() },
                success: function (subSubCategoryId) {
                    $.each(subSubCategoryId, function (i, subSubCategoryId) {
                        $("#SubSubCategoryId").append('<option value="' + subSubCategoryId.Value + '">' + subSubCategoryId.Text + '</option>');
                    });
                },
                error: function (ex) { $.alert('Failed to retrieve SubSubCategory List.' + ex); }
            });
        } else {
            $("#SubSubCategoryId").prop("disabled", true);
            $("#mappingSave").prop("disabled", true);
        }
    });

    $("#SubSubCategoryId").change(function () {
        var subCatId = $("#SubSubCategoryId").val();
        if (!isNaN(subCatId) && subCatId) {
            $("#mappingSave").prop("disabled", false);
        } else {
            $("#mappingSave").prop("disabled", true);
        }
    });
    //Cascading Dropdown Ajax Request End


    //Product Info Section

    //For Primery Image
    function newReadUrl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).parents('.fileUploadm').siblings('.show-image-divm').find('.show-prev-imgm').attr('src', e.target.result);
                $("#displayImage").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.imageUploadm', function () {
        newReadUrl(this);
    });

    //discount
    //$("#Discount").keyup(function () {
    //    var discount = $("#Discount").val();
    //    if (discount) {
    //        $("#DiscountStarTime").prop("disabled", false);
    //        $("#DiscountEndTime").prop("disabled", false);
    //    } else {
    //        $("#DiscountStarTime").prop("disabled", true);
    //        $("#DiscountStarTime").val("");
    //        $("#DiscountEndTime").prop("disabled", true);
    //        $("#DiscountEndTime").val("");
    //    }
    //});
    //Product Info Section End



    //Image And Color Section Start

    //Get DropDown  Color
    $.ajax({
        type: 'POST',
        url: '/ProductInventory/GetColor',
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i, data) {
                $("#ColorId").append('<option value="' + data.Value + '">' + data.Text + '</option>');
            });
        },
        error: function (ex) { $.alert('Failed to retrieve ColorId List.' + ex); }
    });
    //Color DropDown End

    //For Image 1
    function newReadUrl1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#showImage1").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '#Image1Base', function () {
        newReadUrl1(this);
    });

    //For Image 2
    function newReadUrl2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#showImage2").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '#Image2Base', function () {
        newReadUrl2(this);
    });

    //For Image 3
    function newReadUrl3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#showImage3").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '#Image3Base', function () {
        newReadUrl3(this);
    });

    //For Image 4
    function newReadUrl4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#showImage4").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '#Image4Base', function () {
        newReadUrl4(this);
    });

    //Save Image And Color
    var colorId = "";
    var image1 = "";
    var image2 = "";
    var image3 = "";
    var image4 = "";

    function colorChange() {
        $("#Image1Base").val("");
        $("#Image2Base").val("");
        $("#Image3Base").val("");
        $("#Image4Base").val("");

        $('#showImage1').removeAttr('src');
        $('#showImage2').removeAttr('src');
        $('#showImage3').removeAttr('src');
        $('#showImage4').removeAttr('src');
    }

    function saveColorImage() {
        var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
        var productId = $('#ProductId').val();
        colorId = $('#ColorId option:selected').val();
        var displayOrder = $('#DisplayOrder').val();
        var imageId = $('#ImageId').val();

        var formData = new FormData();
        formData.append("ProductId", productId);
        formData.append("ColorId", colorId);
        formData.append("DisplayOrder", displayOrder);
        formData.append("ImageId", imageId);

        var file1;
        var file2;
        var file3;
        var file4;

        if ($("#Image1Base")[0].files[0] == null && $('#showImage1').attr('src') != null) {
            var src1 = $('#showImage1').attr('src').split('/');
            file1 = src1[src1.length - 1];
            formData.append("ImageString1", file1);
        } else {
            image1 = $("#Image1Base")[0].files[0];
            formData.append("Image1Base", image1);
            if (image1 != null) {
                var fileType1 = image1["type"];
                if (!$.inArray(fileType1, validImageTypes) > 0) {
                    $.dialog({
                        title: 'Required!',
                        content: 'image 1 required or image must be jpg / jpeg / png.'
                    });
                }
            }
            
        }
        if ($("#Image2Base")[0].files[0] == null && $('#showImage2').attr('src') != null) {
            var src2 = $('#showImage2').attr('src').split('/');
            file2 = src2[src2.length - 1];
            formData.append("ImageString2", file2);
        } else {
            image2 = $("#Image2Base")[0].files[0];
            if (image2 != null) {
                formData.append("Image2Base", image2);
                var fileType2 = image2["type"];
                if (!$.inArray(fileType2, validImageTypes) > 0) {
                    $.dialog({
                        title: 'Required!',
                        content: 'image 2 required or image must be jpg / jpeg / png.'
                    });
                }
            }
            
        }
        if ($("#Image3Base")[0].files[0] == null && $('#showImage3').attr('src') != null) {
            var src3 = $('#showImage3').attr('src').split('/');
            file3 = src3[src3.length - 1];
            formData.append("ImageString3", file3);
        } else {
            image3 = $("#Image3Base")[0].files[0];
            formData.append("Image3Base", image3);
            if (image3 != null) {
                var fileType3 = image3["type"];
                if (!$.inArray(fileType3, validImageTypes) > 0) {
                    $.dialog({
                        title: 'Required!',
                        content: 'image 3 required or image must be jpg / jpeg / png.'
                    });
                }
            }
            
        }
        if ($("#Image4Base")[0].files[0] == null && $('#showImage4').attr('src') != null) {
            var src4 = $('#showImage4').attr('src').split('/');
            file4 = src4[src4.length - 1];
            formData.append("ImageString4", file4);
        } else {
            image4 = $("#Image4Base")[0].files[0];
            formData.append("Image4Base", image4);
            if (image4 != null) {
                var fileType4 = image4["type"];
                if (!$.inArray(fileType4, validImageTypes) > 0) {
                    $.dialog({
                        title: 'Required!',
                        content: 'image 4 required or image must be jpg / jpeg / png.'
                    });
                }
            }
            
        }

        if (!isNaN(productId) && productId && !isNaN(colorId) && colorId) {
            if (!isNaN(displayOrder) && displayOrder) {
                if ((image1 != null || file1 != null) && (image2 != null || file2 != null) && (image3 != null || file3 != null) && (image4 != null || file4 != null)) {
                                    $.ajax({
                                        type: 'post',
                                        url: '/ProductInventory/SaveImageAndColor/',
                                        data: formData,
                                        dataType: 'json',
                                        contentType: false,
                                        processData: false,
                                        success: function (response) {
                                            //alert('succes!!');
                                            if (response.success) {
                                                $("#productPicSection").load(" #productPicSection > *");
                                                table.ajax.reload();
                                                $('#ColorId').val('');
                                                $('#DisplayOrder').val('');
                                                $.dialog({
                                                    title: 'Success!',
                                                    content: 'save successfully done'
                                                });
                                            } else {
                                                $.dialog({
                                                    title: 'Error!',
                                                    content: response
                                                });
                                            }
                                        },
                                        error: function (error) {
                                            $.dialog({
                                                title: 'Error!',
                                                content: 'something unexpected happened please contact with development team!!!'
                                            });
                                        }
                                    });
                } else {
                    $.dialog({
                        title: 'Required!',
                        content: 'All image required or image must be jpg / jpeg / png.'
                    });
                }
            } else {
                $.dialog({
                    title: 'Required!',
                    content: 'Display Order Required.'
                });
            }
            
        } else {
            $.dialog({
                title: 'Required!',
                content: 'Color Required.'
            });
        }
    }

    $(document).on('click', '#SaveColorAndImage', function () {
        saveColorImage();
    });

    $(document).on('change', '#ColorId', function () {
        colorChange();
    });

    var table = $("#color-grid").DataTable({
        "lengthMenu": [[5, 10, 15], [5, 10, 15]],
        "processing": true,
        "ordering": false,
        "ajax": {
            url: "/ProductInventory/LoadImageGrid/",
            data: { id: $('#ProductId').val() },
            type: 'POST'
        },
        "columns": [
            {
                "data": "Image1",
                render: function (data, type, image1) {
                    return "<img style='width: 50px;height: 50px;border: 1px solid #ddd;padding: 3px;'src='" + image1.Image1 + "'/>";
                }
            },
            {
                "data": "Image2",
                render: function (data, type, image2) {
                    return "<img style='width: 50px;height: 50px;border: 1px solid #ddd;padding: 3px;'src='" + image2.Image2 + "'/>";
                }
            },
            {
                "data": "Image3",
                render: function (data, type, image3) {
                    return "<img style='width: 50px;height: 50px;border: 1px solid #ddd;padding: 3px;'src='" + image3.Image3 + "'/>";
                }
            },
            {
                "data": "Image4",
                render: function (data, type, image4) {
                    return "<img style='width: 50px;height: 50px;border: 1px solid #ddd;padding: 3px;'src='" + image4.Image4 + "'/>";
                }
            },
            { "data": "ProductName" },
            { "data": "Sku" },
            { "data": "ColorName" },
            { "data": "DisplayOrder" },
            {
                "data": "ProductImageId",
                render: function (data, type, productImageGrid) {
                    return "<button type='button' id='EditImage' class='btn btn-sm image-edit' data-productid='" +
                        productImageGrid.ProductId +
                        "' data-colorid='" +
                        productImageGrid.ColorId +
                        "'><i class='ti-trash-o'></i>Delete</button> ";
                    //return "<button type='button' id='EditImage' class='btn btn-sm image-edit' data-productid='" + productImageGrid.ProductId + "' data-colorid='" + productImageGrid.ColorId + "'><i class='ti-trash-o'></i>Delete</button> || " +
                    //    "<button type='button' id='DeleteImage' class='btn btn-sm image-edit' data-displayorder='" + productImageGrid.DisplayOrder + "' data-image1='" + productImageGrid.Image1 + "' data-image2='" + productImageGrid.Image2 + "' data-image3='" + productImageGrid.Image3 + "' data-image4='" + productImageGrid.Image4 + "' data-colorid='" + productImageGrid.ColorId + "'data-imageid='" + productImageGrid.ProductImageId + "'><i class='ti-trash'></i>Edit</button>";

                }
            }
        ]
    });
    //Image Grid End

    //Edit Image
    function deleteProductImage(displayOrder, colorId, imageId, image1, image2, image3, image4) {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to edit this item???',
            buttons: {
                confirm: function () {
                    $("#ColorId").val(colorId);
                    $("#ImageId").val(imageId);
                    $("#DisplayOrder").val(displayOrder);
                    $("#showImage1").attr("src", image1);
                    $("#showImage2").attr("src", image2);
                    $("#showImage3").attr("src", image3);
                    $("#showImage4").attr("src", image4);
                },
                cancel: function () {
                    $.dialog({
                        title: 'Confirm!',
                        content: 'Operation cancel.'
                    });
                }
            }
        });
    }

    function editProductImage(productId, colorId) {

        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure you want to delete this item???',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: 'POST',
                        url: '/ProductInventory/DeleteProductImage/',
                        data: { productId: productId, colorId: colorId },
                        dataType: 'json',
                        success: function (data) {
                            table.ajax.reload();
                            $("#productPicSection").load(" #productPicSection > *");
                            $('#ColorId').val('');
                            $('#DisplayOrder').val('');
                            $.dialog({
                                title: 'Confirm!',
                                content: data
                            });
                        },
                        error: function (ex) {
                            $.dialog({
                                title: 'Error!',
                                content: ex
                            });
                        }
                    });
                },
                cancel: function () {
                    $.dialog({
                        title: 'Confirm!',
                        content: 'Operation cancel.'
                    });
                }
            }
        });
    }

    $(document).on('click', '#EditImage', function () {
        var productId = $(this).data("productid");
        var colorId = $(this).data("colorid");
        editProductImage(productId, colorId);
    });
    $(document).on('click', '#DeleteImage', function () {
        var colorId = $(this).data("colorid");
        var imageId = $(this).data("imageid");
        var displayOrder = $(this).data("displayorder");
        var image1 = $(this).data("image1");
        var image2 = $(this).data("image2");
        var image3 = $(this).data("image3");
        var image4 = $(this).data("image4");
        deleteProductImage(displayOrder, colorId, imageId, image1, image2, image3, image4);
    });
    //Edit Image End

    //Image Upload End


    //Size Panel Start
    function loadColorForSize() {
        $.ajax({
            type: 'POST',
            url: '/ProductInventory/GetProductColor/',
            data: { id: $('#ProductId').val() },
            dataType: 'json',
            success: function (data) {
                $('#ColorForSize').empty();
                $.each(data, function (i, data) {
                    $("#ColorForSize").append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            },
            error: function(ex) {
                $.dialog({
                    title: 'Error!',
                    content: 'Failed to retrieve Color List.' + ex
                });
            }
        });
    }

    function loadSizeList() {
        $.ajax({
            type: 'POST',
            url: '/ProductInventory/GetSize/',
            dataType: 'json',
            success: function (data) {
                $('.size-id').empty();
                $.each(data, function (i, data) {
                    $(".size-id").append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            },
            error: function(ex) {
                $.dialog({
                    title: 'Error!',
                    content: 'Failed to retrieve Size List.' + ex
                });
            }
        });
    }

    function inventorySummaryGrid() {
        $.ajax({
            type: 'POST',
            url: '/ProductInventory/InventorySummary/',
            data: { id: $('#ProductId').val() },
            dataType: 'json',
            success: function (data) {
                // $("#InventorySummary").empty();
                $("#InventorySummary > tbody > tr").html("");
                $("#InventorySummary tbody tr").append("<td><img src=" + data.ImageString + " style='width: 50px; height: 50px; border: 1px solid #ddd; padding: 3px;'></td>");
                $("#InventorySummary tbody tr").append("<td>" + data.ProductName + "</td>");
                $("#InventorySummary tbody tr").append("<td>" + data.Sku + "</td>");
                $("#InventorySummary tbody tr").append("<td>" + data.TotalColor + "</td>");
                $("#InventorySummary tbody tr").append("<td>" + data.TotalSize + "</td>");
                $("#InventorySummary tbody tr").append("<td>" + data.TotalQuantity + "</td>");
                $("#InventorySummary tbody tr").append("<td><button type='button' class='btn btn-secondary btn-sm' data-toggle='modal' data-target='#modalForInventory'>Details</button></td>");
            },
            error: function(ex) {
                $.dialog({
                    title: 'Error!',
                    content: ex
                });
            }
        });
    }

    function colorCount() {
        $.ajax({
            type: 'post',
            url: '/ProductInventory/ColorCount/',
            data: { id: $('#ProductId').val() },
            dataType: 'json',
            beforeSend: function () {
                $('.loading-image').hide();
            },
            complete: function () {
                $('.loading-image').hide();
            },
            success: function (response) {
                //alert('succes!!');
                if (!isNaN(response) && response > 0) {
                    $('.sizeDivMsg').hide();
                    $('.sizeDiv').show();

                    loadColorForSize();
                    loadSizeList();
                    inventorySummaryGrid();
                    $("#modalForInventory").load(" #modalForInventory > *");
                }
            }
        });
    }

    function sizePanel() {
        colorCount();
    }

    $(document).on('click', '#size-tab', function () {
        sizePanel();
    });

    function saveSizeAndQuantity() {
        var entryDate = $("#inputDate").val();
        var productId = $('#ProductId').val();
        var colorId = $('#ColorForSize option:selected').val();

        var sizesId = [];
        var sizesValue = [];
        var pricesValue = [];

        $('.GridFields').each(function () {
            var sizeId = $(this).find(".size-id option:selected").val();
            var sizeValue = $(this).find(".quantity-value").val();
            var priceValue = $(this).find(".price-value").val();

            if (sizeId && sizeValue) {
                sizesId.push(sizeId);
                sizesValue.push(sizeValue);
                pricesValue.push(priceValue);
            }
        });

        if (productId) {
            if (colorId) {
                if (entryDate) {
                    if (sizesId.length > 0 && sizesValue.length > 0) {
                        if (sizesId.length === sizesValue.length) {
                            $.ajax({
                                type: 'post',
                                url: '/ProductInventory/SaveSizeAndQuantity',
                                data: { ProductId: productId, ColorId: colorId, ProductEntryDate: entryDate, SizeIdList: sizesId, QuantityList: sizesValue, PriceList: pricesValue },
                                success: function (response) {
                                    $('.size-id').val("");
                                    $('#ColorForSize').val("");
                                    $(".quantity-value").val("");
                                    $(".price-value").val("");
                                    inventorySummaryGrid();
                                    $("#modalForInventory").load(" #modalForInventory > *");
                                    $('.GridFields:gt( 0 )').remove();
                                    $.dialog({
                                        title: 'Success!',
                                        content: 'Save Successful'
                                    });
                                }
                            });
                        } else {
                            $.dialog({
                                title: 'Required!',
                                content: 'Every Size must have an value.'
                            });
                        }
                    } else {
                        $.dialog({
                            title: 'Required!',
                            content: 'Size and Quantity is Required.'
                        });
                    }
                } else {
                    $.dialog({
                        title: 'Required!',
                        content: 'Entry Date Can not By Null.'
                    });
                }
            } else {
                $.dialog({
                    title: 'Required!',
                    content: 'color is required.'
                });
            }
        }
        inventorySummaryGrid();
    }

    $(document).on('click', '#SaveSizeAndQty', function () {
        saveSizeAndQuantity();
    });

    //Size Panel End


    //Ratio Panel Start
    function loadRatioList() {
        $.ajax({
            type: 'POST',
            url: '/ProductInventory/GetRatio/',
            dataType: 'json',
            success: function (data) {
                $('#RatioForSize').empty();
                $.each(data, function (i, data) {
                    $("#RatioForSize").append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            },
            error: function (ex) { $.alert('Failed to retrieve Ratio List.' + ex); }
        });
    }

    function loadSizeForSizeRatio() {
        $.ajax({
            type: 'POST',
            url: '/ProductInventory/GetProductSize/',
            data: { id: $('#ProductId').val() },
            dataType: 'json',
            success: function (data) {
                $('.size-for-ratio').empty();
                $.each(data, function (i, data) {
                    $(".size-for-ratio").append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
            },
            error: function (ex) { $.alert('Failed to retrieve Size List.' + ex); }
        });
    }

    function saveSizeAndRatio() {
        var productId = $('#ProductId').val();
        var ratioId = $('#RatioForSize option:selected').val();

        var sizesId = [];
        var sizesValue = [];

        $('.GridSizeFields').each(function () {
            var sizeId = $(this).find(".size-for-ratio option:selected").val();
            var sizeValue = $(this).find(".size-value").val();

            if (sizeId) {
                sizesId.push(sizeId);
            }

            if (sizeValue) {
                sizesValue.push(sizeValue);
            }
        });

        if (productId) {
            if (ratioId) {
                if (sizesId.length > 0 && sizesValue.length > 0) {
                    if (sizesId.length === sizesValue.length) {

                        $.ajax({
                            type: 'post',
                            url: '/ProductInventory/SaveSizeAndRatio',
                            data: { ProductId: productId, RatioId: ratioId, SizeIdList: sizesId, SizeValueList: sizesValue },
                            success: function (response) {
                                if (response.success) {
                                    $.alert(response.strMessageRatio);
                                    $('.size-for-ratio').val("");
                                    $('#RatioForSize').val("");
                                    $(".size-value").val("");
                                    $('.GridSizeFields:gt( 0 )').remove();
                                    $("#RatioChart").load(" #RatioChart > *");
                                } else {
                                    $.alert(response.responseText);
                                }
                                //InventorySummaryGrid();

                            },
                            error: function (ex) {
                                $.alert("error");
                            }
                        });

                    } else { $.alert("evry size must have an value"); }
                } else { $.alert("Size and Length is required"); }
            } else { $.alert("Ratio Is Not Selected"); }
        } else {
            $.alert("Unexpected Error");
            window.location.href = '/ProductInventory/Index';
        }
    }

    $(document).on('click', '#SaveSizeAndRatio', function () {
        saveSizeAndRatio();
    });

    function sizeCount() {
        $.ajax({
            type: 'post',
            url: '/ProductInventory/SizeCount/',
            data: { id: $('#ProductId').val() },
            dataType: 'json',
            success: function (response) {
                //alert('succes!!');
                if (!isNaN(response) && response > 0) {
                    $('.ratioDivMsg').hide();
                    $('.ratioDiv').show();

                    loadSizeForSizeRatio();
                    loadRatioList();
                }
            }
        });
    }

    function ratioPanel() {
        sizeCount();
        //SizeRatioGrid();
    }

    $(document).on('click', '#ratio-tab', function () {
        ratioPanel();
    });

    //Ratio Panel End
});
﻿using System.Threading.Tasks;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using System.IO;
using SaRaEcomAdmin.Utility;
using System;

namespace SaRaEcomAdmin.Controllers
{
    public class ConfigurationController : Controller
    {
        private readonly ConfigurationDal _objConfigurationDal = new ConfigurationDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        #region Slider

        public async Task<ActionResult> Slider(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            SliderModel model = new SliderModel();

            var objSliderModel = await _objConfigurationDal.GetSliderList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SliderList = objSliderModel;

            if (id != null && id != 0)
            {
                model = await _objConfigurationDal.GetSlider((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSlider(SliderModel objSliderModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objSliderModel.UpdateBy = _strEmployeeId;
                objSliderModel.HeadOfficeId = _strHeadOfficeId;
                objSliderModel.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/Slider/");
                string vFileName = Guid.NewGuid().ToString();

                if (objSliderModel.SliderId == 0)
                {
                    if (objSliderModel.SliderImage != null)
                    {
                        var imageExtension = Path.GetExtension(objSliderModel.SliderImage.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                            {
                                string imagePath = filePath + objSliderModel.SliderImageSiring;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                objSliderModel.SliderImage.SaveAs(filePath);

                                objSliderModel.SliderImageSiring = vFileName + imageExtension.ToLower();
                                string strMessage = await _objConfigurationDal.SaveSlider(objSliderModel);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            }
                        }
                    }
                    else
                    {
                        TempData["message"] = "Image is required";
                    }
                }
                else
                {
                    var imageExtension = Path.GetExtension(objSliderModel.SliderImage?.FileName);
                    if (imageExtension != null)
                    {
                        imageExtension = imageExtension.ToUpper();
                        if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                        {
                            string imagePath = filePath + objSliderModel.SliderImageSiring;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }

                            filePath += vFileName;
                            filePath += imageExtension.ToLower();

                            objSliderModel.SliderImage.SaveAs(filePath);

                            objSliderModel.SliderImageSiring = vFileName + imageExtension.ToLower();
                        }
                        else
                        {
                            TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            return RedirectToAction("Slider");
                        }
                    }
                    string strMessage = await _objConfigurationDal.SaveSlider(objSliderModel);
                    TempData["message"] = strMessage;
                }
            }
            return RedirectToAction("Slider");
        }

        public async Task<ActionResult> DeleteSlider(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeleteSlider(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Slider");
        }

        #endregion

        #region Offer

        public async Task<ActionResult> Offer(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            OfferModel model = new OfferModel();

            var objOfferModel = await _objConfigurationDal.GetOfferList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SliderList = objOfferModel;

            if (id != null && id != 0)
            {
                model = await _objConfigurationDal.GetOffer((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveOffer(OfferModel offerModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                offerModel.UpdateBy = _strEmployeeId;
                offerModel.HeadOfficeId = _strHeadOfficeId;
                offerModel.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/Offer/");
                string vFileName = Guid.NewGuid().ToString();

                if (offerModel.OfferId == 0)
                {
                    if (offerModel.OfferImage != null)
                    {
                        var imageExtension = Path.GetExtension(offerModel.OfferImage.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG" || imageExtension == ".GIF")
                            {
                                string imagePath = filePath + offerModel.OfferImageSiring;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                offerModel.OfferImage.SaveAs(filePath);

                                offerModel.OfferImageSiring = vFileName + imageExtension.ToLower();
                                string strMessage = await _objConfigurationDal.SaveSlider(offerModel);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG of GIF";
                            }
                        }
                    }
                    else
                    {
                        TempData["message"] = "Image is required";
                    }
                }
                else
                {
                    var imageExtension = Path.GetExtension(offerModel.OfferImage?.FileName);
                    if (imageExtension != null)
                    {
                        imageExtension = imageExtension.ToUpper();
                        if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG" || imageExtension == ".GIF")
                        {
                            string imagePath = filePath + offerModel.OfferImageSiring;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }

                            filePath += vFileName;
                            filePath += imageExtension.ToLower();

                            offerModel.OfferImage.SaveAs(filePath);

                            offerModel.OfferImageSiring = vFileName + imageExtension.ToLower();
                        }
                        else
                        {
                            TempData["message"] = "Image Must be .JPG or .JPEG or .PNG of GIF";
                            return RedirectToAction("Offer");
                        }
                    }
                    string strMessage = await _objConfigurationDal.SaveSlider(offerModel);
                    TempData["message"] = strMessage;
                }
            }
            return RedirectToAction("Offer");
        }

        public async Task<ActionResult> DeleteOffer(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeleteOffer(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Offer");
        }

        #endregion

        #region Banner

        public async Task<ActionResult> Banner(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            BannerModel model = new BannerModel();

            var objBannerModel = await _objConfigurationDal.GetBannerList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.BannerList = objBannerModel;

            if (id != null && id != 0)
            {
                model = await _objConfigurationDal.GetBanner((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveBanner(BannerModel objBannerModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                if (objBannerModel.BannerId < 4 && objBannerModel.BannerId > 0)
                {
                    objBannerModel.UpdateBy = _strEmployeeId;
                    objBannerModel.HeadOfficeId = _strHeadOfficeId;
                    objBannerModel.BranchOfficeId = _strBranchOfficeId;

                    string filePath = Server.MapPath("~/Files/Banner/");
                    string vFileName = Guid.NewGuid().ToString();

                    if (objBannerModel.BannerImage != null)
                    {
                        var imageExtension = Path.GetExtension(objBannerModel.BannerImage.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                            {
                                string imagePath = filePath + objBannerModel.BannerImageSiring;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                objBannerModel.BannerImage.SaveAs(filePath);

                                objBannerModel.BannerImageSiring = vFileName + imageExtension.ToLower();
                                string strMessage = await _objConfigurationDal.SaveBanner(objBannerModel);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            }
                        }
                    }
                    else
                    {
                        string strMessage = await _objConfigurationDal.SaveBanner(objBannerModel);
                        TempData["message"] = strMessage;
                    }
                }
                else
                {
                    TempData["message"] = "Banner can't be Added, Only Edit is Available";
                }
            }
            return RedirectToAction("Banner");
        }

        #endregion

        #region Trending Product

        public async Task<ActionResult> TrendingProduct()
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            TrendingProductModel model = new TrendingProductModel();

            var objTrendingProductModel = await _objConfigurationDal.GetTrendingProductList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.TrendingList = objTrendingProductModel;
            ViewBag.ProductListDropdown = UtilityClass.GetSelectListByDataTable(_objConfigurationDal.GetProductListDropdown(), "PRODUCT_ID", "PRODUCT_CODE");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveTrendingProduct(TrendingProductModel objTrendingProduct)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objTrendingProduct.UpdateBy = _strEmployeeId;
                objTrendingProduct.HeadOfficeId = _strHeadOfficeId;
                objTrendingProduct.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objConfigurationDal.SaveTrendingProduct(objTrendingProduct);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("TrendingProduct");
        }

        public async Task<ActionResult> DeleteTrendingProduct(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeleteTrendingProduct(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("TrendingProduct");
        }

        #endregion

        #region Promotional Product

        public async Task<ActionResult> PromotionalProducts(int? productId)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"] as string;
            }
            PromotionalProductModel model = new PromotionalProductModel();

            var promotionalProducts = await _objConfigurationDal.GetPromotionalProducts();
            ViewBag.PromotionalProduct = promotionalProducts;

            if (productId != null && productId != 0)
            {
                model = await _objConfigurationDal.GetPromotionalProduct((int)productId);
            }

            ViewBag.ProductListDropdown = UtilityClass.GetSelectListByDataTable(_objConfigurationDal.ProductListBySku(), "PRODUCT_ID", "product_code");
            return View(model);
        }

        public async Task<ActionResult> SavePromotionalProduct(PromotionalProductModel objPromotionalProduct)
        {
            string message = null;
            if (ModelState.IsValid)
            {
                LoadSession();

                objPromotionalProduct.UpdateBy = _strEmployeeId;
                objPromotionalProduct.HeadOfficeId = _strHeadOfficeId;
                objPromotionalProduct.BranchOfficeId = _strBranchOfficeId;

                if (objPromotionalProduct.PromotionPercentage < 100 && objPromotionalProduct.PromotionPercentage >= 0)
                {
                    message =await _objConfigurationDal.SavePromotionalProduct(objPromotionalProduct);
                }
                else
                {
                    message = "Something went wrong, please try again!";
                }
            }

            TempData["message"] = message;
            return RedirectToAction("PromotionalProducts", "Configuration");
        }

        public async Task<ActionResult> DeletePromotionalProduct(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeletePromotionalProduct(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("PromotionalProducts", "Configuration");
        }

        #endregion

        #region Occasional Product

        public async Task<ActionResult> OccasionalProduct()
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            OccasionalProduct model = new OccasionalProduct();

            var objOccasionalProductModel = await _objConfigurationDal.GetOccasionalProductList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.OccasionalList = objOccasionalProductModel;
            ViewBag.ProductListDropdown = UtilityClass.GetSelectListByDataTable(_objConfigurationDal.GetProductListDropdown(), "PRODUCT_ID", "PRODUCT_CODE");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveOccasionalProduct(OccasionalProduct occasionalProduct)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                occasionalProduct.UpdateBy = _strEmployeeId;
                occasionalProduct.HeadOfficeId = _strHeadOfficeId;
                occasionalProduct.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objConfigurationDal.SaveOccasionalProduct(occasionalProduct);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("OccasionalProduct");
        }

        public async Task<ActionResult> DeleteOccasionalProduct(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeleteOccasionalProduct(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("OccasionalProduct");
        }

        #endregion

        #region Occasional Slider

        public async Task<ActionResult> OccasionalSlider(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            OccasionalSliderModel model = new OccasionalSliderModel();

            var objSliderModel = await _objConfigurationDal.GetOccasionalSliderList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SliderList = objSliderModel;

            if (id != null && id != 0)
            {
                model = await _objConfigurationDal.GetOccasionalSlider((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveOccasionalSlider(OccasionalSliderModel sliderModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                sliderModel.UpdateBy = _strEmployeeId;
                sliderModel.HeadOfficeId = _strHeadOfficeId;
                sliderModel.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/Slider/");
                string vFileName = Guid.NewGuid().ToString();

                if (sliderModel.SliderId == 0)
                {
                    if (sliderModel.SliderImage != null)
                    {
                        var imageExtension = Path.GetExtension(sliderModel.SliderImage.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                            {
                                string imagePath = filePath + sliderModel.SliderImageSiring;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                sliderModel.SliderImage.SaveAs(filePath);

                                sliderModel.SliderImageSiring = vFileName + imageExtension.ToLower();
                                string strMessage = await _objConfigurationDal.SaveOccasionalSlider(sliderModel);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            }
                        }
                    }
                    else
                    {
                        TempData["message"] = "Image is required";
                    }
                }
                else
                {
                    var imageExtension = Path.GetExtension(sliderModel.SliderImage?.FileName);
                    if (imageExtension != null)
                    {
                        imageExtension = imageExtension.ToUpper();
                        if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                        {
                            string imagePath = filePath + sliderModel.SliderImageSiring;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }

                            filePath += vFileName;
                            filePath += imageExtension.ToLower();

                            sliderModel.SliderImage.SaveAs(filePath);

                            sliderModel.SliderImageSiring = vFileName + imageExtension.ToLower();
                        }
                        else
                        {
                            TempData["message"] = "Image Must be .JPG or .JPEG or .PNG";
                            return RedirectToAction("Slider");
                        }
                    }
                    string strMessage = await _objConfigurationDal.SaveOccasionalSlider(sliderModel);
                    TempData["message"] = strMessage;
                }
            }
            return RedirectToAction("OccasionalSlider");
        }

        public async Task<ActionResult> DeleteOccasionalSlider(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeleteOccasionalSlider(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("OccasionalSlider");
        }

        #endregion

        #region Homepage Popup

        public async Task<ActionResult> HomePagePopup(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            PopupModel model = new PopupModel();

            var objPopupModel = await _objConfigurationDal.GetPopupList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.PopupList = objPopupModel;

            if (id != null && id != 0)
            {
                model = await _objConfigurationDal.GetPopup((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveHomePagePopup(PopupModel popupModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                popupModel.UpdateBy = _strEmployeeId;
                popupModel.HeadOfficeId = _strHeadOfficeId;
                popupModel.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/Popup/");
                string vFileName = Guid.NewGuid().ToString();

                if (popupModel.PopupId == 0)
                {
                    if (popupModel.PopupImage != null)
                    {
                        var imageExtension = Path.GetExtension(popupModel.PopupImage.FileName);
                        if (imageExtension != null)
                        {
                            imageExtension = imageExtension.ToUpper();
                            if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG" || imageExtension == ".GIF")
                            {
                                string imagePath = filePath + popupModel.PopupImageSiring;
                                if (System.IO.File.Exists(imagePath))
                                {
                                    System.IO.File.Delete(imagePath);
                                }

                                filePath += vFileName;
                                filePath += imageExtension.ToLower();

                                popupModel.PopupImage.SaveAs(filePath);

                                popupModel.PopupImageSiring = vFileName + imageExtension.ToLower();
                                string strMessage = await _objConfigurationDal.SavePopupImage(popupModel);
                                TempData["message"] = strMessage;
                            }
                            else
                            {
                                TempData["message"] = "Image Must be .JPG or .JPEG or .PNG of .GIF";
                            }
                        }
                    }
                    else
                    {
                        TempData["message"] = "Image is required";
                    }
                }
                else
                {
                    var imageExtension = Path.GetExtension(popupModel.PopupImage?.FileName);
                    if (imageExtension != null)
                    {
                        imageExtension = imageExtension.ToUpper();
                        if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG" || imageExtension == ".GIF")
                        {
                            string imagePath = filePath + popupModel.PopupImageSiring;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }

                            filePath += vFileName;
                            filePath += imageExtension.ToLower();

                            popupModel.PopupImage.SaveAs(filePath);

                            popupModel.PopupImageSiring = vFileName + imageExtension.ToLower();
                        }
                        else
                        {
                            TempData["message"] = "Image Must be .JPG or .JPEG or .PNG of .GIF";
                            return RedirectToAction("HomePagePopup");
                        }
                    }
                    string strMessage = await _objConfigurationDal.SavePopupImage(popupModel);
                    TempData["message"] = strMessage;
                }
            }
            return RedirectToAction("HomePagePopup");
        }

        public async Task<ActionResult> DeleteHomePagePopup(int id)
        {
            LoadSession();
            string message = await _objConfigurationDal.DeletePopupImage(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("HomePagePopup");
        }

        #endregion

    }
}
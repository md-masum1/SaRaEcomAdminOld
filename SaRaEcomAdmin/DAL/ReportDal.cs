﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class ReportDal
    {
        private object objReportDocument;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

 
        public DataSet SaleInformation(ReportModel objSaleReport)
        {

            try
            {

                DataSet ds = null;
                DataTable dt = new DataTable();
                try
                {
                    string where = "";
                    string inWordTaka = "";

                 
                    if(!string.IsNullOrWhiteSpace(objSaleReport.CategoryId) && !string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId) && !string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                    {
                        inWordTaka = "   'In Word : ' " +
                                    "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                                     "       FROM VEW_RPT_SALE_INFO " +
                                      "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                                        "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                                          "       AND CATEGORY_ID = '" + objSaleReport.CategoryId + "' " +
                                           "      AND SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "' " +
                                            "     AND SUB_SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "' ) " +
                                   "   || ' Only' ";
                    }
                    else if (!string.IsNullOrWhiteSpace(objSaleReport.CategoryId) && !string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId) && string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                    {
                        inWordTaka = "  'In Word : ' " +
                                     " || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                                      "      FROM VEW_RPT_SALE_INFO " +
                                        "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                                        "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                                          "       AND CATEGORY_ID = '" + objSaleReport.CategoryId + "' " +
                                           "      AND SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "'  ) " +
                                     " || ' Only' ";
                    }
                    else if (!string.IsNullOrWhiteSpace(objSaleReport.CategoryId) && string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId) && string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                    {
                        inWordTaka = "   'In Word : ' " +
                                    "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                                     "       FROM VEW_RPT_SALE_INFO " +
                                      "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                                        "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                                           "      AND CATEGORY_ID =  '" + objSaleReport.CategoryId + "' ) " +
                                    "  || ' Only' ";
                    }
                    else if (string.IsNullOrWhiteSpace(objSaleReport.CategoryId) && string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId) && string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                    {
                        inWordTaka = "   'In Word : ' " +
                                    "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                                     "       FROM VEW_RPT_SALE_INFO " +
                                      "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                                        "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') )" +
                                 "     || ' Only' ";
                    }
                    else
                    {
                        inWordTaka = "      'In Word : ' " +
                                    "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                                     "       FROM VEW_RPT_SALE_INFO " +
                                      "     WHERE 1 = 1) " +
                                     " || ' Only' ";
                    }

                    var sql = "SELECT " +
                                "OFFICE_NAME," +
                                "'Sale History '|| 'From  '|| to_date( '" + objSaleReport.FormDate + "', 'dd/mm/yyyy') || ' to ' || to_date('" + objSaleReport.ToDate + "', 'dd/mm/yyyy')  RPT_TITLE, " +

                                "DELIVERY_HISTORY_ID," +
                                "ORDER_NUMBER," +
                                "DELEVERY_DATE," +
                                "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "CATEGORY_ID," +
                               "CATEGORY_NAME," +
                               "SUB_CATEGORY_ID," +
                               "SUB_CATEGORY_NAME," +
                               "SUB_SUB_CATEGORY_ID," +
                               "SUB_SUB_CATEGORY_NAME," +
                                "DESIGNER_ID," +
                                "DESIGNER_NAME," +
                                "MERCHANDISER_ID," +
                                "MERCHANDISER_NAME," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE," +
                                "TOTAL_AMOUNT," +
                                "PRODUCT_DESCRIPTION," +

                                inWordTaka + " PAYMENT_AMOUNT_IN_WORD_TOTAL " +

                               //                              "  CASE " +
                               //  "  WHEN LENGTH(TO_DATE ('" + objSaleReport.FormDate + "', 'dd/mm/yyyy')) >= 8 " +
                               //   "      AND '" + objSaleReport.CategoryId + "' <> '' " +
                               //    "     AND '" + objSaleReport.SubCategoryId + "' <> ''  " +
                               //     "    AND '" + objSaleReport.SubSubCategoryId + "' <> ''  " +
                               //   " THEN " +
                               //    "   'In Word : ' " +
                               //     "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                               //      "       FROM VEW_RPT_SALE_INFO " +
                               //       "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                               //         "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                               //           "       AND CATEGORY_ID = '" + objSaleReport.CategoryId + "' " +
                               //            "      AND SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "' " +
                               //             "     AND SUB_SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "' ) " +
                               //    "   || ' Only' " +
                               //  "  WHEN LENGTH(TO_DATE ('" + objSaleReport.FormDate + "', 'dd/mm/yyyy')) >= 8 " +
                               //   "      AND '" + objSaleReport.CategoryId + "' <> '' " +
                               //    "     AND '" + objSaleReport.SubCategoryId + "' <> ''  " +
                               //     "    AND '" + objSaleReport.SubSubCategoryId + "' = ''  " +
                               //    " THEN " +
                               //     "  'In Word : ' " +
                               //      " || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                               //       "      FROM VEW_RPT_SALE_INFO " +
                               //         "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                               //         "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                               //           "       AND CATEGORY_ID = '" + objSaleReport.CategoryId + "' " +
                               //            "      AND SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "'  ) " +
                               //      " || ' Only' " +
                               //   " WHEN LENGTH(TO_DATE ('" + objSaleReport.FormDate + "', 'dd/mm/yyyy')) >= 8 " +
                               //    "      AND '" + objSaleReport.CategoryId + "' <> '' " +
                               //    "     AND '" + objSaleReport.SubCategoryId + "' = ''  " +
                               //     "    AND '" + objSaleReport.SubSubCategoryId + "' = ''  " +
                               //   " THEN " +
                               //    "   'In Word : ' " +
                               //     "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                               //      "       FROM VEW_RPT_SALE_INFO " +
                               //       "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                               //         "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                               //            "      AND CATEGORY_ID =  '" + objSaleReport.CategoryId + "' ) " +
                               //     "  || ' Only' " +
                               //   " WHEN LENGTH(TO_DATE ('" + objSaleReport.FormDate + "', 'dd/mm/yyyy')) >= 8 " +
                               //   "      AND '" + objSaleReport.CategoryId + "' = '' " +
                               //    "     AND '" + objSaleReport.SubCategoryId + "' = ''  " +
                               //     "    AND '" + objSaleReport.SubSubCategoryId + "' = ''  " +
                               //   " THEN " +
                               //    "   'In Word : ' " +
                               //     "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                               //      "       FROM VEW_RPT_SALE_INFO " +
                               //       "     WHERE DELEVERY_DATE BETWEEN TO_DATE('" + objSaleReport.FormDate + "', 'dd/mm/yyyy') " +
                               //         "                           AND TO_DATE('" + objSaleReport.ToDate + "', 'dd/mm/yyyy') " +
                               //  "     || ' Only' " +
                               //   " ELSE " +
                               //    "      'In Word : ' " +
                               //     "  || (SELECT func_number_to_word(SUM(NVL(TOTAL_AMOUNT, 0))) " +
                               //      "       FROM VEW_RPT_SALE_INFO " +
                               //       "     WHERE 1 = 1) " +
                               //      " || ' Only' " +
                               //" END  PAYMENT_AMOUNT_IN_WORD_TOTAL " +

                     //"(SELECT func_number_to_word(SUM(TOTAL_AMOUNT)) FROM VEW_RPT_SALE_INFO WHERE 1=1 "+ where +") PAYMENT_AMOUNT_IN_WORD_TOTAL " +


                    "from VEW_RPT_SALE_INFO rs where 1=1 ";


                        if (!string.IsNullOrWhiteSpace(objSaleReport.FormDate) && !string.IsNullOrWhiteSpace(objSaleReport.ToDate))
                        {
                            sql = sql + "and DELEVERY_DATE BETWEEN  to_date('" + objSaleReport.FormDate.Trim() + "', 'dd/mm/yyyy') AND  to_date('" + objSaleReport.ToDate.Trim() + "' , 'dd/mm/yyyy')  ";
                            where = "and DELEVERY_DATE BETWEEN  to_date('" + objSaleReport.FormDate.Trim() + "', 'dd/mm/yyyy') AND  to_date('" + objSaleReport.ToDate.Trim() + "' , 'dd/mm/yyyy')  ";

                        }

                        //if (!string.IsNullOrEmpty(objSaleReport.CategoryId))
                        //{
                        //    sql = sql + "and department_id = '" + objEmployeeReportModel.DepartmentId + "' ";

                        //}
                        if (!string.IsNullOrWhiteSpace(objSaleReport.CategoryId))
                        {
                            sql = sql + "and CATEGORY_ID = '" + objSaleReport.CategoryId + "' ";
                            where = where + "and CATEGORY_ID = '" + objSaleReport.CategoryId + "' ";
                        }
                        if (!string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId))
                        {
                            sql = sql + "and SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "' ";
                            where = where + "and SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "' ";
                        }
                        if (!string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                        {
                            sql = sql + "and SUB_SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "' ";
                            where = where + "and SUB_SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "' ";
                        }
                        if (!string.IsNullOrWhiteSpace(objSaleReport.DesignerName))
                        {
                            sql = sql + "and DESIGNER_ID = '" + objSaleReport.DesignerName + "' ";
                            where = where + "and DESIGNER_ID = '" + objSaleReport.DesignerName + "' ";
                        }
                        if (!string.IsNullOrWhiteSpace(objSaleReport.MerchandiserName))
                        {
                            sql = sql + "and MERCHANDISER_ID = '" + objSaleReport.MerchandiserName + "' ";
                            where = where + "and MERCHANDISER_ID = '" + objSaleReport.MerchandiserName + "' ";
                        }

                    


                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.UnitId))
                    //{
                    //    sql = sql + "and unit_id = '" + objEmployeeReportModel.UnitId + "' ";

                    //}

                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.SectionId))
                    //{
                    //    sql = sql + "and section_id = '" + objEmployeeReportModel.SectionId + "' ";

                    //}

                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.SubSectionId))
                    //{
                    //    sql = sql + "and sub_section_id = '" + objEmployeeReportModel.SubSectionId + "' ";

                    //}



                    OracleCommand objOracleCommand = new OracleCommand(sql);
                    using (OracleConnection strConn = GetConnection())
                    {
                        try
                        {
                            objOracleCommand.Connection = strConn;
                            strConn.Open();
                            var objDataAdapter = new OracleDataAdapter(objOracleCommand);
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "VEW_RPT_SALE_INFO");
                            objDataAdapter.Dispose();
                            objOracleCommand.Dispose();
                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);

                        }

                        finally
                        {

                            strConn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataSet ProductInfoByEmployee(ReportModel objSaleReport)
        {

            try
            {

                DataSet ds = null;
                DataTable dt = new DataTable();
                try
                {

                    var sql = "SELECT " +
                                 "OFFICE_NAME," +
                                 "'Product Information by Employee Wise '|| 'From  '|| to_date( '" + objSaleReport.FormDate + "', 'dd/mm/yyyy') || ' to ' || to_date('" + objSaleReport.ToDate + "', 'dd/mm/yyyy')  RPT_TITLE, " +

                                 "PRODUCT_ID," +
                    "PRODUCT_NAME," +
                    "SKU," +
                    "PRODUCT_CODE," +
                    "CATEGORY_ID," +
                    "CATEGORY_NAME," +
                    "SUB_CATEGORY_ID," +
                    "SUB_CATEGORY_NAME," +
                    "SUB_SUB_CATEGORY_ID," +
                    "SUB_SUB_CATEGORY_NAME," +
                    "UNIT_ID," +
                    "UNIT_NAME," +
                    "FABRIC_ID," +
                    "FABRIC_NAME," +
                    "PRIMARY_IMAGE," +
                    "PRIMARY_IMAGE_URL," +
                    "SHORT_DESCRIPTION," +
                    "LONG_DESCRIPTION," +
                    "PURCHASE_AMOUNT," +
                    "SELL_AMOUNT," +
                    "INCLUDE_VAT," +
                    "DESIGNER_ID," +
                    "MARCHANDISER_ID," +
                    "ACTIVE_YN," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID," +
                    "CREATE_BY," +
                    "CREATE_BY_NAME," +
                    "CREATE_DATE," +
                    "TOTAL_COUNT " +
                                 "from VEW_RPT_PRODUCT_BY_EMPLOYEE where 1=1 ";


                    if (!string.IsNullOrWhiteSpace(objSaleReport.FormDate) && !string.IsNullOrWhiteSpace(objSaleReport.ToDate))
                    {
                        sql = sql + "and TRUNC(CREATE_DATE) BETWEEN  to_date('" + objSaleReport.FormDate.Trim() + "', 'dd/mm/yyyy') AND  to_date('" + objSaleReport.ToDate.Trim() + "' , 'dd/mm/yyyy')  ";

                    }

                    //if (!string.IsNullOrEmpty(objSaleReport.CategoryId))
                    //{
                    //    sql = sql + "and department_id = '" + objEmployeeReportModel.DepartmentId + "' ";

                    //}
                    if (!string.IsNullOrWhiteSpace(objSaleReport.CategoryId))
                    {
                        sql = sql + "and CATEGORY_ID = '" + objSaleReport.CategoryId + "' ";
                    }
                    if (!string.IsNullOrWhiteSpace(objSaleReport.SubCategoryId))
                    {
                        sql = sql + "and SUB_CATEGORY_ID = '" + objSaleReport.SubCategoryId + "' ";
                    }
                    if (!string.IsNullOrWhiteSpace(objSaleReport.SubSubCategoryId))
                    {
                        sql = sql + "and SUB_SUB_CATEGORY_ID = '" + objSaleReport.SubSubCategoryId + "' ";
                    }

                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.UnitId))
                    //{
                    //    sql = sql + "and unit_id = '" + objEmployeeReportModel.UnitId + "' ";

                    //}

                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.SectionId))
                    //{
                    //    sql = sql + "and section_id = '" + objEmployeeReportModel.SectionId + "' ";

                    //}

                    //if (!string.IsNullOrEmpty(objEmployeeReportModel.SubSectionId))
                    //{
                    //    sql = sql + "and sub_section_id = '" + objEmployeeReportModel.SubSectionId + "' ";

                    //}



                    OracleCommand objOracleCommand = new OracleCommand(sql);
                    using (OracleConnection strConn = GetConnection())
                    {
                        try
                        {
                            objOracleCommand.Connection = strConn;
                            strConn.Open();
                            var objDataAdapter = new OracleDataAdapter(objOracleCommand);
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "VEW_RPT_PRODUCT_BY_EMPLOYEE");
                            objDataAdapter.Dispose();
                            objOracleCommand.Dispose();
                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);

                        }

                        finally
                        {

                            strConn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataSet RejectedOrderList(ReportModel objSaleReport)
        {

            try
            {

                DataSet ds = null;
                DataTable dt = new DataTable();
                try
                {

                    var sql = "SELECT * from VEW_RPT_ORDER_REJECT_LIST where 1=1 ";


                    if (!string.IsNullOrWhiteSpace(objSaleReport.FormDate) && !string.IsNullOrWhiteSpace(objSaleReport.ToDate))
                    {
                        sql = sql + "and TRUNC(ORDER_DATE) BETWEEN  to_date('" + objSaleReport.FormDate.Trim() + "', 'dd/mm/yyyy') AND  to_date('" + objSaleReport.ToDate.Trim() + "' , 'dd/mm/yyyy')  ";

                    }



                    OracleCommand objOracleCommand = new OracleCommand(sql);
                    using (OracleConnection strConn = GetConnection())
                    {
                        try
                        {
                            objOracleCommand.Connection = strConn;
                            strConn.Open();
                            var objDataAdapter = new OracleDataAdapter(objOracleCommand);
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "VEW_RPT_ORDER_REJECT_LIST");
                            objDataAdapter.Dispose();
                            objOracleCommand.Dispose();
                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);

                        }

                        finally
                        {

                            strConn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataSet RejectedOrderSuccessPaymentList(ReportModel objSaleReport)
        {

            try
            {

                DataSet ds = null;
                DataTable dt = new DataTable();
                try
                {

                    var sql = "SELECT * from VEW_RPT_REJECT_SUCCESS_ORDER where 1=1 ";


                    if (!string.IsNullOrWhiteSpace(objSaleReport.FormDate) && !string.IsNullOrWhiteSpace(objSaleReport.ToDate))
                    {
                        sql = sql + "and TRUNC(ORDER_DATE) BETWEEN  to_date('" + objSaleReport.FormDate.Trim() + "', 'dd/mm/yyyy') AND  to_date('" + objSaleReport.ToDate.Trim() + "' , 'dd/mm/yyyy')  ";

                    }



                    OracleCommand objOracleCommand = new OracleCommand(sql);
                    using (OracleConnection strConn = GetConnection())
                    {
                        try
                        {
                            objOracleCommand.Connection = strConn;
                            strConn.Open();
                            var objDataAdapter = new OracleDataAdapter(objOracleCommand);
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "VEW_RPT_REJECT_SUCCESS_ORDER");
                            objDataAdapter.Dispose();
                            objOracleCommand.Dispose();
                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);

                        }

                        finally
                        {

                            strConn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataSet RegisteredCustomer()
        {
            try
            {

                DataSet ds = null;
                DataTable dt = new DataTable();
                try
                {

                    var sql = "SELECT * from VEW_RPT_REGISTERED_CUSTOMER where 1=1 ";



                    OracleCommand objOracleCommand = new OracleCommand(sql);
                    using (OracleConnection strConn = GetConnection())
                    {
                        try
                        {
                            objOracleCommand.Connection = strConn;
                            strConn.Open();
                            var objDataAdapter = new OracleDataAdapter(objOracleCommand);
                            dt.Clear();
                            ds = new System.Data.DataSet();
                            objDataAdapter.Fill(ds, "VEW_RPT_REGISTERED_CUSTOMER");
                            objDataAdapter.Dispose();
                            objOracleCommand.Dispose();
                        }

                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);

                        }

                        finally
                        {

                            strConn.Close();
                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public async Task<DataTable> GetMerchandiser()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "EMPLOYEE_ID, " +
                               "EMPLOYEE_NAME " +
                               "FROM VEW_MERCHANDISER ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetDesigner()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "EMPLOYEE_ID, " +
                               "EMPLOYEE_NAME " +
                               "FROM VEW_DESIGNER ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

        public async Task<DataTable> GetCategory()
        {
            DataTable dt = new DataTable();
            const string sql = "SELECT " +
                               "CATEGORY_ID, " +
                               "CATEGORY_NAME " +
                               "FROM L_CATEGORY order by CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }

                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetSubCategoryList(int categoryId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "CATEGORY_ID, " +
                         "SUB_CATEGORY_ID, " +
                         "SUB_CATEGORY_NAME " +
                         "FROM L_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID  order by SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        public async Task<DataTable> GetSubSubCategoryList(int categoryId, int subCategoryId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "CATEGORY_ID, " +
                         "SUB_CATEGORY_ID, " +
                         "SUB_SUB_CATEGORY_ID, " +
                         "SUB_SUB_CATEGORY_NAME " +
                         "FROM L_SUB_SUB_CATEGORY WHERE CATEGORY_ID = :CATEGORY_ID AND SUB_CATEGORY_ID = :SUB_CATEGORY_ID  order by SUB_SUB_CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql) { CommandType = CommandType.Text };
            objCommand.Parameters.Add("CATEGORY_ID", categoryId);
            objCommand.Parameters.Add("SUB_CATEGORY_ID", subCategoryId);

            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }

    }
}
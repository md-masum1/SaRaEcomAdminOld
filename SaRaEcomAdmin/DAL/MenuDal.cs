﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class MenuDal
    {
        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        public async Task<List<MenuMain>> GetMenueMain(string headOfficeId, string branchOfficeId)
        {
            List<MenuMain> menuMainList = new List<MenuMain>();

            var sql = "SELECT " +
                        "MENU_ID," +
                        "MENU_NAME," +
                        "MENU_URL," +
                        "MENU_ICON," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                        "FROM VEW_MENU_MAIN where HEAD_OFFICE_ID = '" + headOfficeId + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {

                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        MenuMain objMenuMain = new MenuMain
                        {
                            MenuId = Convert.ToInt32(objDataReader["MENU_ID"].ToString()),
                            MenuName = objDataReader["MENU_NAME"].ToString(),
                            MenuUrl = objDataReader["MENU_URL"].ToString(),
                            MenuIcon = objDataReader["MENU_ICON"].ToString()
                        };
                        menuMainList.Add(objMenuMain);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return menuMainList;
        }

        public async Task<List<MenuSub>> GetMenueSub(int menuId, string headOfficeId, string branchOfficeId)
        {
            List<MenuSub> menuSubList = new List<MenuSub>();

            var sql = "SELECT " +
                        "MENU_SUB_ID," +
                        "MENU_MAIN_ID," +
                        "MENU_SUB_NAME," +
                        "MENU_SUB_URL," +
                        "MENU_SUB_ICON," +
                        "MENU_ORDER," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                        "FROM VEW_MENU_SUB where MENU_MAIN_ID = "+ menuId +" AND HEAD_OFFICE_ID = '" + headOfficeId + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId + "' ORDER BY MENU_ORDER ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {

                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        MenuSub objMenuSub = new MenuSub
                        {
                            MenuId = Convert.ToInt32(objDataReader["MENU_SUB_ID"].ToString()),
                            MenuName = objDataReader["MENU_SUB_NAME"].ToString(),
                            MenuUrl = objDataReader["MENU_SUB_URL"].ToString(),
                            MenuIcon = objDataReader["MENU_SUB_ICON"].ToString()
                        };
                        menuSubList.Add(objMenuSub);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return menuSubList;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class DiscountAndOfferModel
    {
    }

    public class PrivilegeCardModel
    {
        public int CardId { get; set; }

        [Required]
        [DisplayName("Card Number")]
        public string CardNumber { get; set; }

        [Required]
        [DisplayName("Discount Percent")]
        public double Discount { get; set; }

        [Required]
        [DisplayName("Active Status")]
        public bool IsActive { get; set; }

        [Required]
        [DisplayName("Start Date")]
        public string StartDate { get; set; }

        [Required]
        [DisplayName("End Date")]
        public string EndDate { get; set; }

        public string UpdateBy { get; set; }

        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
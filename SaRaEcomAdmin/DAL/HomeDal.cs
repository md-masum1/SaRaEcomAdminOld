﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class HomeDal
    {
        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        public async Task<HomeModel> DashBoardTopReport()
        {
            const string sql = "SELECT " +
                               "DAILY_SALE," +
                               "MONTHLY_SALE," +
                               "YEARLY_SALE," +
                               "TOTAL_SALE," +
                               "TOTAL_ORDER," +
                               "TOTAL_DELIVERY," +
                               "TOTAL_PENDING," +
                               "TOTAL_RETURN," +
                               "TOTAL_SALE_MONTH," +
                               "TOTAL_REVENUE_MONTH," +
                               "TOTAL_ORDER_MONTH," +
                               "TOTAL_AVAIABLE_PRODUCT," +
                               "TOTAL_SALE_YEAR," +
                               "TOTAL_REVENUE_YEAR," +
                               "TOTAL_ORDER_YEAR," +
                               "TOTAL_SALE_ALL_TIME," +
                               "TOTAL_REVENUE_ALL_TIME," +
                               "TOTAL_ORDER_ALL_TIME " +
                               "FROM VEW_DASHBOARD_SUMMERY ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        HomeModel objHomeModel = new HomeModel();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objHomeModel.TotalSale = Convert.ToDouble(objDataReader["TOTAL_SALE"]);
                                objHomeModel.YearlySale = Convert.ToDouble(objDataReader["YEARLY_SALE"]);
                                objHomeModel.MonthlySale = Convert.ToDouble(objDataReader["MONTHLY_SALE"]);
                                objHomeModel.DailySale = Convert.ToDouble(objDataReader["DAILY_SALE"]);

                                objHomeModel.TotalOrder = Convert.ToInt32(objDataReader["TOTAL_ORDER"]);
                                objHomeModel.TotalDelivery = Convert.ToInt32(objDataReader["TOTAL_DELIVERY"]);
                                objHomeModel.TotalReturn = Convert.ToInt32(objDataReader["TOTAL_RETURN"]);
                                objHomeModel.DeliveryPending = Convert.ToInt32(objDataReader["TOTAL_PENDING"]);

                                objHomeModel.TotalAvailableProduct = Convert.ToInt32(objDataReader["TOTAL_AVAIABLE_PRODUCT"]);

                                objHomeModel.TotalSaleMonth = Convert.ToDouble(objDataReader["TOTAL_SALE_MONTH"]);
                                objHomeModel.TotalRevenueMonth = Convert.ToDouble(objDataReader["TOTAL_REVENUE_MONTH"]);
                                objHomeModel.TotalOrderMonth = Convert.ToInt32(objDataReader["TOTAL_ORDER_MONTH"]);

                                objHomeModel.TotalSaleYear = Convert.ToDouble(objDataReader["TOTAL_SALE_YEAR"]);
                                objHomeModel.TotalRevenueYear = Convert.ToDouble(objDataReader["TOTAL_REVENUE_YEAR"]);
                                objHomeModel.TotalOrderYear = Convert.ToInt32(objDataReader["TOTAL_ORDER_YEAR"]);

                                objHomeModel.AllTimeSale = Convert.ToDouble(objDataReader["TOTAL_SALE_ALL_TIME"]);
                                objHomeModel.AllTimeRevenue = Convert.ToDouble(objDataReader["TOTAL_REVENUE_ALL_TIME"]);
                                objHomeModel.AllTimeOrder = Convert.ToInt32(objDataReader["TOTAL_ORDER_ALL_TIME"]);
                            }
                            return objHomeModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }
    }
}
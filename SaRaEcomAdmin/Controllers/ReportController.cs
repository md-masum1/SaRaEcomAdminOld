﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class ReportController : Controller
    {
        private readonly ReportDocument _objReportDocument = new ReportDocument();
        private ExportFormatType _objExportFormatType = ExportFormatType.NoFormat;
        private readonly ReportDal _objReportDal = new ReportDal();


        public FileStreamResult ShowReport(string pReportType, string pFileDownloadName)
        {

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Clear();
            Response.Buffer = true;



            if (pReportType == "PDF")
            {
                _objExportFormatType = ExportFormatType.PortableDocFormat;

                Stream oStream = _objReportDocument.ExportToStream(_objExportFormatType);
                byte[] byteArray = new byte[oStream.Length];
                oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));

                Response.ContentType = "application/pdf";

                pFileDownloadName += ".pdf";

                Response.BinaryWrite(byteArray);
                Response.Flush();
                Response.Close();
                _objReportDocument.Close();
                _objReportDocument.Dispose();

                return File(oStream, Response.ContentType, pFileDownloadName);
            }
            else if (pReportType == "Excel")
            {
                _objExportFormatType = ExportFormatType.Excel;

                Stream oStream = _objReportDocument.ExportToStream(_objExportFormatType);
                byte[] byteArray = new byte[oStream.Length];
                oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));

                Response.ContentType = "application/vnd.ms-excel";

                pFileDownloadName += ".xls";

                Response.BinaryWrite(byteArray);
                Response.Flush();
                Response.Close();
                _objReportDocument.Close();
                _objReportDocument.Dispose();

                return File(oStream, Response.ContentType, pFileDownloadName);
            }
            else if (pReportType == "CSV")
            {
                _objExportFormatType = ExportFormatType.CharacterSeparatedValues;

                Stream oStream = _objReportDocument.ExportToStream(_objExportFormatType);
                byte[] byteArray = new byte[oStream.Length];
                oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));

                Response.ContentType = "text/csv";

                pFileDownloadName += ".csv";

                Response.BinaryWrite(byteArray);
                Response.Flush();
                Response.Close();
                _objReportDocument.Close();
                _objReportDocument.Dispose();

                return File(oStream, Response.ContentType, pFileDownloadName);
            }
            else if (pReportType == "TXT")
            {
                _objExportFormatType = ExportFormatType.RichText;

                Stream oStream = _objReportDocument.ExportToStream(_objExportFormatType);
                byte[] byteArray = new byte[oStream.Length];
                oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));

                Response.ContentType = "text/plain";

                pFileDownloadName += ".txt";

                Response.BinaryWrite(byteArray);
                Response.Flush();
                Response.Close();
                _objReportDocument.Close();
                _objReportDocument.Dispose();

                return File(oStream, Response.ContentType, pFileDownloadName);
            }

            return null;
        }

        private void GenerateSaleReport(ReportModel objSaleReport)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/rptSaleHistory.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (_objReportDal.SaleInformation(objSaleReport));


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            ShowReport(objSaleReport.ReportType, "Sale Report");
        }

        private void GenerateUploadReport(ReportModel objSaleReport)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/rptProductInfoByEmp.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (_objReportDal.ProductInfoByEmployee(objSaleReport));


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            ShowReport(objSaleReport.ReportType, "ProductUploadReport");
        }

        private void GenerateCustomerReport(ReportModel model)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/rptRegisteredCustomer.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (_objReportDal.RegisteredCustomer());


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            ShowReport(model.ReportType, "ProductUploadReport");
        }

        private void GenerateRejectedOrderReport(ReportModel objSaleReport)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/rptOrderRejectList.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (_objReportDal.RejectedOrderList(objSaleReport));


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            ShowReport(objSaleReport.ReportType, "RejectedOrderReport");
        }

        private void GenerateSuccessOrderRejectedReport(ReportModel objSaleReport)
        {
            string strPath = Path.Combine(Server.MapPath("~/Reports/rptRejectSuccessOrder.rpt"));
            _objReportDocument.Load(strPath);

            DataSet objDataSet = (_objReportDal.RejectedOrderSuccessPaymentList(objSaleReport));


            _objReportDocument.Load(strPath);
            _objReportDocument.SetDataSource(objDataSet);
            _objReportDocument.SetDatabaseLogon("saraecom", "saraecom");

            ShowReport(objSaleReport.ReportType, "RejectedOrderReport");
        }

        public async Task<ActionResult> ViewReport()
        {
            ReportModel model = new ReportModel();

            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
            ViewBag.MerchandiserList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetMerchandiser(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
            ViewBag.DesignerList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetDesigner(), "EMPLOYEE_ID", "EMPLOYEE_NAME");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ShowReport(ReportModel model)
        {
            if (model.ReportFor == "1")
            {
                GenerateSaleReport(model);
            }
            else if (model.ReportFor == "2")
            {
                GenerateUploadReport(model);
            }
            else if(model.ReportFor == "3")
            {
                GenerateCustomerReport(model);
            }
            else if (model.ReportFor == "4")
            {
                GenerateRejectedOrderReport(model);
            }
            else if (model.ReportFor == "5")
            {
                GenerateSuccessOrderRejectedReport(model);
            }

            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
            ViewBag.MerchandiserList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetMerchandiser(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
            ViewBag.DesignerList = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetDesigner(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
            return View("ViewReport", model);
        }

        public async Task<JsonResult> GetSubCategoryList(int categoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable( await _objReportDal.GetSubCategoryList(categoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetSubSubCategoryList(int categoryId, int subCategoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(await _objReportDal.GetSubSubCategoryList(categoryId, subCategoryId), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
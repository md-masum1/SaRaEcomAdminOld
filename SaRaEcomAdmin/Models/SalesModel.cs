﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class SalesModel
    {
        
    }

    public class Orders
    {
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string DeliveryDate { get; set; }
        public string ReturnDate { get; set; }
        public string RejectDate { get; set; }
        public string RejectBy { get; set; }
        public string RejectRemarks { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int ShippingCost { get; set; }

        public int ReturnRequestId { get; set; }
        public string ReturnReason { get; set; }
        public string ExchangeType { get; set; }
        public string Comment { get; set; }
        public bool ReturnStatus { get; set; }
        public bool RejectStatus { get; set; }

        public string PaymentType { get; set; }
        public string PaymentStatus { get; set; }
        public string Discount { get; set; }
        public string TotalAmount { get; set; }

        public IEnumerable<OrderedProduct> OrderedProducts { get; set; }
    }

    public class PreOrders
    {
        public string OrderNumber { get; set; }

        public string OrderDate { get; set; }

        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhone { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerAddress { get; set; }

        public string ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string ProductImage { get; set; }

        public string ProductPrice { get; set; }

        public bool Approve { get; set; }

        public string DeliveryDate { get; set; }

        public string ApproveRemarks { get; set; }

        public bool Reject { get; set; }

        public string RejectRemarks { get; set; }

    }

    public class OrderedProduct
    {
        public string OrderId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Sku { get; set; }
        public int ColorId { get; set; }
        public string ColorName { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }

    public class ReturnRequestModel
    {
        public int ReturnRequestId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string ReturnDate { get; set; }
        public string ReturnReason { get; set; }
        public string ExchangeType { get; set; }
        public string Comments { get; set; }
        public int ProductId { get; set; }
        public bool ReturnStatus { get; set; }
        public string ProductName { get; set; }
        public string Sku { get; set; }
        public string ProductImage { get; set; }
    }
}
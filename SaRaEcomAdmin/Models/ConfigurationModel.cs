﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class ConfigurationModel
    {
    }

    public class SliderModel
    {
        public int SliderId { get; set; }

        [DisplayName("Slider Title")]
        public string SliderTitle { get; set; }

        [DisplayName("Slider Description")]
        public string SliderDescription { get; set; }

        [Required]
        [DisplayName("Slider Url")]
        public string SliderUrl { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        [DisplayName("Slider Image")]
        public HttpPostedFileBase SliderImage { get; set; }
        public string SliderImageSiring { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class BannerModel
    {
        public int BannerId { get; set; }

        [Required]
        [DisplayName("Banner Title")]
        public string BannerTitle { get; set; }

        [DisplayName("Banner Description")]
        public string BannerDescription { get; set; }

        [Required]
        [DisplayName("Banner Url")]
        public string BannerUrl { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        [DisplayName("Banner Image")]
        public HttpPostedFileBase BannerImage { get; set; }
        public string BannerImageSiring { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class OfferModel
    {
        public int OfferId { get; set; }

        [Required]
        [DisplayName("Offer Url")]
        public string OfferUrl { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        [DisplayName("Offer Image")]
        public HttpPostedFileBase OfferImage { get; set; }
        public string OfferImageSiring { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class TrendingProductModel
    {
        [Required]
        [DisplayName("Product")]
        public int? ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        public string ProductImage { get; set; }

        public string Sku { get; set; }

        public string ProductCode { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class PromotionalProductModel
    {

        [Required]
        [DisplayName("Product")]
        public int? ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        public string ProductImage { get; set; }

        public string Sku { get; set; }

        public double PreviousPrice { get; set; }
        public double NewPrice { get; set; }
        public string Status { get; set; }

        [Required]
        [DisplayName("Start Date")]
        public string StartDate { get; set; }
        [Required]
        [DisplayName("End Date")]
        public string EndDate { get; set; }
        [Required]
        [DisplayName("Percentage")]
        public double? PromotionPercentage { get; set; }
        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class OccasionalProduct
    {
        [Required]
        [DisplayName("Product")]
        public int? ProductId { get; set; }

        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        public string ProductImage { get; set; }

        public string Sku { get; set; }

        public string ProductCode { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class OccasionalSliderModel
    {
        public int SliderId { get; set; }

        [DisplayName("Slider Title")]
        public string SliderTitle { get; set; }

        [Required]
        [DisplayName("Slider Url")]
        public string SliderUrl { get; set; }

        [Required]
        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        [DisplayName("Slider Image")]
        public HttpPostedFileBase SliderImage { get; set; }
        public string SliderImageSiring { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class PopupModel
    {
        public int PopupId { get; set; }

        [Required]
        [DisplayName("Popup Name")]
        public string PopupName { get; set; }

        [DisplayName("Active Status")]
        public bool IsActive { get; set; }

        [DisplayName("Popup Image")]
        public HttpPostedFileBase PopupImage { get; set; }
        public string PopupImageSiring { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
﻿using System.Threading.Tasks;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.Controllers
{
    public class MenueController : Controller
    {
        readonly MenuDal _menuDal = new MenuDal();

        public async Task<ActionResult> Index()
        {
            var employee = Session["authentication"] as AuthModel;
            if (employee != null)
            {
                MenuModel menuModel = new MenuModel
                {
                    MenuMains = await _menuDal.GetMenueMain(employee.HeadOfficeId, employee.BranchOfficeId)
                };

                foreach (var menuMain in menuModel.MenuMains)
                {
                    menuMain.MenuSubs =
                        await _menuDal.GetMenueSub(menuMain.MenuId, employee.HeadOfficeId, employee.BranchOfficeId);
                }

                return PartialView("_slidbarPartial", menuModel);
            }
            return null;
        }
    }
}
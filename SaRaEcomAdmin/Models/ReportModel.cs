﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class ReportModel
    {
        [DisplayName("From Date")]
        public string FormDate { get; set; }

        [DisplayName("To Date")]
        public string ToDate { get; set; }

        [DisplayName("Category")]
        public string CategoryId { get; set; }

        [DisplayName("Sub Category")]
        public string SubCategoryId { get; set; }

        [DisplayName("Sub Sub Category")]
        public string SubSubCategoryId { get; set; }

        [DisplayName("Designer")]
        public string DesignerName { get; set; }

        [DisplayName("Merchandiser")]
        public string MerchandiserName { get; set; }

        public string ReportFor { get; set; }

        public string ReportType { get; set; }
    }

    public class SaleReport
    {
        [Required]
        [DisplayName("From Date")]
        public string FormDate { get; set; }

        [Required]
        [DisplayName("To Date")]
        public string ToDate { get; set; }

        [DisplayName("Category")]
        public string CategoryId { get; set; }

        [DisplayName("Sub Category")]
        public string SubCategoryId { get; set; }

        [DisplayName("Sub Sub Category")]
        public string SubSubCategoryId { get; set; }

        [DisplayName("Designer")]
        public string DesignerName { get; set; }

        [DisplayName("Merchandiser")]
        public string MerchandiserName { get; set; }

        public string ReportFor { get; set; }

        public string ReportType { get; set; }
    }
}
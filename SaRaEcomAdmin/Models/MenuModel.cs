﻿using System.Collections.Generic;

namespace SaRaEcomAdmin.Models
{
    public class MenuModel
    {
        public List<MenuMain> MenuMains { get; set; }
    }

    public class MenuMain
    {
        public int MenuId { get; set; }

        public string MenuName { get; set; }

        public string MenuUrl { get; set; }

        public string MenuIcon { get; set; }

        public List<MenuSub> MenuSubs { get; set; }
    }

    public class MenuSub
    {
        public int MenuId { get; set; }

        public string MenuName { get; set; }

        public string MenuUrl { get; set; }

        public string MenuIcon { get; set; }
    }
}
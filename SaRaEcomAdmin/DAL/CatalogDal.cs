﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class CatalogDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Product Review Section  

        public async Task<IEnumerable<ProductReviewModel>> GetApprovedReviews()
        {
            const string sql = "SELECT " +
                                "PRODUCT_REVIEW_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "CUSTOMER_NAME," +
                                "REVIEW_DATE," +
                                "APPROVE_YN," +
                                "APPROVED_BY," +
                                "APPROVED_DATE," +
                                "APPROVE_YN," +
                                "REJECT_YN " +
                                "FROM VEW_PRODUCT_REVIEW WHERE APPROVE_YN = 'Y' AND REJECT_YN = 'N' ORDER BY PRODUCT_REVIEW_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductReviewModel> objProductReview = new List<ProductReviewModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductReviewModel model = new ProductReviewModel();

                                model.ProductReviewId = Convert.ToInt32(objDataReader["PRODUCT_REVIEW_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.ProductReviewDate = objDataReader["REVIEW_DATE"].ToString();
                                model.ApprovedBy = objDataReader["APPROVED_BY"].ToString();
                                model.ApprovedDate = objDataReader["APPROVED_DATE"].ToString();
                                model.Approval = objDataReader["APPROVE_YN"].ToString() == "Y";

                                objProductReview.Add(model);
                            }
                            return objProductReview;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductReviewModel>> GetPendingReviews()
        {
            const string sql = "SELECT " +
                                "PRODUCT_REVIEW_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "CUSTOMER_NAME," +
                                "REVIEW_DATE," +
                                "APPROVE_YN," +
                                "REJECT_YN " +
                                "FROM VEW_PRODUCT_REVIEW WHERE APPROVE_YN = 'N' AND REJECT_YN = 'N' ORDER BY PRODUCT_REVIEW_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductReviewModel> objProductReview = new List<ProductReviewModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductReviewModel model = new ProductReviewModel();

                                model.ProductReviewId = Convert.ToInt32(objDataReader["PRODUCT_REVIEW_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.ProductReviewDate = objDataReader["REVIEW_DATE"].ToString();
                                model.Approval = objDataReader["APPROVE_YN"].ToString() == "Y";

                                objProductReview.Add(model);
                            }
                            return objProductReview;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<ProductReviewModel>> GetRejectedReviews()
        {
            const string sql = "SELECT " +
                                "PRODUCT_REVIEW_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "CUSTOMER_NAME," +
                                "REVIEW_DATE," +
                                "APPROVE_YN," +
                                "REJECT_YN," +
                                "REJECT_BY," +
                                "REJECT_DATE " +
                                "FROM VEW_PRODUCT_REVIEW WHERE REJECT_YN = 'Y' ORDER BY PRODUCT_REVIEW_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<ProductReviewModel> objProductReview = new List<ProductReviewModel>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                ProductReviewModel model = new ProductReviewModel();

                                model.ProductReviewId = Convert.ToInt32(objDataReader["PRODUCT_REVIEW_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.ProductReviewDate = objDataReader["REVIEW_DATE"].ToString();
                                model.RejectBy = objDataReader["REJECT_BY"].ToString();
                                model.RejectDate = objDataReader["REJECT_DATE"].ToString();
                                model.Approval = objDataReader["APPROVE_YN"].ToString() == "Y";

                                objProductReview.Add(model);
                            }
                            return objProductReview;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<ProductReviewModel> GetProductReview(int productReviewId)
        {
            string sql = "SELECT " +
                         "PRODUCT_REVIEW_ID," +
                         "PRODUCT_ID," +
                         "PRODUCT_NAME," +
                         "SKU," +
                         "CUSTOMER_ID," +
                         "CUSTOMER_NAME," +
                         "PHONE_NUMBER," +
                         "CUSTOMER_EMAIL," +
                         "MESSAGE," +
                         "REVIEW_SCORE," +
                         "REVIEW_DATE," +
                         "APPROVE_YN," +
                         "APPROVED_BY," +
                         "APPROVED_DATE," +
                         "REJECT_YN," +
                         "REJECT_BY," +
                         "REJECT_DATE " +
                         "FROM VEW_PRODUCT_REVIEW WHERE PRODUCT_REVIEW_ID = :PRODUCT_REVIEW_ID ORDER BY PRODUCT_REVIEW_ID DESC ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRODUCT_REVIEW_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productReviewId;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        ProductReviewModel model = new ProductReviewModel();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                model.ProductReviewId = Convert.ToInt32(objDataReader["PRODUCT_REVIEW_ID"].ToString());
                                model.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.CustomerPhone = objDataReader["PHONE_NUMBER"].ToString();
                                model.Message = objDataReader["MESSAGE"].ToString();
                                model.ProductRating = Convert.ToDouble(objDataReader["REVIEW_SCORE"].ToString());
                                model.ProductReviewDate = objDataReader["REVIEW_DATE"].ToString();
                                model.Approval = objDataReader["APPROVE_YN"].ToString() == "Y";
                                model.ApprovedBy = objDataReader["APPROVED_BY"].ToString();
                                model.ApprovedDate = objDataReader["APPROVED_DATE"].ToString();
                                model.Reject = objDataReader["REJECT_YN"].ToString() == "Y";
                                model.RejectBy = objDataReader["REJECT_BY"].ToString();
                                model.RejectDate = objDataReader["REJECT_DATE"].ToString();
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> ApproveReview(int productReviewId, string approvedBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_APPROVE_PRODUCT_REVIEW")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_PRODUCT_REVIEW_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productReviewId != 0 ? (object) productReviewId : null;
            objOracleCommand.Parameters.Add("P_APPROVED_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(approvedBy) ? approvedBy : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> RejectReview(int productReviewId, string rejectBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_REJECT_PRODUCT_REVIEW")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_PRODUCT_REVIEW_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productReviewId != 0 ? (object)productReviewId : null;
            objOracleCommand.Parameters.Add("P_REJECT_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(rejectBy) ? rejectBy : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion

        #region Upcoming Product

        public async Task<IEnumerable<UpcomingProduct>> GetUpcomingProducts(string headOfficeId, string branchOfficeId)
        {
            const string sql = "SELECT " +
                               "UPCOMING_PRODUCT_ID," +
                               "PRODUCT_NAME," +
                               "DISPLAY_ORDER," +
                               "SKU," +
                               "PRICE," +
                               "IMAGE," +
                               "UPDATE_BY," +
                               "HEAD_OFFICE_ID," +
                               "BRANCH_OFFICE_ID " +
                               "FROM VEW_UPCOMING_PRODUCT WHERE HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ORDER BY DISPLAY_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = headOfficeId;
                    objCommand.Parameters.Add(":BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = branchOfficeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<UpcomingProduct> objUpcomingProduct = new List<UpcomingProduct>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                UpcomingProduct model = new UpcomingProduct();

                                model.ProductId = Convert.ToInt32(objDataReader["UPCOMING_PRODUCT_ID"].ToString());
                                model.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                model.Sku = objDataReader["SKU"].ToString();
                                model.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                                model.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());
                                model.ImageString = objDataReader["IMAGE"].ToString();
                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                model.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                model.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();

                                objUpcomingProduct.Add(model);
                            }
                            return objUpcomingProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<UpcomingProduct> GetUpcomingProduct(int productId, string headOfficeId, string branchOfficeId)
        {
            const string sql = "SELECT " +
                               "UPCOMING_PRODUCT_ID," +
                               "PRODUCT_NAME," +
                               "DISPLAY_ORDER," +
                               "SKU," +
                               "PRICE," +
                               "IMAGE," +
                               "UPDATE_BY," +
                               "HEAD_OFFICE_ID," +
                               "BRANCH_OFFICE_ID " +
                               "FROM VEW_UPCOMING_PRODUCT WHERE UPCOMING_PRODUCT_ID = :UPCOMING_PRODUCT_ID AND HEAD_OFFICE_ID = :HEAD_OFFICE_ID AND BRANCH_OFFICE_ID = :BRANCH_OFFICE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":UPCOMING_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = productId;
                    objCommand.Parameters.Add(":HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = headOfficeId;
                    objCommand.Parameters.Add(":BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = branchOfficeId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        UpcomingProduct objUpcomingProduct = new UpcomingProduct();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objUpcomingProduct.ProductId = Convert.ToInt32(objDataReader["UPCOMING_PRODUCT_ID"].ToString());
                                objUpcomingProduct.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                objUpcomingProduct.Sku = objDataReader["SKU"].ToString();
                                objUpcomingProduct.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                                objUpcomingProduct.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());
                                objUpcomingProduct.ImageString = objDataReader["IMAGE"].ToString();
                                objUpcomingProduct.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                                objUpcomingProduct.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                                objUpcomingProduct.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                            }
                            return objUpcomingProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> SaveUpcomingProduct(UpcomingProduct upcomingProduct)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_UPCOMING_PRODUCT_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = upcomingProduct.ProductId;
            objOracleCommand.Parameters.Add("P_PRODUCT_NAME", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.ProductName) ? upcomingProduct.ProductName : null;
            objOracleCommand.Parameters.Add("P_DISPLAY_ORDER", OracleDbType.Varchar2, ParameterDirection.Input).Value = upcomingProduct.DisplayOrder;
            objOracleCommand.Parameters.Add("P_SKU", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.Sku) ? upcomingProduct.Sku : null;
            objOracleCommand.Parameters.Add("P_PRODUCT_IMAGE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.ImageString) ? upcomingProduct.ImageString : null;
            objOracleCommand.Parameters.Add("P_PRODUCT_PRICE", OracleDbType.Varchar2, ParameterDirection.Input).Value =  upcomingProduct.Price;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.UpdateBy) ? upcomingProduct.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.HeadOfficeId) ? upcomingProduct.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(upcomingProduct.BranchOfficeId) ? upcomingProduct.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> DeleteUpcomingProduct(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_UPCOMING_PRODUCT_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_PRODUCT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        #endregion
    }
}
﻿using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SaRaEcomAdmin.Controllers
{
    public class ProductInventoryController : Controller
    {
        readonly ProductInventoryDal _productInventoryDal = new ProductInventoryDal();
        readonly ProductInventoryModel _productInventoryModel = new ProductInventoryModel();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion


        public ActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"] as string;
            }

            return View();
        }

        public ActionResult GetProductList(DataTableAjaxPostModel model)
        {
            LoadSession();

            ProductGrid objProductGrid = new ProductGrid
            {
                SearchBy = model.search?.value,
                UpdateBy = _strEmployeeId,
                HeadOfficeId = _strHeadOfficeId,
                BranchOfficeId = _strBranchOfficeId
            };

            if (model.order != null)
            {
                objProductGrid.OrderByName = model.columns[model.order[0].column].data;
                objProductGrid.OrderByDirection = model.order[0].dir.ToUpper();
            }

            List<ProductGrid> data = _productInventoryDal.GetProductGrids(objProductGrid).ToList();
            foreach (var grid in data)
            {
                grid.ProductImageUrl = Url.Content(grid.ProductImageUrl);
            }

            int recordsFiltered = data.Count;
            int recordsTotal = data.Count;

            if (recordsTotal < model.length)
            {
                recordsTotal = model.length;
            }

            if (model.length == -1)
            {
                data = data.ToList();
            }
            else
            {
                data = data.Skip(model.start).Take(model.length).ToList();
            }

            return Json(new { model.draw, recordsTotal, recordsFiltered, data }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Create()
        {
            LoadSession();

            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
            ViewBag.FabricList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetFabric(), "FABRIC_ID", "FABRIC_NAME");
            ViewBag.MeasurementUnitList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMeasurementUnit(), "UNIT_ID", "UNIT_NAME");

            ViewBag.MerchandiserList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMerchandiser(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
            ViewBag.DesignerList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetDesigner(), "EMPLOYEE_ID", "EMPLOYEE_NAME");

            return View(_productInventoryModel);
        }

        public async Task<ActionResult> Edit(int? id)
        {
            LoadSession();

            if (id != null && id != 0)
            {
                //load DropDown List
                ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
                ViewBag.FabricList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetFabric(), "FABRIC_ID", "FABRIC_NAME");
                ViewBag.MeasurementUnitList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMeasurementUnit(), "UNIT_ID", "UNIT_NAME");

                ViewBag.MerchandiserList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMerchandiser(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
                ViewBag.DesignerList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetDesigner(), "EMPLOYEE_ID", "EMPLOYEE_NAME");

                var objProductInventoryModel = await _productInventoryDal.GetProductInventory((int)id, _strHeadOfficeId, _strBranchOfficeId);
                objProductInventoryModel.MapStringList = await _productInventoryDal.GetProductMap(objProductInventoryModel.ProductId);

                ViewBag.SubCategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubCategoryList(Convert.ToInt32(objProductInventoryModel.CategoryId)), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
                ViewBag.SubSubCategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubSubCategoryList(Convert.ToInt32(objProductInventoryModel.CategoryId), Convert.ToInt32(objProductInventoryModel.SubCategoryId)), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");

                return View("Create", objProductInventoryModel);

            }
            return View("Index");
        }

        private async Task SaveMappingForProduct(string mapString, int productId)
        {
            LoadSession();
            await _productInventoryDal.DeleteProductMapping(productId, _strHeadOfficeId, _strBranchOfficeId);
            string[] singleMap = mapString.Split('|');
            foreach (var data in singleMap)
            {
                string[] attr = data.Split(',');
                MapList model = new MapList();
                if (attr.Length == 3)
                {
                    model.CategoryId = Convert.ToInt32(attr[0]);
                    model.SubCategoryId = Convert.ToInt32(attr[1]);
                    model.SubSubCategoryId = Convert.ToInt32(attr[2]);
                    model.ProductId = productId;
                    model.UpdateBy = _strEmployeeId;
                    model.HeadOfficeId = _strHeadOfficeId;
                    model.BranchOfficeId = _strBranchOfficeId;

                    if (productId > 0)
                    {
                        
                        await _productInventoryDal.SaveProductMapping(model);
                    }
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveOrUpdate(HttpPostedFileBase primaryImage, ProductInventoryModel productInventoryModel)
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth == null)
            {
                return RedirectToAction("Index", "Auth");
            }

            if (productInventoryModel.ProductId != 0)
            {
                ModelState.Remove("PrimaryImage");
            }
            if (!ModelState.IsValid)
            {
                if (productInventoryModel.ProductId != 0)
                    return RedirectToAction("Edit", new { id = productInventoryModel.ProductId });
                return RedirectToAction("Create");
            }
            ModelState.Clear();
            LoadSession();
            productInventoryModel.UpdateBy = _strEmployeeId;
            productInventoryModel.HeadOfficeId = _strHeadOfficeId;
            productInventoryModel.BranchOfficeId = _strBranchOfficeId;

            string filePath = Server.MapPath("~/Files/ImageProductMain/");
            string vFileName = Guid.NewGuid().ToString();

            if (primaryImage != null)
            {
                var imageExtension = Path.GetExtension(primaryImage.FileName);
                if (imageExtension != null)
                {
                    imageExtension = imageExtension.ToUpper();

                    if (imageExtension == ".JPG" || imageExtension == ".JPEG" || imageExtension == ".PNG")
                    {
                        if (productInventoryModel.ProductId != 0)
                        {
                            string imagePath = filePath + productInventoryModel.PrimaryImageString;
                            if (System.IO.File.Exists(imagePath))
                            {
                                System.IO.File.Delete(imagePath);
                            }
                        }
                        filePath += vFileName;
                        filePath += imageExtension.ToLower();

                        productInventoryModel.PrimaryImage.SaveAs(filePath);

                        productInventoryModel.PrimaryImageUrl = filePath;
                        productInventoryModel.PrimaryImageString = vFileName + imageExtension.ToLower();
                    }
                }

                if (productInventoryModel.PrimaryImageString != null)
                {
                    if (!string.IsNullOrWhiteSpace(productInventoryModel.MapString))
                    {
                        var strMessage = await _productInventoryDal.SaveProductInfo(productInventoryModel);
                        await SaveMappingForProduct(productInventoryModel.MapString, strMessage.Item2);
                        TempData["message"] = strMessage.Item1;
                    }
                    else
                    {
                        TempData["message"] = "Map not found";
                    }
                    
                }
                else
                {
                    LoadSession();

                    //load DropDown List
                    ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");
                    ViewBag.FabricList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetFabric(), "FABRIC_ID", "FABRIC_NAME");
                    ViewBag.MeasurementUnitList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMeasurementUnit(), "UNIT_ID", "UNIT_NAME");

                    ViewBag.MerchandiserList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetMerchandiser(), "EMPLOYEE_ID", "EMPLOYEE_NAME");
                    ViewBag.DesignerList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetDesigner(), "EMPLOYEE_ID", "EMPLOYEE_ID");

                    //var objProductInventoryModel = _productInventoryDal.GetProductInventory((int)id, _strHeadOfficeId, _strBranchOfficeId);

                    ViewBag.SubCategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubCategoryList(Convert.ToInt32(productInventoryModel.CategoryId)), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
                    ViewBag.SubSubCategoryList = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubSubCategoryList(Convert.ToInt32(productInventoryModel.CategoryId), Convert.ToInt32(productInventoryModel.SubCategoryId)), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");

                    TempData["message"] = "Image must be jpg or png";

                    return View("Create", productInventoryModel);
                }

            }
            else if (productInventoryModel.PrimaryImageString != null)
            {
                productInventoryModel.PrimaryImageUrl = filePath + productInventoryModel.PrimaryImageString;
                if (!string.IsNullOrWhiteSpace(productInventoryModel.MapString))
                {
                    var strMessage = await _productInventoryDal.SaveProductInfo(productInventoryModel);
                    await SaveMappingForProduct(productInventoryModel.MapString, strMessage.Item2);
                    TempData["message"] = strMessage.Item1;
                }
                else
                {
                    TempData["message"] = "Map not found";
                }
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<JsonResult> SaveImageAndColor(ProductInventoryModel productInventoryModel)
        {
            if (productInventoryModel.ColorId != 0 && productInventoryModel.ColorId != null)
            {
                string strMessageImage = "";

                LoadSession();

                productInventoryModel.UpdateBy = _strEmployeeId;
                productInventoryModel.HeadOfficeId = _strHeadOfficeId;
                productInventoryModel.BranchOfficeId = _strBranchOfficeId;

                string filePath = Server.MapPath("~/Files/ImageProductColor/");

                var image1Extension = Path.GetExtension(productInventoryModel.Image1Base?.FileName);
                if (image1Extension != null)
                {
                    image1Extension = image1Extension.ToUpper();
                    if (image1Extension == ".JPG" || image1Extension == ".JPEG" || image1Extension == ".PNG")
                    {
                        productInventoryModel.ImageString1 = UtilityClass.SaveAnImage(productInventoryModel.Image1Base, filePath);
                    }
                }

                var image2Extension = Path.GetExtension(productInventoryModel.Image2Base?.FileName);
                if(image2Extension != null)
                {
                    image2Extension = image2Extension.ToUpper();
                    if (image2Extension == ".JPG" || image2Extension == ".JPEG" || image2Extension == ".PNG")
                    {
                        productInventoryModel.ImageString2 = UtilityClass.SaveAnImage(productInventoryModel.Image2Base, filePath);
                    }
                }

                var image3Extension = Path.GetExtension(productInventoryModel.Image3Base?.FileName);
                if (image3Extension != null)
                {
                    image3Extension = image3Extension.ToUpper();
                    if (image3Extension == ".JPG" || image3Extension == ".JPEG" || image3Extension == ".PNG")
                    {
                        productInventoryModel.ImageString3 = UtilityClass.SaveAnImage(productInventoryModel.Image3Base, filePath);
                    }
                }

                var image4Extension = Path.GetExtension(productInventoryModel.Image4Base?.FileName);
                if (image4Extension != null)
                {
                    image4Extension = image4Extension.ToUpper();
                    if (image4Extension == ".JPG" || image4Extension == ".JPEG" || image4Extension == ".PNG")
                    {
                        productInventoryModel.ImageString4 = UtilityClass.SaveAnImage(productInventoryModel.Image4Base, filePath);
                    }
                }

                if (!string.IsNullOrWhiteSpace(productInventoryModel.ImageString1) &&
                    !string.IsNullOrWhiteSpace(productInventoryModel.ImageString2) &&
                    !string.IsNullOrWhiteSpace(productInventoryModel.ImageString3) &&
                    !string.IsNullOrWhiteSpace(productInventoryModel.ImageString4))
                {
                    if (productInventoryModel.ImageId != 0)
                    {
                        var getImageName = await _productInventoryDal.GetImageName(productInventoryModel.ProductId, (int)productInventoryModel.ColorId, _strHeadOfficeId, _strBranchOfficeId);

                        foreach (var data in getImageName)
                        {
                            string path = Server.MapPath("~/Files/ImageProductColor/");
                            UtilityClass.DeleteAnImage(data, path);
                        }
                    }
                    strMessageImage = await _productInventoryDal.SaveProductImageAndColor(productInventoryModel);
                }
                return Json(new { success = true, strMessageImage }, JsonRequestBehavior.AllowGet);

            }

            return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> SaveSizeAndQuantity(SizeAndQuantitySave sizeAndQuantitySave)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, issue = sizeAndQuantitySave, errors = ModelState.Values.Where(i => i.Errors.Count > 0) });
            }

            string strMessageInventory = "";

            LoadSession();

            sizeAndQuantitySave.UpdateBy = _strEmployeeId;
            sizeAndQuantitySave.HeadOfficeId = _strHeadOfficeId;
            sizeAndQuantitySave.BranchOfficeId = _strBranchOfficeId;

            sizeAndQuantitySave.EntryDate = sizeAndQuantitySave.ProductEntryDate;

            for (int i = 0; i < sizeAndQuantitySave.SizeIdList.Count(); i++)
            {
                sizeAndQuantitySave.SizeId = sizeAndQuantitySave.SizeIdList[i];
                await _productInventoryDal.SizeSave(sizeAndQuantitySave);

                sizeAndQuantitySave.Quantity = sizeAndQuantitySave.QuantityList[i];
                sizeAndQuantitySave.Price = sizeAndQuantitySave.PriceList[i];
                strMessageInventory = await _productInventoryDal.SaveProductInventory(sizeAndQuantitySave);
            }

            return Json(strMessageInventory, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> SaveSizeAndRatio(SizeAndRatioSave sizeAndRatioSave)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, responseText = "Validation Error" }, JsonRequestBehavior.AllowGet);
            }

            string strMessageRatio = "";

            LoadSession();

            sizeAndRatioSave.UpdateBy = _strEmployeeId;
            sizeAndRatioSave.HeadOfficeId = _strHeadOfficeId;
            sizeAndRatioSave.BranchOfficeId = _strBranchOfficeId;

            for (int i = 0; i < sizeAndRatioSave.SizeIdList.Count(); i++)
            {
                sizeAndRatioSave.SizeId = sizeAndRatioSave.SizeIdList[i];
                sizeAndRatioSave.SizeValue = sizeAndRatioSave.SizeValueList[i];

                strMessageRatio = await _productInventoryDal.SaveProductSizeRatio(sizeAndRatioSave);
            }

            return Json(new { success = true, strMessageRatio }, JsonRequestBehavior.AllowGet);
        }



        public async Task<JsonResult> LoadImageGrid(int id)
        {
            List<ProductImageGrid> grid = new List<ProductImageGrid>();

            if (id != 0)
            {
                grid = await _productInventoryDal.GetImageGrids(id);
            }
            return new JsonResult{ Data = new { data = grid }, MaxJsonLength = int.MaxValue};
            /*Json(new { data = grid }, JsonRequestBehavior.AllowGet);*/
        }

        public async Task<JsonResult> DeleteProductImage(int productId, int colorId)
        {
            string message = "";
            if (productId != 0 && colorId != 0)
            {
                LoadSession();

                var getImageName = await _productInventoryDal.GetImageName(productId, colorId, _strHeadOfficeId, _strBranchOfficeId);

                message = await _productInventoryDal.DeleteProductImage(productId, colorId, _strHeadOfficeId, _strBranchOfficeId);

                if (message == "DELETED SUCCESSFULLY")
                {
                    foreach (var data in getImageName)
                    {
                        string path = Server.MapPath("~/Files/ImageProductColor/");
                        string desPath = Server.MapPath("~/Files/DeletedFiles/");

                        UtilityClass.MoveAnImage(data, path, desPath);
                        UtilityClass.DeleteAnImage(data, path);
                    }
                }

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteProductSize(int productId, int colorId, int sizeId)
        {
            string message = "";
            if (productId != 0 && colorId != 0)
            {
                LoadSession();

                message = await _productInventoryDal.DeleteProductSize(productId, colorId, sizeId, _strHeadOfficeId, _strBranchOfficeId);

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ColorCount(int id)
        {
            int count = await _productInventoryDal.ColorCount(id);

            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SizeCount(int id)
        {
            int count = await _productInventoryDal.SizeCount(id);

            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> InventoryDetails(int id)
        {
            PeoductInventoryDisplay proDisplay = new PeoductInventoryDisplay();
            if (id != 0)
            {
                LoadSession();

                proDisplay = await _productInventoryDal.GetProductInventorySummary(id, _strHeadOfficeId, _strBranchOfficeId);

                if (proDisplay != null)
                {
                    proDisplay.ColorLists = await _productInventoryDal.GetProductColor(id, _strHeadOfficeId,
                        _strBranchOfficeId);

                    if (proDisplay.ColorLists != null)
                    {
                        foreach (var color in proDisplay.ColorLists)
                        {
                            color.SizeLists = await _productInventoryDal.GetProductSizeAndQuantity(id, color.ColorId,
                                _strHeadOfficeId, _strBranchOfficeId);
                        }
                    }
                }
            }
            return PartialView("ProductInventory/_ProductInventoryPartial", proDisplay);
        }

        public async Task<ActionResult> RatioChartGrid(int id)
        {
            RatioChartDisplay ratioChart = new RatioChartDisplay();
            if (id != 0)
            {
                LoadSession();

                ratioChart = _productInventoryDal.GetProductForRatioChart(id, _strHeadOfficeId, _strBranchOfficeId);

                if (ratioChart != null)
                {
                    ratioChart.RatioLists = await _productInventoryDal.GetRatioLists(id, _strHeadOfficeId, _strBranchOfficeId);

                    if (ratioChart.RatioLists != null)
                    {
                        foreach (var sizeList in ratioChart.RatioLists)
                        {
                            sizeList.RatioSizeLists = await _productInventoryDal.GetRatioSizeLists(id, sizeList.RatioId,
                                _strHeadOfficeId, _strBranchOfficeId);
                        }
                    }
                }
            }
            return PartialView("ProductInventory/_RatioChartPartial", ratioChart);
        }

        public async Task<JsonResult> InventorySummary(int id)
        {
            PeoductInventoryDisplay proDisplay = new PeoductInventoryDisplay();
            if (id != 0)
            {
                LoadSession();

                proDisplay = await _productInventoryDal.GetProductInventorySummary(id, _strHeadOfficeId, _strBranchOfficeId);
            }

            //return new JsonResult { Data = new { data = proDisplay }, MaxJsonLength = int.MaxValue };
            return new JsonResult { Data = proDisplay, MaxJsonLength = int.MaxValue };
            //return Json(proDisplay, JsonRequestBehavior.AllowGet);
        }


        //Load DropDown Data
        public async Task<JsonResult> GetColor()
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetColor(), "COLOR_ID", "COLOR_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetProductColor(int id)
        {
            if (id != 0)
            {
                LoadSession();

                var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetProductColorList(id, _strHeadOfficeId, _strBranchOfficeId), "COLOR_ID", "COLOR_NAME");
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return Json("Error", JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetSize()
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSize(), "SIZE_ID", "SIZE_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetProductSize(int id)
        {
            if (id != 0)
            {
                LoadSession();

                var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetProductSizeList(id, _strHeadOfficeId, _strBranchOfficeId), "SIZE_ID", "SIZE_NAME");
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            return Json("Error", JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetRatio()
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetRatioList(), "RATIO_ID", "RATIO_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetSubCategory(int categoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubCategoryList(categoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetSubSubCategory(int categoryId, int subCategoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(await _productInventoryDal.GetSubSubCategoryList(categoryId, subCategoryId), "SUB_SUB_CATEGORY_ID", "SUB_SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
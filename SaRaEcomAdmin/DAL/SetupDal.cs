﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class SetupDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region "Category Section"
        public async Task<List<CategoryModel>> GetCategoryList(string headOfficeId, string branchOfficeId)
        {
            List<CategoryModel> objCategoryModels = new List<CategoryModel>();

            var sql = "SELECT " +
                         "CATEGORY_ID," +
                         "CATEGORY_NAME," +
                         "DISPLAY_ORDER," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM L_CATEGORY where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY DISPLAY_ORDER ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        CategoryModel model = new CategoryModel
                        {
                            CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString()),
                            CategoryName = objDataReader["CATEGORY_NAME"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objCategoryModels.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objCategoryModels;
        }
        public async Task<CategoryModel> GetCategory(int categoryId, string headOfficeId, string branchOfficeId)
        {
            CategoryModel objCategoryModels = new CategoryModel();

            var sql = "SELECT " +
                         "CATEGORY_ID," +
                         "CATEGORY_NAME," +
                         "DISPLAY_ORDER," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM L_CATEGORY where CATEGORY_ID = '"+ categoryId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objCategoryModels.CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                        objCategoryModels.CategoryName = objDataReader["CATEGORY_NAME"].ToString();
                        objCategoryModels.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objCategoryModels.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objCategoryModels.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objCategoryModels.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objCategoryModels;
        }
        public async Task<string> SaveCategory(CategoryModel objCategoryModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_category_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCategoryModel.CategoryId;
            objOracleCommand.Parameters.Add("p_category_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCategoryModel.CategoryName) ? objCategoryModel.CategoryName : null;
            objOracleCommand.Parameters.Add("P_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = objCategoryModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCategoryModel.UpdateBy) ? objCategoryModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCategoryModel.HeadOfficeId) ? objCategoryModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCategoryModel.BranchOfficeId) ? objCategoryModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteCategory(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_category_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region "Sub Category Section"
        public async Task<List<SubCategoryModel>> GetSubCategoryList(string headOfficeId, string branchOfficeId)
        {
            List<SubCategoryModel> objSubCategory = new List<SubCategoryModel>();

            var sql = "SELECT " +
                         "SUB_CATEGORY_ID," +
                         "SUB_CATEGORY_NAME," +
                         "CATEGORY_ID," +
                         "CATEGORY_NAME," +
                         "DISPLAY_ORDER," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_SUB_CATEGORY where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY CATEGORY_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        SubCategoryModel model = new SubCategoryModel
                        {
                            CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString()),
                            SubCategoryId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString()),
                            SubCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString(),
                            CategoryName = objDataReader["CATEGORY_NAME"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSubCategory.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSubCategory;
        }
        public async Task<SubCategoryModel> GetSubCategory(int categoryId, int subCategoryId, string headOfficeId, string branchOfficeId)
        {
            SubCategoryModel objSubCategoryModel = new SubCategoryModel();

            var sql = "SELECT " +
                        "SUB_CATEGORY_ID," +
                        "SUB_CATEGORY_NAME," +
                        "CATEGORY_ID," +
                        "CATEGORY_NAME," +
                        "DISPLAY_ORDER," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                        "FROM VEW_SUB_CATEGORY where CATEGORY_ID = '" + categoryId + "' AND SUB_CATEGORY_ID = '"+ subCategoryId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY CATEGORY_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSubCategoryModel.CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                        objSubCategoryModel.SubCategoryId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString());
                        objSubCategoryModel.SubCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString();
                        objSubCategoryModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objSubCategoryModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSubCategoryModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSubCategoryModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSubCategoryModel;
        }
        public async Task<string> SaveSubCategory(SubCategoryModel objSubCategoryModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_sub_category_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubCategoryModel.CategoryId;
            objOracleCommand.Parameters.Add("p_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubCategoryModel.SubCategoryId;
            objOracleCommand.Parameters.Add("p_sub_category_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubCategoryModel.SubCategoryName) ? objSubCategoryModel.SubCategoryName : null;
            objOracleCommand.Parameters.Add("P_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubCategoryModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubCategoryModel.UpdateBy) ? objSubCategoryModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubCategoryModel.HeadOfficeId) ? objSubCategoryModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubCategoryModel.BranchOfficeId) ? objSubCategoryModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteSubCategory(int categoryId, int subCategoryId, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_sub_category_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
            objOracleCommand.Parameters.Add("p_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        public DataTable GetCategoryListDropdown()
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "CATEGORY_ID, " +
                         "CATEGORY_NAME " +
                         "FROM VEW_CATEGORY ";

            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    strConn.Open();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        #endregion

        #region "Sub Sub Category Section"
        public async Task<List<SubSubCategoryModel>> GetSubSubCategoryList(string headOfficeId, string branchOfficeId)
        {
            List<SubSubCategoryModel> objSubSubCategory = new List<SubSubCategoryModel>();

            var sql = "SELECT " +
                        "SUB_SUB_CATEGORY_ID," +
                        "SUB_SUB_CATEGORY_NAME," +
                        "SUB_CATEGORY_ID," +
                        "SUB_CATEGORY_NAME," +
                        "CATEGORY_ID," +
                        "CATEGORY_NAME," +
                        "DISPLAY_ORDER," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                         "FROM VEW_SUB_SUB_CATEGORY where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY SUB_CATEGORY_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();
                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        SubSubCategoryModel model = new SubSubCategoryModel
                        {
                            SubSubCategoryId = Convert.ToInt32(objDataReader["SUB_SUB_CATEGORY_ID"].ToString()),
                            SubSubCategoryName = objDataReader["SUB_SUB_CATEGORY_NAME"].ToString(),
                            SubCategoryId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString()),
                            SubCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString(),
                            CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString()),
                            CategoryName = objDataReader["CATEGORY_NAME"].ToString(),
                            DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString()),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSubSubCategory.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
           return objSubSubCategory;
        }
        public async Task<SubSubCategoryModel> GetSubSubCategory(int categoryId, int subCategoryId, int subSubCategoryId, string headOfficeId, string branchOfficeId)
        {
            SubSubCategoryModel objSubCategoryModel = new SubSubCategoryModel();

            var sql = "SELECT " +
                        "SUB_SUB_CATEGORY_ID," +
                        "SUB_SUB_CATEGORY_NAME," +
                        "SUB_CATEGORY_ID," +
                        "SUB_CATEGORY_NAME," +
                        "CATEGORY_ID," +
                        "CATEGORY_NAME," +
                        "DISPLAY_ORDER," +
                        "UPDATE_BY," +
                        "HEAD_OFFICE_ID," +
                        "BRANCH_OFFICE_ID " +
                        "FROM VEW_SUB_SUB_CATEGORY where CATEGORY_ID = '" + categoryId + "' AND SUB_CATEGORY_ID = '" + subCategoryId + "' AND SUB_SUB_CATEGORY_ID = '"+ subSubCategoryId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY SUB_CATEGORY_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSubCategoryModel.CategoryId = Convert.ToInt32(objDataReader["CATEGORY_ID"].ToString());
                        objSubCategoryModel.CategoryName = objDataReader["CATEGORY_NAME"].ToString();
                        objSubCategoryModel.SubCategoryId = Convert.ToInt32(objDataReader["SUB_CATEGORY_ID"].ToString());
                        objSubCategoryModel.SubCategoryName = objDataReader["SUB_CATEGORY_NAME"].ToString();
                        objSubCategoryModel.SubSubCategoryId = Convert.ToInt32(objDataReader["SUB_SUB_CATEGORY_ID"].ToString());
                        objSubCategoryModel.SubSubCategoryName = objDataReader["SUB_SUB_CATEGORY_NAME"].ToString();
                        objSubCategoryModel.DisplayOrder = Convert.ToInt32(objDataReader["DISPLAY_ORDER"].ToString());
                        objSubCategoryModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSubCategoryModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSubCategoryModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSubCategoryModel;
        }
        public async Task<string> SaveSubSubCategory(SubSubCategoryModel objSubSubCategoryModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_sub_sub_category_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubSubCategoryModel.CategoryId;
            objOracleCommand.Parameters.Add("p_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubSubCategoryModel.SubCategoryId;
            objOracleCommand.Parameters.Add("p_sub_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubSubCategoryModel.SubSubCategoryId;

            objOracleCommand.Parameters.Add("p_sub_sub_category_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubSubCategoryModel.SubSubCategoryName) ? objSubSubCategoryModel.SubSubCategoryName : null;
            objOracleCommand.Parameters.Add("P_display_order", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSubSubCategoryModel.DisplayOrder;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubSubCategoryModel.UpdateBy) ? objSubSubCategoryModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubSubCategoryModel.HeadOfficeId) ? objSubSubCategoryModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSubSubCategoryModel.BranchOfficeId) ? objSubSubCategoryModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteSubSubCategory(int categoryId, int subCategoryId, int subSubCategoryId, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_sub_sub_category_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = categoryId;
            objOracleCommand.Parameters.Add("p_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = subCategoryId;
            objOracleCommand.Parameters.Add("p_sub_sub_category_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = subSubCategoryId;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }

        public DataTable GetSubCategoryListDropdown(int categoryId)
        {
            DataTable dt = new DataTable();
            var sql = "SELECT " +
                         "SUB_CATEGORY_ID, " +
                         "SUB_CATEGORY_NAME " +
                         "FROM VEW_SUB_CATEGORY where CATEGORY_ID = '"+ categoryId +"' ";


            OracleCommand objCommand = new OracleCommand(sql);
            OracleDataAdapter objDataAdapter = new OracleDataAdapter(objCommand);
            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objCommand.Connection = strConn;
                    strConn.Open();
                    objDataAdapter.Fill(dt);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return dt;
        }
        #endregion

        #region "Size Section"
        public async Task<List<SizeModel>> GetSizeList(string headOfficeId, string branchOfficeId)
        {
            List<SizeModel> objSizeModels = new List<SizeModel>();

            var sql = "SELECT " +
                    "SIZE_ID," +
                    "SIZE_NAME," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID " +
                    "FROM VEW_SIZE where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY SIZE_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        SizeModel model = new SizeModel
                        {
                            SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString()),
                            SizeName = objDataReader["SIZE_NAME"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSizeModels.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSizeModels;
        }
        public async Task<SizeModel> GetSize(int sizeId, string headOfficeId, string branchOfficeId)
        {
            SizeModel objSizeModel = new SizeModel();

            var sql = "SELECT " +
                         "SIZE_ID," +
                         "SIZE_NAME," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_SIZE where SIZE_ID = '" + sizeId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY SIZE_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objSizeModel.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                        objSizeModel.SizeName = objDataReader["SIZE_NAME"].ToString();
                        objSizeModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objSizeModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objSizeModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSizeModel;
        }
        public async Task<string> SaveSize(SizeModel objSizeModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_size_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_size_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objSizeModel.SizeId;
            objOracleCommand.Parameters.Add("p_size_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeModel.SizeName) ? objSizeModel.SizeName : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeModel.UpdateBy) ? objSizeModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeModel.HeadOfficeId) ? objSizeModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objSizeModel.BranchOfficeId) ? objSizeModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteSize(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_size_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_size_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region "Ratio Section"
        public async Task<List<RatioModel>> GetRatioList(string headOfficeId, string branchOfficeId)
        {
            List<RatioModel> objSizeModels = new List<RatioModel>();

            var sql = "SELECT " +
                    "RATIO_ID," +
                    "RATIO_NAME," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID " +
                    "FROM VEW_RATIO where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY RATIO_NAME ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        RatioModel model = new RatioModel
                        {
                            RatioId = Convert.ToInt32(objDataReader["RATIO_ID"].ToString()),
                            RatioName = objDataReader["RATIO_NAME"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objSizeModels.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objSizeModels;
        }
        public async Task<RatioModel> GetRatio(int ratioId, string headOfficeId, string branchOfficeId)
        {
            RatioModel objRatioModel = new RatioModel();

            var sql = "SELECT " +
                         "RATIO_ID," +
                         "RATIO_NAME," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_RATIO where RATIO_ID = '" + ratioId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objRatioModel.RatioId = Convert.ToInt32(objDataReader["RATIO_ID"].ToString());
                        objRatioModel.RatioName = objDataReader["RATIO_NAME"].ToString();
                        objRatioModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objRatioModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objRatioModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objRatioModel;
        }
        public async Task<string> SaveRatio(RatioModel ratioModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_RATIO_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_ratio_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = ratioModel.RatioId;
            objOracleCommand.Parameters.Add("p_ratio_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(ratioModel.RatioName) ? ratioModel.RatioName : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(ratioModel.UpdateBy) ? ratioModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(ratioModel.HeadOfficeId) ? ratioModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(ratioModel.BranchOfficeId) ? ratioModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteRatio(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ratio_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_ratio_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region "Color Section"
        public async Task<List<ColorModel>> GetColorList(string headOfficeId, string branchOfficeId)
        {
            List<ColorModel> objColorModel = new List<ColorModel>();

            var sql = "SELECT " +
                    "COLOR_ID," +
                    "COLOR_NAME," +
                    "COLOR_CODE," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID " +
                    "FROM VEW_COLOR where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        ColorModel model = new ColorModel
                        {
                            ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString()),
                            ColorName = objDataReader["COLOR_NAME"].ToString(),
                            ColorCode = objDataReader["COLOR_CODE"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objColorModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objColorModel;
        }
        public async Task<ColorModel> GetColor(int colorId, string headOfficeId, string branchOfficeId)
        {
            ColorModel objColorModel = new ColorModel();

            var sql = "SELECT " +
                         "COLOR_ID," +
                         "COLOR_NAME," +
                         "COLOR_CODE," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_COLOR where COLOR_ID = '" + colorId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY COLOR_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objColorModel.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                        objColorModel.ColorName = objDataReader["COLOR_NAME"].ToString();
                        objColorModel.ColorCode = objDataReader["COLOR_CODE"].ToString();
                        objColorModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objColorModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objColorModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objColorModel;
        }
        public async Task<string> SaveColor(ColorModel colorModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_color_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_color_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = colorModel.ColorId;
            objOracleCommand.Parameters.Add("p_color_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(colorModel.ColorName) ? colorModel.ColorName : null;

            objOracleCommand.Parameters.Add("p_color_code", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(colorModel.ColorCode) ? colorModel.ColorCode : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(colorModel.UpdateBy) ? colorModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(colorModel.HeadOfficeId) ? colorModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(colorModel.BranchOfficeId) ? colorModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteColor(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_color_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_color_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region "Fabric Section"
        public async Task<List<FabricModel>> GetFabricList(string headOfficeId, string branchOfficeId)
        {
            List<FabricModel> objFabricModel = new List<FabricModel>();

            var sql = "SELECT " +
                    "FABRIC_ID," +
                    "FABRIC_NAME," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID " +
                    "FROM VEW_FABRIC where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        FabricModel model = new FabricModel
                        {
                            FabricId = Convert.ToInt32(objDataReader["FABRIC_ID"].ToString()),
                            FabricName = objDataReader["FABRIC_NAME"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objFabricModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objFabricModel;
        }
        public async Task<FabricModel> GetFabric(int fabricId, string headOfficeId, string branchOfficeId)
        {
            FabricModel objFabricModel = new FabricModel();

            var sql = "SELECT " +
                         "FABRIC_ID," +
                         "FABRIC_NAME," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_FABRIC where FABRIC_ID = '" + fabricId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY FABRIC_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objFabricModel.FabricId = Convert.ToInt32(objDataReader["FABRIC_ID"].ToString());
                        objFabricModel.FabricName = objDataReader["FABRIC_NAME"].ToString();
                        objFabricModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objFabricModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objFabricModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objFabricModel;
        }
        public async Task<string> SaveFabric(FabricModel objFabricModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_fabric_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_fabric_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objFabricModel.FabricId;
            objOracleCommand.Parameters.Add("p_fabric_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objFabricModel.FabricName) ? objFabricModel.FabricName : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objFabricModel.UpdateBy) ? objFabricModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objFabricModel.HeadOfficeId) ? objFabricModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objFabricModel.BranchOfficeId) ? objFabricModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteFabric(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_fabric_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_fabric_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion

        #region "Measurement Unit Section"
        public async Task<List<MeasurementUnitModel>> GetUnitList(string headOfficeId, string branchOfficeId)
        {
            List<MeasurementUnitModel> objMeasurementUnitModel = new List<MeasurementUnitModel>();

            var sql = "SELECT " +
                    "UNIT_ID," +
                    "UNIT_NAME," +
                    "UPDATE_BY," +
                    "HEAD_OFFICE_ID," +
                    "BRANCH_OFFICE_ID " +
                    "FROM VEW_MEASUREMENT_UNIT where HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        MeasurementUnitModel model = new MeasurementUnitModel
                        {
                            UnitId = Convert.ToInt32(objDataReader["UNIT_ID"].ToString()),
                            UnitName = objDataReader["UNIT_NAME"].ToString(),
                            UpdateBy = objDataReader["UPDATE_BY"].ToString(),
                            HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString(),
                            BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString()
                        };
                        objMeasurementUnitModel.Add(model);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objMeasurementUnitModel;
        }
        public async Task<MeasurementUnitModel> GetUnit(int unitId, string headOfficeId, string branchOfficeId)
        {
            MeasurementUnitModel objMeasurementUnitModel = new MeasurementUnitModel();

            var sql = "SELECT " +
                         "UNIT_ID," +
                         "UNIT_NAME," +
                         "UPDATE_BY," +
                         "HEAD_OFFICE_ID," +
                         "BRANCH_OFFICE_ID " +
                         "FROM VEW_MEASUREMENT_UNIT where UNIT_ID = '" + unitId + "' AND HEAD_OFFICE_ID = '" + headOfficeId.Trim() + "' AND BRANCH_OFFICE_ID = '" + branchOfficeId.Trim() + "' ORDER BY UNIT_ID ";

            OracleCommand objCommand = new OracleCommand(sql);

            using (OracleConnection strConn = GetConnection())
            {
                objCommand.Connection = strConn;
                await strConn.OpenAsync();
                var objDataReader = await objCommand.ExecuteReaderAsync();

                try
                {
                    while (await objDataReader.ReadAsync())
                    {
                        objMeasurementUnitModel.UnitId = Convert.ToInt32(objDataReader["UNIT_ID"].ToString());
                        objMeasurementUnitModel.UnitName = objDataReader["UNIT_NAME"].ToString();
                        objMeasurementUnitModel.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                        objMeasurementUnitModel.HeadOfficeId = objDataReader["HEAD_OFFICE_ID"].ToString();
                        objMeasurementUnitModel.BranchOfficeId = objDataReader["BRANCH_OFFICE_ID"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return objMeasurementUnitModel;
        }
        public async Task<string> SaveUnit(MeasurementUnitModel objMeasurementUnitModel)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_measurement_unit_save")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_unit_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = objMeasurementUnitModel.UnitId;
            objOracleCommand.Parameters.Add("p_unit_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objMeasurementUnitModel.UnitName) ? objMeasurementUnitModel.UnitName : null;

            objOracleCommand.Parameters.Add("p_update_by", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objMeasurementUnitModel.UpdateBy) ? objMeasurementUnitModel.UpdateBy : null;
            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objMeasurementUnitModel.HeadOfficeId) ? objMeasurementUnitModel.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objMeasurementUnitModel.BranchOfficeId) ? objMeasurementUnitModel.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        public async Task<string> DeleteUnit(int id, string headOfficeId, string branchOfficeId)
        {
            string strMessage;

            OracleCommand objOracleCommand = new OracleCommand("PRO_measurement_unit_delete")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("p_unit_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = id;

            objOracleCommand.Parameters.Add("p_head_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(headOfficeId) ? headOfficeId : null;
            objOracleCommand.Parameters.Add("p_branch_office_id", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(branchOfficeId) ? branchOfficeId : null;

            objOracleCommand.Parameters.Add("p_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMessage = objOracleCommand.Parameters["p_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                }
            }
            return strMessage;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class CatalogModel
    {
        public IEnumerable<ProductReviewModel> PendingReview { get; set; }
        public IEnumerable<ProductReviewModel> ApprovedReview { get; set; }
        public IEnumerable<ProductReviewModel> RejectedReview { get; set; }
    }

    public class ProductReviewModel
    {
        public int ProductReviewId { get; set; }
        public string ProductReviewDate { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string CustomerId { get; set; }

        public string CustomerName { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerPhone { get; set; }

        public double ProductRating { get; set; }

        public string Message { get; set; }

        public bool Approval { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }

        public bool Reject { get; set; }
        public string RejectBy { get; set; }
        public string RejectDate { get; set; }

    }

    public class UpcomingProduct
    {
        public int ProductId { get; set; }
        [Required]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }
        [Required]
        [DisplayName("SKU")]
        public string Sku { get; set; }
        [Required]
        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }
        public string ImageString { get; set; }
        public HttpPostedFileBase Image { get; set; }

        [Required]
        [DisplayName("Price")]
        public double Price { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class AccountDal
    {
        OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Inbox

        public async Task<IEnumerable<InboxModel>> GetCustomerMessage()
        {
            const string sql = "SELECT " +
                                "CONTACT_ID," +
                                "CUSTOMER_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "MESSAGE," +
                                "MACHINE_IP," +
                                "CONTACT_DATE," +
                                "RECEIVED_YN," +
                                "TRASH_YN " +
                                "FROM VEW_CUSTOMER_CONTACT WHERE TRASH_YN = 'N' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    //objCommand.Parameters.Add(":REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;
                    //objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            List<InboxModel> objInboxModels = new List<InboxModel>();

                            while (await objDataReader.ReadAsync())
                            {
                                InboxModel model = new InboxModel();

                                model.ContactId = Convert.ToInt32(objDataReader["CONTACT_ID"].ToString());
                                model.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.Message = objDataReader["MESSAGE"].ToString();
                                model.ShortMessage = model.Message.Substring(0, Math.Min(model.Message.Length, 30));
                                model.Date = objDataReader["CONTACT_DATE"].ToString();
                                model.ReadStatus = objDataReader["RECEIVED_YN"].ToString();

                                objInboxModels.Add(model);
                            }
                            return objInboxModels;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<InboxModel>> GetTrashMessage()
        {
            const string sql = "SELECT " +
                                "CONTACT_ID," +
                                "CUSTOMER_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "MESSAGE," +
                                "MACHINE_IP," +
                                "CONTACT_DATE," +
                                "RECEIVED_YN," +
                                "TRASH_YN " +
                                "FROM VEW_CUSTOMER_CONTACT WHERE TRASH_YN = 'Y' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    //objCommand.Parameters.Add(":REG_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = customerId;
                    //objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            List<InboxModel> objInboxModels = new List<InboxModel>();

                            while (await objDataReader.ReadAsync())
                            {
                                InboxModel model = new InboxModel();

                                model.ContactId = Convert.ToInt32(objDataReader["CONTACT_ID"].ToString());
                                model.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.Message = objDataReader["MESSAGE"].ToString();
                                model.ShortMessage = model.Message.Substring(0, Math.Min(model.Message.Length, 30));
                                model.Date = objDataReader["CONTACT_DATE"].ToString();
                                model.ReadStatus = objDataReader["RECEIVED_YN"].ToString();

                                objInboxModels.Add(model);
                            }
                            return objInboxModels;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<Compose>> GetSentMessageList()
        {
            const string sql =  "SELECT " +
                                "MESSAGE_ID," +
                                "EMAIL," +
                                "EMAIL_SUBJECT," +
                                "EMAIL_BODY," +
                                "MAIL_DATE," +
                                "EMPLOYEE_NAME," +
                                "UPDATE_BY," +
                                "HEAD_OFFICE_ID," +
                                "BRANCH_OFFICE_ID " +
                                "FROM VEW_SENT_MESSAGES ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            List<Compose> objComposeModel = new List<Compose>();

                            while (await objDataReader.ReadAsync())
                            {
                                Compose model = new Compose();

                                model.MessageId = objDataReader["MESSAGE_ID"].ToString();
                                model.ToEmail = objDataReader["EMAIL"].ToString();
                                model.MailSubject = objDataReader["EMAIL_SUBJECT"].ToString();
                                model.MailBody = objDataReader["EMAIL_BODY"].ToString();
                                model.ShortMessage = model.MailSubject.Substring(0, Math.Min(model.MailSubject.Length, 30));
                                model.Sender = objDataReader["EMPLOYEE_NAME"].ToString();
                                model.SentDate = objDataReader["MAIL_DATE"].ToString();
                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();

                                objComposeModel.Add(model);
                            }
                            return objComposeModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Compose> GetSentMessage(int messageId)
        {
            const string sql = "SELECT " +
                                "MESSAGE_ID," +
                                "EMAIL," +
                                "EMAIL_SUBJECT," +
                                "EMAIL_BODY," +
                                "MAIL_DATE," +
                                "EMPLOYEE_NAME," +
                                "UPDATE_BY," +
                                "HEAD_OFFICE_ID," +
                                "BRANCH_OFFICE_ID " +
                                "FROM VEW_SENT_MESSAGES WHERE MESSAGE_ID = :MESSAGE_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":MESSAGE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = messageId;

                    await objConnection.OpenAsync();

                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            Compose model = new Compose();

                            while (await objDataReader.ReadAsync())
                            {
                                model.MessageId = objDataReader["MESSAGE_ID"].ToString();
                                model.ToEmail = objDataReader["EMAIL"].ToString();
                                model.MailSubject = objDataReader["EMAIL_SUBJECT"].ToString();
                                model.MailBody = objDataReader["EMAIL_BODY"].ToString();
                                model.Sender = objDataReader["EMPLOYEE_NAME"].ToString();
                                model.SentDate = objDataReader["MAIL_DATE"].ToString();
                                model.UpdateBy = objDataReader["UPDATE_BY"].ToString();
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<InboxModel> GetCustomerMessage(int contactId)
        {
            const string sql = "SELECT " +
                                "CONTACT_ID," +
                                "CUSTOMER_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "MESSAGE," +
                                "MACHINE_IP," +
                                "CONTACT_DATE," +
                                "RECEIVED_YN " +
                                "FROM VEW_CUSTOMER_CONTACT WHERE CONTACT_ID = :CONTACT_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":CONTACT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = contactId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        try
                        {
                            InboxModel model = new InboxModel();

                            while (await objDataReader.ReadAsync())
                            {
                                model.ContactId = Convert.ToInt32(objDataReader["CONTACT_ID"].ToString());
                                model.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                model.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                model.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                model.Message = objDataReader["MESSAGE"].ToString();
                                model.Date = objDataReader["CONTACT_DATE"].ToString();
                                model.ReadStatus = objDataReader["RECEIVED_YN"].ToString();
                            }
                            return model;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> MakeMessageRead(int contactId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_CONTACT_READ")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_CONTACT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = contactId != 0 ? contactId : 0;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> MessageDelete(int contactId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_CUSTOMER_CONTACT_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_CONTACT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = contactId != 0 ? (object)contactId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> SaveReplayMessage(Compose objCompose)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_SENT_MESSAGES_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };

            objOracleCommand.Parameters.Add("P_MESSAGE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = null;
            objOracleCommand.Parameters.Add("P_CUSTOMER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.CustomerId) ? objCompose.CustomerId : null;
            objOracleCommand.Parameters.Add("P_CONTACT_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.ContactId) ? objCompose.ContactId : null;

            objOracleCommand.Parameters.Add("P_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.ToEmail) ? objCompose.ToEmail : null;
            objOracleCommand.Parameters.Add("P_EMAIL_SUBJECT", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.MailSubject) ? objCompose.MailSubject : null;
            objOracleCommand.Parameters.Add("P_EMAIL_BODY", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.MailBody) ? objCompose.MailBody : null;

            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.UpdateBy) ? objCompose.UpdateBy : null;
            objOracleCommand.Parameters.Add("P_HEAD_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.HeadOfficeId) ? objCompose.HeadOfficeId : null;
            objOracleCommand.Parameters.Add("P_BRANCH_OFFICE_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(objCompose.BranchOfficeId) ? objCompose.BranchOfficeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion


    }
}
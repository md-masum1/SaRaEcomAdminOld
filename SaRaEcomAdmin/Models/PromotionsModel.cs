﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class PromotionsModel
    {
    }

    public class Discount
    {
        public int DiscountId { get; set; }

        [DisplayName("Discount Name")]
        public string DiscountName { get; set; }

        [DisplayName("Discount Type")]
        public string DiscountType { get; set; }

        [DisplayName("Category")]
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }

        [DisplayName("Discount Percentage")]
        public int? DiscountPercentage { get; set; }

        [DisplayName("Use Amount")]
        public bool UseAmount { get; set; }

        [DisplayName("Discount Amount")]
        public double? DiscountAmount { get; set; }

        [DisplayName("Coupon Code")]
        public string CouponCode { get; set; }

        [DisplayName("Start Date")]
        public string StartDate { get; set; }

        [DisplayName("End Date")]
        public string EndDate { get; set; }

        [DisplayName("Status")]
        public bool IsActive { get; set; } = true;

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }

    }
}
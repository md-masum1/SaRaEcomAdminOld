﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountDal _accountDal = new AccountDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        #region Inbox

        public async Task<ActionResult> Inbox()
        {
            AccountModel model = new AccountModel();

            model.GetMessageList = await _accountDal.GetCustomerMessage();
            model.GetTrashList = await _accountDal.GetTrashMessage();
            model.GetSentMessage = await _accountDal.GetSentMessageList();

            return View(model);
        }

        public async Task<ActionResult> GetMessage(int contactId)
        {
            InboxModel model = new InboxModel();

            if (contactId != 0)
            {
                model = await _accountDal.GetCustomerMessage(contactId);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> MessageMakeRead(int contactId)
        {
            string message = "";

            if (contactId != 0)
            {
                message = await _accountDal.MakeMessageRead(contactId);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> MessageDelete(int contactId)
        {
            string message = "";

            if (contactId != 0)
            {
                message = await _accountDal.MessageDelete(contactId);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> GetSentMessage(int messageId)
        {
            Compose model = new Compose();

            if (messageId != 0)
            {
                model = await _accountDal.GetSentMessage(messageId);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ReplayMessage(Compose objCompose)
        {
            LoadSession();

            string message = "";

            if (objCompose != null)
            {
                objCompose.UpdateBy = _strEmployeeId;
                objCompose.HeadOfficeId = _strHeadOfficeId;
                objCompose.BranchOfficeId = _strBranchOfficeId;

                message = await _accountDal.SaveReplayMessage(objCompose);


                new Thread(() =>
                {
                    string body = objCompose.MailBody;
                    UtilityClass.SendEmail(objCompose.ToEmail, objCompose.MailSubject, body, null);

                }).Start();
            }
            

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
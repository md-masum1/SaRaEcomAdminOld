﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace SaRaEcomAdmin.Models
{
    public class ProductInventoryModel
    {
        public int ProductId { get; set; }

        [Required]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [Required]
        [DisplayName("SKU")]
        public string Sku { get; set; }

        [Required]
        [DisplayName("Short Description")]
        public string ShortDescription { get; set; }

        [Required]
        [AllowHtml]
        [DisplayName("Full Description")]
        public string FullDescription { get; set; }

        public string MapString { get; set; }
        public List<MapList> MapStringList { get; set; }

        [Required]
        [DisplayName("Category")]
        public string CategoryId { get; set; }

        [Required]
        [DisplayName("Sub Category")]
        public string SubCategoryId { get; set; }

        [Required]
        [DisplayName("Sub Sub Category")]
        public string SubSubCategoryId { get; set; }

        [Required]
        [DisplayName("Fabric")]
        public string FabricId { get; set; }

        [Required]
        [DisplayName("Measurement Unit")]
        public string MeasurementUnitId { get; set; }

        [Required]
        [DisplayName("Purchase Price")]
        public float? PurchasePrice { get; set; }

        [Required]
        [DisplayName("Selling Price")]
        public float? SellingPrice { get; set; }

        [DisplayName("Discount")]
        public float? Discount { get; set; }

        [DataType(DataType.Date)]
        public string DiscountStarTime { get; set; }

        //[DataType(DataType.Date)]
        [DisplayName("Discount End")]
        public string DiscountEndTime { get; set; }

        [DisplayName("Including VAT")]
        public bool IncludeVat { get; set; } = true;

        [DisplayName("Active Status")]
        public bool ActiveStatus { get; set; } = false;

        [Required]
        [DisplayName("Merchandiser")]
        public string MerchandiserId { get; set; }

        [Required]
        [DisplayName("Designer")]
        public string DesignerId { get; set; }

        [Required]
        [DisplayName("Primary Image")]
        public HttpPostedFileBase PrimaryImage { get; set; }
        public string PrimaryImageString { get; set; }
        public string PrimaryImageUrl { get; set; }

        [DisplayName("Color")]
        public int? ColorId { get; set; }

        [DisplayName("Display Order")]
        public int? DisplayOrder { get; set; }

        public int ImageId { get; set; }
        [DisplayName("Product Image")]
        public HttpPostedFileBase Image1Base { get; set; }
        public string ImageString1 { get; set; }
        public HttpPostedFileBase Image2Base { get; set; }
        public string ImageString2 { get; set; }
        public HttpPostedFileBase Image3Base { get; set; }
        public string ImageString3 { get; set; }
        public HttpPostedFileBase Image4Base { get; set; }
        public string ImageString4 { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class MapList
    {
        public string MapId { get; set; }

        public string MapName { get; set; }

        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int SubSubCategoryId { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class ProductGrid
    {
        public bool IsActive { get; set; }

        public string ProductId { get; set; }

        public byte[] ProductImageBytes { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageUrl { get; set; }

        public string ProductName { get; set; }

        public string Sku { get; set; }

        public string PurchasePrice { get; set; }
        public string SellPrice { get; set; }

        public string ProductQuantity { get; set; }

        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string SubSubCategoryName { get; set; }
        public string UnitName { get; set; }
        public string FabricName { get; set; }

        public string SearchBy { get; set; }
        public string UpdateBy { get; set; }
        public string OrderByName { get; set; }
        public string OrderByDirection { get; set; }

        public string HeadOfficeId { get; set; }

        public string BranchOfficeId { get; set; }
    }

    public class ProductImageGrid
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ColorId { get; set; }

        public string ColorName { get; set; }

        public string Sku { get; set; }

        public int ProductImageId { get; set; }

        public int? DisplayOrder { get; set; }

        public string Image1 { get; set; }
        public byte[] Image1Bytes { get; set; }

        public string Image2 { get; set; }
        public byte[] Image2Bytes { get; set; }

        public string Image3 { get; set; }
        public byte[] Image3Bytes { get; set; }

        public string Image4 { get; set; }
        public byte[] Image4Bytes { get; set; }
    }

    public class SizeAndQuantitySave
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int ColorId { get; set; }
        [Required]
        public string ProductEntryDate { get; set; }
        public string EntryDate { get; set; }
        [Required]
        public int[] SizeIdList { get; set; }
        [Required]
        public int[] QuantityList { get; set; }

        public double[] PriceList { get; set; }


        public int SizeId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }

    public class SizeAndRatioSave
    {
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int RatioId { get; set; }
        [Required]
        public int[] SizeIdList { get; set; }
        [Required]
        public float[] SizeValueList { get; set; }

        public int SizeId { get; set; }
        public float SizeValue { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }


    #region "Ratio Chart"
    public class RatioChartDisplay
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImageString { get; set; }
        public string Sku { get; set; }

        public IEnumerable<RatioList> RatioLists { get; set; }
    }

    public class RatioList
    {
        public int RatioId { get; set; }
        public string RatioName { get; set; }

        public IEnumerable<RatioSizeList> RatioSizeLists { get; set; }
    }

    public class RatioSizeList
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public float SizeValue { get; set; }
    }
    #endregion


    #region "Inventory Grid"
    public class PeoductInventoryDisplay
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImageString { get; set; }
        public string Sku { get; set; }
        public string StyleNumber { get; set; }

        public string TotalColor { get; set; }
        public string TotalSize { get; set; }
        public string TotalQuantity { get; set; }

        public IEnumerable<ColorList> ColorLists { get; set; }
    }

    public class ColorList
    {
        public int ColorId { get; set; }
        public string ColorName { get; set; }

        public IEnumerable<SizeList> SizeLists { get; set; }
    }

    public class SizeList
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
    #endregion
}
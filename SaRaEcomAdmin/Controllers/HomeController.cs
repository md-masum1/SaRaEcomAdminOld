﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeDal _homeDal = new HomeDal();

        public void LoadSession()
        {
            var employee = Session["authentication"] as AuthModel;
        }

        public async Task<ActionResult> Index()
        {
            var employee = Session["authentication"] as AuthModel;

            if (employee != null)
            {
                var topSummery = await _homeDal.DashBoardTopReport();

                return View(topSummery);
            }

            return RedirectToAction("Index", "Auth");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error()
        {
            return View("Error");
        }
    }
}
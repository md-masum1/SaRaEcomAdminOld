﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using SaRaEcomAdmin.Models;

namespace SaRaEcomAdmin.DAL
{
    public class SalesDal
    {
        private OracleTransaction _trans;

        #region "Oracle Connection Check"
        private OracleConnection GetConnection()
        {
            var conString = ConfigurationManager.ConnectionStrings["OracleDbContext"];
            string strConnString = conString.ConnectionString;
            return new OracleConnection(strConnString);
        }
        #endregion

        #region Order Section

        public async Task<IEnumerable<Orders>> GetOrdersList()
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                                "CUSTOMER_ID," +
                                "FIRST_NAME," +
                                "LAST_NAME," +
                                "EMAIL," +
                                "PHONE," +
                                "ADDRESS_FIRST," +
                                "ADDRESS_SECOND," +
                                "CITY," +
                                "COUNTRY," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS," +
                                "TOTAL_AMOUNT " +
                               "FROM VEW_CUSTOMER_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Orders> objOrderList = new List<Orders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Orders order = new Orders
                                {
                                    OrderNumber = objDataReader["ORDER_NUMBER"].ToString(),
                                    OrderDate = objDataReader["ORDER_DATE"].ToString(),
                                    CustomerId = objDataReader["CUSTOMER_ID"].ToString(),
                                    CustomerName = objDataReader["FIRST_NAME"].ToString() + " " +
                                                   objDataReader["LAST_NAME"].ToString(),
                                    Email = objDataReader["EMAIL"].ToString(),
                                    PhoneNumber = objDataReader["PHONE"].ToString(),
                                    Address1 = objDataReader["ADDRESS_FIRST"].ToString(),
                                    Address2 = objDataReader["ADDRESS_SECOND"].ToString(),
                                    City = objDataReader["CITY"].ToString(),
                                    Country = objDataReader["COUNTRY"].ToString()
                                };


                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    order.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    order.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    order.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    order.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    order.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    order.PaymentType = "Master Card";
                                else if(objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    order.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    order.PaymentType = "bKash Mobile Banking";

                                order.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                order.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Orders> GetSingleOrder(string orderNumber)
        {
            const string sql = "SELECT " +
                               "ORDER_NUMBER," +
                               "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                               "CUSTOMER_ID," +
                               "REG_CUSTOMER_ID," +
                               "FIRST_NAME," +
                               "LAST_NAME," +
                               "EMAIL," +
                               "PHONE," +
                               "ADDRESS_FIRST," +
                               "ADDRESS_SECOND," +
                               "CITY," +
                               "COUNTRY," +
                               "SHIPPING_COST," +
                               "PAYMENT_TYPE," +
                               "PAYMENT_STATUS," +
                               "DISCOUNT_PERCENT," +
                               "TOTAL_AMOUNT " +
                               "FROM VEW_CUSTOMER_ORDER WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        Orders objOrderModel = new Orders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objOrderModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objOrderModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objOrderModel.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                objOrderModel.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                objOrderModel.Email = objDataReader["EMAIL"].ToString();
                                objOrderModel.PhoneNumber = objDataReader["PHONE"].ToString();
                                objOrderModel.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                objOrderModel.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                objOrderModel.City = objDataReader["CITY"].ToString();
                                objOrderModel.Country = objDataReader["COUNTRY"].ToString();
                                objOrderModel.ShippingCost = Convert.ToInt32(objDataReader["SHIPPING_COST"].ToString());

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    objOrderModel.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    objOrderModel.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    objOrderModel.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    objOrderModel.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    objOrderModel.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    objOrderModel.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    objOrderModel.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    objOrderModel.PaymentType = "bKash Mobile Banking";

                                objOrderModel.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                objOrderModel.Discount = objDataReader["DISCOUNT_PERCENT"].ToString();
                                objOrderModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                            }
                            return objOrderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<OrderedProduct>> GetOrderedProduct(string orderNumber)
        {
            const string sql = "SELECT " +
                                "ORDER_ID," +
                                "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE," +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE " +
                               "FROM VEW_ORDERED_PRODUCT WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<OrderedProduct> objOrderedProduct = new List<OrderedProduct>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                OrderedProduct orderedProduct = new OrderedProduct();

                                orderedProduct.OrderId = objDataReader["ORDER_ID"].ToString();
                                orderedProduct.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                orderedProduct.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                orderedProduct.Sku = objDataReader["SKU"].ToString();
                                orderedProduct.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                orderedProduct.ColorName = objDataReader["COLOR_NAME"].ToString();
                                orderedProduct.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                                orderedProduct.SizeName = objDataReader["SIZE_NAME"].ToString();
                                orderedProduct.Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString());
                                orderedProduct.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());

                                objOrderedProduct.Add(orderedProduct);
                            }
                            return objOrderedProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> OrderConfirm(string orderNumber)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ORDER_CONFIRM_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> DeleteOrderItem(string orderNumber, string orderId, string updateBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ORDER_ITEM_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;
            objOracleCommand.Parameters.Add("P_ORDER_ID", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderId) ? orderId : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(updateBy) ? updateBy : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> OrderDelete(string orderNumber, string remarks, string updateBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_ORDER_DELETE")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;
            objOracleCommand.Parameters.Add("P_REJECT_REMARKS", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(remarks) ? remarks : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(updateBy) ? updateBy : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion

        #region PreOrder Section

        public async Task<IEnumerable<PreOrders>> GetPreOrdersList()
        {
            const string sql = "SELECT " +
                               "PRE_ORDER_ID," +
                               "CUSTOMER_ID," +
                               "CUSTOMER_NAME," +
                               "CUSTOMER_PHONE," +
                               "CUSTOMER_EMAIL," +
                               "CUSTOMER_ADDRESS," +
                               "UPCOMING_PRODUCT_ID," +
                               "UPCOMING_PRODUCT_NAME," +
                               "UPCOMING_PRODUCT_SKU," +
                               "UPCOMING_PRODUCT_IMAGE," +
                               "UPCOMING_PRODUCT_PRICE," +
                               "ORDER_DATE," +
                               "APPROVE_YN," +
                               "POSSIBLE_DELIVERY_DATE," +
                               "APPROVED_REMARKS," +
                               "REJECT_YN," +
                               "REJECT_REMARKS " +
                               "FROM VEW_PRE_ORDER WHERE APPROVE_YN = 'N' AND REJECT_YN = 'N' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<PreOrders> objOrderList = new List<PreOrders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                PreOrders order = new PreOrders();

                                order.OrderNumber = objDataReader["PRE_ORDER_ID"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();

                                order.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                order.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                order.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                order.CustomerPhone = objDataReader["CUSTOMER_PHONE"].ToString();
                                order.CustomerAddress = objDataReader["CUSTOMER_ADDRESS"].ToString();

                                order.ProductId = objDataReader["UPCOMING_PRODUCT_ID"].ToString();
                                order.ProductName = objDataReader["UPCOMING_PRODUCT_NAME"].ToString();
                                order.Sku = objDataReader["UPCOMING_PRODUCT_SKU"].ToString();
                                order.ProductImage = objDataReader["UPCOMING_PRODUCT_IMAGE"].ToString();
                                order.ProductPrice = objDataReader["UPCOMING_PRODUCT_PRICE"].ToString();

                                order.Approve = objDataReader["APPROVE_YN"].ToString() == "Y";

                                order.DeliveryDate = objDataReader["POSSIBLE_DELIVERY_DATE"].ToString();
                                order.ApproveRemarks = objDataReader["APPROVED_REMARKS"].ToString();

                                order.Reject = objDataReader["REJECT_YN"].ToString() == "Y";
                                order.RejectRemarks = objDataReader["REJECT_REMARKS"].ToString();

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<PreOrders> GetSinglePreOrder(string orderNumber)
        {
            string sql = "SELECT " +
                         "PRE_ORDER_ID," +
                         "CUSTOMER_ID," +
                         "CUSTOMER_NAME," +
                         "CUSTOMER_PHONE," +
                         "CUSTOMER_EMAIL," +
                         "CUSTOMER_ADDRESS," +
                         "UPCOMING_PRODUCT_ID," +
                         "UPCOMING_PRODUCT_NAME," +
                         "UPCOMING_PRODUCT_SKU," +
                         "UPCOMING_PRODUCT_IMAGE," +
                         "UPCOMING_PRODUCT_PRICE," +
                         "ORDER_DATE," +
                         "APPROVE_YN," +
                         "POSSIBLE_DELIVERY_DATE," +
                         "APPROVED_REMARKS," +
                         "REJECT_YN," +
                         "REJECT_REMARKS " +
                         "FROM VEW_PRE_ORDER WHERE PRE_ORDER_ID = :PRE_ORDER_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":PRE_ORDER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        PreOrders order = new PreOrders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                order.OrderNumber = objDataReader["PRE_ORDER_ID"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();

                                order.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                order.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                order.CustomerEmail = objDataReader["CUSTOMER_EMAIL"].ToString();
                                order.CustomerPhone = objDataReader["CUSTOMER_PHONE"].ToString();
                                order.CustomerAddress = objDataReader["CUSTOMER_ADDRESS"].ToString();

                                order.ProductId = objDataReader["UPCOMING_PRODUCT_ID"].ToString();
                                order.ProductName = objDataReader["UPCOMING_PRODUCT_NAME"].ToString();
                                order.Sku = objDataReader["UPCOMING_PRODUCT_SKU"].ToString();
                                order.ProductImage = objDataReader["UPCOMING_PRODUCT_IMAGE"].ToString();
                                order.ProductPrice = objDataReader["UPCOMING_PRODUCT_PRICE"].ToString();

                                order.Approve = objDataReader["APPROVE_YN"].ToString() == "Y";

                                order.DeliveryDate = objDataReader["POSSIBLE_DELIVERY_DATE"].ToString();
                                order.ApproveRemarks = objDataReader["APPROVED_REMARKS"].ToString();

                                order.Reject = objDataReader["REJECT_YN"].ToString() == "Y";
                                order.RejectRemarks = objDataReader["REJECT_REMARKS"].ToString();
                            }
                            return order;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> PreOrderConfirm(string orderNumber, string deliveryDate, string approveRemarks, string employeeId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRE_ORDER_CONFIRM")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;
            objOracleCommand.Parameters.Add("P_DELIVERY_DATE", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(deliveryDate) ? Convert.ToDateTime(deliveryDate).ToString("dd/MM/yyyy").Replace('-', '/') : null;
            objOracleCommand.Parameters.Add("P_APPROVE_REMARKS", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(approveRemarks) ? approveRemarks : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(employeeId) ? employeeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    _trans.Dispose();
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> PreOrderDelete(string orderNumber, string rejectRemarks, string employeeId)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_PRE_ORDER_REJECT")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;
            objOracleCommand.Parameters.Add("P_REJECT_REMARKS", OracleDbType.NVarchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(rejectRemarks) ? rejectRemarks : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(employeeId) ? employeeId : null;

            objOracleCommand.Parameters.Add("P_MESSAGE", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion

        #region Delivery Section

        public async Task<IEnumerable<Orders>> GetDeliveryList()
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                                "TO_CHAR (DELEVERY_DATE, 'dd/mm/yyyy hh12:mi:ssam') DELEVERY_DATE," +
                                "CUSTOMER_ID," +
                                "FIRST_NAME," +
                                "LAST_NAME," +
                                "EMAIL," +
                                "PHONE," +
                                "ADDRESS_FIRST," +
                                "ADDRESS_SECOND," +
                                "CITY," +
                                "COUNTRY," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS," +
                                "TOTAL_AMOUNT " +
                               "FROM VEW_DELIVERED_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Orders> objOrderList = new List<Orders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Orders order = new Orders();

                                order.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                order.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                order.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                order.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                order.Email = objDataReader["EMAIL"].ToString();
                                order.PhoneNumber = objDataReader["PHONE"].ToString();
                                order.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                order.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                order.City = objDataReader["CITY"].ToString();
                                order.Country = objDataReader["COUNTRY"].ToString();

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    order.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    order.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    order.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    order.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    order.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    order.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    order.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    order.PaymentType = "bKash Mobile Banking";

                                order.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                order.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Orders> GetSingleDelivery(string orderNumber)
        {
            string sql = "SELECT " +
                           "ORDER_NUMBER," +
                           "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                           "TO_CHAR (DELEVERY_DATE, 'dd/mm/yyyy hh12:mi:ssam') DELEVERY_DATE," +
                           "CUSTOMER_ID," +
                           "FIRST_NAME," +
                           "LAST_NAME," +
                           "EMAIL," +
                           "PHONE," +
                           "ADDRESS_FIRST," +
                           "ADDRESS_SECOND," +
                           "CITY," +
                           "COUNTRY," +
                           "SHIPPING_COST," +
                           "PAYMENT_TYPE," +
                           "PAYMENT_STATUS," +
                           "DISCOUNT_PERCENT," +
                           "TOTAL_AMOUNT " +
                           "FROM VEW_DELIVERED_ORDER WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        Orders objOrderModel = new Orders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objOrderModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objOrderModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objOrderModel.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                objOrderModel.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                objOrderModel.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                objOrderModel.Email = objDataReader["EMAIL"].ToString();
                                objOrderModel.PhoneNumber = objDataReader["PHONE"].ToString();
                                objOrderModel.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                objOrderModel.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                objOrderModel.City = objDataReader["CITY"].ToString();
                                objOrderModel.Country = objDataReader["COUNTRY"].ToString();
                                objOrderModel.ShippingCost = Convert.ToInt32(objDataReader["SHIPPING_COST"].ToString());

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    objOrderModel.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    objOrderModel.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    objOrderModel.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    objOrderModel.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    objOrderModel.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    objOrderModel.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    objOrderModel.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    objOrderModel.PaymentType = "bKash Mobile Banking";

                                objOrderModel.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                objOrderModel.Discount = objDataReader["DISCOUNT_PERCENT"].ToString();
                                objOrderModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                            }
                            return objOrderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> ReturnConfirm(string orderNumber)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_RETURN_CONFIRM_SAVE")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(orderNumber) ? orderNumber : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion

        #region Return Section

        public async Task<IEnumerable<Orders>> GetReturnList()
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                                "TO_CHAR (DELEVERY_DATE, 'dd/mm/yyyy hh12:mi:ssam') DELEVERY_DATE," +
                                "TO_CHAR (RETURN_DATE, 'dd/mm/yyyy hh12:mi:ssam') RETURN_DATE," +
                                "CUSTOMER_ID," +
                                "FIRST_NAME," +
                                "LAST_NAME," +
                                "EMAIL," +
                                "PHONE," +
                                "ADDRESS_FIRST," +
                                "ADDRESS_SECOND," +
                                "CITY," +
                                "COUNTRY," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS," +
                                "TOTAL_AMOUNT " +
                               "FROM VEW_RETURN_PRODUCT ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Orders> objOrderList = new List<Orders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Orders order = new Orders();

                                order.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                order.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                order.ReturnDate = objDataReader["RETURN_DATE"].ToString();
                                order.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                order.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                order.Email = objDataReader["EMAIL"].ToString();
                                order.PhoneNumber = objDataReader["PHONE"].ToString();
                                order.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                order.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                order.City = objDataReader["CITY"].ToString();
                                order.Country = objDataReader["COUNTRY"].ToString();

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    order.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    order.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    order.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    order.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    order.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    order.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    order.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    order.PaymentType = "bKash Mobile Banking";

                                order.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                order.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Orders> GetSingleReturn(string orderNumber)
        {
            string sql = "SELECT " +
                           "ORDER_NUMBER," +
                           "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                           "TO_CHAR (DELEVERY_DATE, 'dd/mm/yyyy hh12:mi:ssam') DELEVERY_DATE," +
                           "TO_CHAR (RETURN_DATE, 'dd/mm/yyyy hh12:mi:ssam') RETURN_DATE," +
                           "CUSTOMER_ID," +
                           "FIRST_NAME," +
                           "LAST_NAME," +
                           "EMAIL," +
                           "PHONE," +
                           "ADDRESS_FIRST," +
                           "ADDRESS_SECOND," +
                           "CITY," +
                           "COUNTRY," +
                           "SHIPPING_COST," +
                           "PAYMENT_TYPE," +
                           "PAYMENT_STATUS," +
                           "DISCOUNT_PERCENT," +
                           "TOTAL_AMOUNT " +
                           "FROM VEW_RETURN_PRODUCT WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        Orders objOrderModel = new Orders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objOrderModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objOrderModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objOrderModel.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
                                objOrderModel.ReturnDate = objDataReader["RETURN_DATE"].ToString();
                                objOrderModel.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                objOrderModel.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                objOrderModel.Email = objDataReader["EMAIL"].ToString();
                                objOrderModel.PhoneNumber = objDataReader["PHONE"].ToString();
                                objOrderModel.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                objOrderModel.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                objOrderModel.City = objDataReader["CITY"].ToString();
                                objOrderModel.Country = objDataReader["COUNTRY"].ToString();
                                objOrderModel.ShippingCost = Convert.ToInt32(objDataReader["SHIPPING_COST"].ToString());

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    objOrderModel.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    objOrderModel.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    objOrderModel.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    objOrderModel.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    objOrderModel.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    objOrderModel.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    objOrderModel.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    objOrderModel.PaymentType = "bKash Mobile Banking";

                                objOrderModel.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                objOrderModel.Discount = objDataReader["DISCOUNT_PERCENT"].ToString();
                                objOrderModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                            }
                            return objOrderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Return Request Section

        public async Task<IEnumerable<Orders>> GetReturnRequestList()
        {
            const string sql = "SELECT " +
                               "RETURN_REQUEST_ID," +
                                "CUSTOMER_NAME," +
                                "CUSTOMER_EMAIL," +
                                "CUSTOMER_PHONE," +
                                "CUSTOMER_ADDRESS," +
                                "CITY," +
                                "COUNTRY," +
                                "RETURN_REQUEST_DATE," +
                                "ORDER_DATE," +
                                "DELIVERY_DATE," +
                                "RETURN_REASON," +
                                "EXCHANGE_TYPE," +
                                "COMMENTS," +
                                "ORDER_NUMBER," +
                                "RETURN_YN," +
                                "REJECT_YN " +
                               "FROM VEW_RETURN_REQUEST WHERE RETURN_YN = 'N' AND REJECT_YN = 'N' ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Orders> objOrderList = new List<Orders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Orders order = new Orders();

                                order.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                order.DeliveryDate = objDataReader["DELIVERY_DATE"].ToString();
                                order.ReturnDate = objDataReader["RETURN_REQUEST_DATE"].ToString();
                                order.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                order.Email = objDataReader["CUSTOMER_EMAIL"].ToString();
                                order.PhoneNumber = objDataReader["CUSTOMER_PHONE"].ToString();
                                order.Address1 = objDataReader["CUSTOMER_ADDRESS"].ToString();
                                order.City = objDataReader["CITY"].ToString();
                                order.Country = objDataReader["COUNTRY"].ToString();

                                order.ReturnRequestId = Convert.ToInt32(objDataReader["RETURN_REQUEST_ID"].ToString());
                                order.ReturnReason = objDataReader["RETURN_REASON"].ToString();
                                order.ExchangeType = objDataReader["EXCHANGE_TYPE"].ToString();
                                order.Comment = objDataReader["COMMENTS"].ToString();

                                order.ReturnStatus = objDataReader["RETURN_YN"].ToString() == "Y";
                                order.RejectStatus = objDataReader["REJECT_YN"].ToString() == "Y";

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Orders> GetSingleReturnRequest(string returnRequestId)
        {
            const string sql = "SELECT " +
                               "RETURN_REQUEST_ID," +
                               "CUSTOMER_NAME," +
                               "CUSTOMER_EMAIL," +
                               "CUSTOMER_PHONE," +
                               "CUSTOMER_ADDRESS," +
                               "CITY," +
                               "COUNTRY," +
                               "RETURN_REQUEST_DATE," +
                               "ORDER_DATE," +
                               "DELIVERY_DATE," +
                               "RETURN_REASON," +
                               "EXCHANGE_TYPE," +
                               "COMMENTS," +
                               "ORDER_NUMBER," +
                               "RETURN_YN," +
                               "REJECT_YN " +
                               "FROM VEW_RETURN_REQUEST WHERE RETURN_REQUEST_ID = :RETURN_REQUEST_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":RETURN_REQUEST_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnRequestId;


                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        Orders objOrderModel = new Orders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objOrderModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objOrderModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objOrderModel.DeliveryDate = objDataReader["DELIVERY_DATE"].ToString();
                                objOrderModel.ReturnDate = objDataReader["RETURN_REQUEST_DATE"].ToString();
                                objOrderModel.CustomerName = objDataReader["CUSTOMER_NAME"].ToString();
                                objOrderModel.Email = objDataReader["CUSTOMER_EMAIL"].ToString();
                                objOrderModel.PhoneNumber = objDataReader["CUSTOMER_PHONE"].ToString();
                                objOrderModel.Address1 = objDataReader["CUSTOMER_ADDRESS"].ToString();
                                objOrderModel.City = objDataReader["CITY"].ToString();
                                objOrderModel.Country = objDataReader["COUNTRY"].ToString();

                                objOrderModel.ReturnRequestId = Convert.ToInt32(objDataReader["RETURN_REQUEST_ID"].ToString());
                                objOrderModel.ReturnReason = objDataReader["RETURN_REASON"].ToString();
                                objOrderModel.ExchangeType = objDataReader["EXCHANGE_TYPE"].ToString();
                                objOrderModel.Comment = objDataReader["COMMENTS"].ToString();

                                objOrderModel.ReturnStatus = objDataReader["RETURN_YN"].ToString() == "Y";
                                objOrderModel.RejectStatus = objDataReader["REJECT_YN"].ToString() == "Y";
                            }
                            return objOrderModel;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<IEnumerable<OrderedProduct>> GetReturnRequestedProduct(int returnRequestId)
        {
            const string sql = "SELECT " +
                               "RETURN_REQUEST_ID," +
                                "ORDER_NUMBER," +
                                "PRODUCT_ID," +
                                "PRODUCT_NAME," +
                                "SKU," +
                                "COLOR_ID," +
                                "COLOR_NAME," +
                                "SIZE_ID," +
                                "SIZE_NAME," +
                                "QUANTITY," +
                                "PRICE " +
                               "FROM VEW_RETURN_REQ_PRODUCT WHERE RETURN_REQUEST_ID = :RETURN_REQUEST_ID ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":RETURN_REQUEST_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = returnRequestId;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<OrderedProduct> objOrderedProduct = new List<OrderedProduct>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                OrderedProduct orderedProduct = new OrderedProduct();

                                orderedProduct.ProductId = Convert.ToInt32(objDataReader["PRODUCT_ID"].ToString());
                                orderedProduct.ProductName = objDataReader["PRODUCT_NAME"].ToString();
                                orderedProduct.Sku = objDataReader["SKU"].ToString();
                                orderedProduct.ColorId = Convert.ToInt32(objDataReader["COLOR_ID"].ToString());
                                orderedProduct.ColorName = objDataReader["COLOR_NAME"].ToString();
                                orderedProduct.SizeId = Convert.ToInt32(objDataReader["SIZE_ID"].ToString());
                                orderedProduct.SizeName = objDataReader["SIZE_NAME"].ToString();
                                orderedProduct.Quantity = Convert.ToInt32(objDataReader["QUANTITY"].ToString());
                                orderedProduct.Price = Convert.ToDouble(objDataReader["PRICE"].ToString());

                                objOrderedProduct.Add(orderedProduct);
                            }
                            return objOrderedProduct;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<string> ReturnRequestConfirm(string returnRequestId, string updateBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_APPROVED_RETURN_REQUEST")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_RETURN_REQUEST_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(returnRequestId) ? returnRequestId : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(updateBy) ? updateBy : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        public async Task<string> ReturnRequestReject(string returnRequestId, string updateBy)
        {
            string strMsg;

            OracleCommand objOracleCommand = new OracleCommand("PRO_REJECT_RETURN_REQUEST")
            {
                CommandType = CommandType.StoredProcedure
            };
            objOracleCommand.Parameters.Add("P_RETURN_REQUEST_ID", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(returnRequestId) ? returnRequestId : null;
            objOracleCommand.Parameters.Add("P_UPDATE_BY", OracleDbType.Varchar2, ParameterDirection.Input).Value = !string.IsNullOrWhiteSpace(updateBy) ? updateBy : null;

            objOracleCommand.Parameters.Add("P_message", OracleDbType.Varchar2, 500).Direction = ParameterDirection.Output;

            using (OracleConnection strConn = GetConnection())
            {
                try
                {
                    objOracleCommand.Connection = strConn;
                    await strConn.OpenAsync();
                    _trans = strConn.BeginTransaction();
                    await objOracleCommand.ExecuteNonQueryAsync();
                    _trans.Commit();
                    strConn.Close();

                    strMsg = objOracleCommand.Parameters["P_message"].Value.ToString();
                }
                catch (Exception ex)
                {
                    _trans.Rollback();
                    throw new Exception("Error : " + ex.Message);
                }
                finally
                {
                    strConn.Close();
                    strConn.Dispose();
                    objOracleCommand.Dispose();
                }
            }
            return strMsg;
        }

        #endregion

        #region Cancel Order

        public async Task<IEnumerable<Orders>> GetCancelList()
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                                "TO_CHAR (REJECT_DATE, 'dd/mm/yyyy hh12:mi:ssam') REJECT_DATE," +
                                "REJECT_BY," +
                                "REJECT_REMARKS," +
                                "CUSTOMER_ID," +
                                "FIRST_NAME," +
                                "LAST_NAME," +
                                "EMAIL," +
                                "PHONE," +
                                "ADDRESS_FIRST," +
                                "ADDRESS_SECOND," +
                                "CITY," +
                                "COUNTRY," +
                                "SHIPPING_COST," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS," +
                                "DISCOUNT_PERCENT," +
                                "TOTAL_AMOUNT " +
                               "FROM VEW_REJECT_ORDER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection))
                {
                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        List<Orders> objOrderList = new List<Orders>();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                Orders order = new Orders();

                                order.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                order.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                order.RejectDate = objDataReader["REJECT_DATE"].ToString();
                                order.RejectBy = objDataReader["REJECT_BY"].ToString();
                                order.RejectRemarks = objDataReader["REJECT_REMARKS"].ToString();
                                order.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                order.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                order.Email = objDataReader["EMAIL"].ToString();
                                order.PhoneNumber = objDataReader["PHONE"].ToString();
                                order.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                order.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                order.City = objDataReader["CITY"].ToString();
                                order.Country = objDataReader["COUNTRY"].ToString();

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    order.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    order.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    order.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    order.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    order.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    order.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    order.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    order.PaymentType = "bKash Mobile Banking";

                                order.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                order.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();

                                objOrderList.Add(order);
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        public async Task<Orders> GetSingleReject(string orderNumber)
        {
            const string sql = "SELECT " +
                                "ORDER_NUMBER," +
                                "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
                                "TO_CHAR (REJECT_DATE, 'dd/mm/yyyy hh12:mi:ssam') REJECT_DATE," +
                                "REJECT_BY," +
                                "REJECT_REMARKS," +
                                "CUSTOMER_ID," +
                                "FIRST_NAME," +
                                "LAST_NAME," +
                                "EMAIL," +
                                "PHONE," +
                                "ADDRESS_FIRST," +
                                "ADDRESS_SECOND," +
                                "CITY," +
                                "COUNTRY," +
                                "SHIPPING_COST," +
                                "PAYMENT_TYPE," +
                                "PAYMENT_STATUS," +
                                "DISCOUNT_PERCENT," +
                                "TOTAL_AMOUNT " +
                               "FROM VEW_REJECT_ORDER WHERE ORDER_NUMBER = :ORDER_NUMBER ";

            using (OracleConnection objConnection = GetConnection())
            {
                using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
                {
                    objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;

                    await objConnection.OpenAsync();
                    using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
                    {
                        Orders objOrderList = new Orders();

                        try
                        {
                            while (await objDataReader.ReadAsync())
                            {
                                objOrderList.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
                                objOrderList.OrderDate = objDataReader["ORDER_DATE"].ToString();
                                objOrderList.RejectDate = objDataReader["REJECT_DATE"].ToString();
                                objOrderList.RejectBy = objDataReader["REJECT_BY"].ToString();
                                objOrderList.RejectRemarks = objDataReader["REJECT_REMARKS"].ToString();
                                objOrderList.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
                                objOrderList.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
                                objOrderList.Email = objDataReader["EMAIL"].ToString();
                                objOrderList.PhoneNumber = objDataReader["PHONE"].ToString();
                                objOrderList.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
                                objOrderList.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
                                objOrderList.City = objDataReader["CITY"].ToString();
                                objOrderList.Country = objDataReader["COUNTRY"].ToString();

                                if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
                                    objOrderList.PaymentType = "Cash";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
                                    objOrderList.PaymentType = "NEXUS Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
                                    objOrderList.PaymentType = "DBBL Master Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
                                    objOrderList.PaymentType = "DBBL Visa Debit";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
                                    objOrderList.PaymentType = "Visa Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
                                    objOrderList.PaymentType = "Master Card";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
                                    objOrderList.PaymentType = "Rocket Mobile Banking";
                                else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
                                    objOrderList.PaymentType = "bKash Mobile Banking";

                                objOrderList.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
                                objOrderList.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
                            }
                            return objOrderList;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error : " + ex.Message);
                        }
                        finally
                        {
                            objDataReader.Dispose();
                            objCommand.Dispose();
                            objConnection.Dispose();
                        }
                    }
                }
            }
        }

        //public async Task<Orders> GetSingleReject(string orderNumber)
        //{
        //    string sql = "SELECT " +
        //                   "ORDER_NUMBER," +
        //                   "TO_CHAR (ORDER_DATE, 'dd/mm/yyyy hh12:mi:ssam') ORDER_DATE," +
        //                   "TO_CHAR (DELEVERY_DATE, 'dd/mm/yyyy hh12:mi:ssam') DELEVERY_DATE," +
        //                   "TO_CHAR (RETURN_DATE, 'dd/mm/yyyy hh12:mi:ssam') RETURN_DATE," +
        //                   "CUSTOMER_ID," +
        //                   "FIRST_NAME," +
        //                   "LAST_NAME," +
        //                   "EMAIL," +
        //                   "PHONE," +
        //                   "ADDRESS_FIRST," +
        //                   "ADDRESS_SECOND," +
        //                   "CITY," +
        //                   "COUNTRY," +
        //                   "SHIPPING_COST," +
        //                   "PAYMENT_TYPE," +
        //                   "PAYMENT_STATUS," +
        //                   "DISCOUNT_PERCENT," +
        //                   "TOTAL_AMOUNT " +
        //                   "FROM VEW_RETURN_PRODUCT WHERE ORDER_NUMBER = :ORDER_NUMBER ";

        //    using (OracleConnection objConnection = GetConnection())
        //    {
        //        using (OracleCommand objCommand = new OracleCommand(sql, objConnection) { CommandType = CommandType.Text })
        //        {
        //            objCommand.Parameters.Add(":ORDER_NUMBER", OracleDbType.Varchar2, ParameterDirection.Input).Value = orderNumber;


        //            await objConnection.OpenAsync();
        //            using (OracleDataReader objDataReader = (OracleDataReader)await objCommand.ExecuteReaderAsync())
        //            {
        //                Orders objOrderModel = new Orders();

        //                try
        //                {
        //                    while (await objDataReader.ReadAsync())
        //                    {
        //                        objOrderModel.OrderNumber = objDataReader["ORDER_NUMBER"].ToString();
        //                        objOrderModel.OrderDate = objDataReader["ORDER_DATE"].ToString();
        //                        objOrderModel.DeliveryDate = objDataReader["DELEVERY_DATE"].ToString();
        //                        objOrderModel.ReturnDate = objDataReader["RETURN_DATE"].ToString();
        //                        objOrderModel.CustomerId = objDataReader["CUSTOMER_ID"].ToString();
        //                        objOrderModel.CustomerName = objDataReader["FIRST_NAME"].ToString() + " " + objDataReader["LAST_NAME"].ToString();
        //                        objOrderModel.Email = objDataReader["EMAIL"].ToString();
        //                        objOrderModel.PhoneNumber = objDataReader["PHONE"].ToString();
        //                        objOrderModel.Address1 = objDataReader["ADDRESS_FIRST"].ToString();
        //                        objOrderModel.Address2 = objDataReader["ADDRESS_SECOND"].ToString();
        //                        objOrderModel.City = objDataReader["CITY"].ToString();
        //                        objOrderModel.Country = objDataReader["COUNTRY"].ToString();
        //                        objOrderModel.ShippingCost = Convert.ToInt32(objDataReader["SHIPPING_COST"].ToString());

        //                        if (objDataReader["PAYMENT_TYPE"].ToString() == "0")
        //                            objOrderModel.PaymentType = "Cash";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "1")
        //                            objOrderModel.PaymentType = "NEXUS Debit";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "2")
        //                            objOrderModel.PaymentType = "DBBL Master Debit";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "3")
        //                            objOrderModel.PaymentType = "DBBL Visa Debit";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "4")
        //                            objOrderModel.PaymentType = "Visa Card";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "5")
        //                            objOrderModel.PaymentType = "Master Card";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "6")
        //                            objOrderModel.PaymentType = "Rocket Mobile Banking";
        //                        else if (objDataReader["PAYMENT_TYPE"].ToString() == "7")
        //                            objOrderModel.PaymentType = "bKash Mobile Banking";

        //                        objOrderModel.PaymentStatus = objDataReader["PAYMENT_STATUS"].ToString();
        //                        objOrderModel.Discount = objDataReader["DISCOUNT_PERCENT"].ToString();
        //                        objOrderModel.TotalAmount = objDataReader["TOTAL_AMOUNT"].ToString();
        //                    }
        //                    return objOrderModel;
        //                }
        //                catch (Exception ex)
        //                {
        //                    throw new Exception("Error : " + ex.Message);
        //                }
        //                finally
        //                {
        //                    objDataReader.Dispose();
        //                    objCommand.Dispose();
        //                    objConnection.Dispose();
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}
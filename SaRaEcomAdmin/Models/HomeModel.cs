﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaRaEcomAdmin.Models
{
    public class HomeModel
    {
        public double TotalSale { get; set; }
        public double YearlySale { get; set; }
        public double MonthlySale { get; set; }
        public double DailySale { get; set; }

        public int TotalOrder { get; set; }
        public int TotalDelivery { get; set; }
        public int TotalReturn { get; set; }
        public int DeliveryPending { get; set; }

        public int TotalAvailableProduct { get; set; }

        public double TotalSaleMonth { get; set; }
        public double TotalRevenueMonth { get; set; }
        public int TotalOrderMonth { get; set; }

        public double TotalSaleYear { get; set; }
        public double TotalRevenueYear { get; set; }
        public int TotalOrderYear { get; set; }

        public double AllTimeSale { get; set; }
        public double AllTimeRevenue { get; set; }
        public int AllTimeOrder { get; set; }

    }
}
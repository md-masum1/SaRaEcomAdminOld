﻿using System.Threading.Tasks;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class SetupController : Controller
    {
        private readonly SetupDal _objSetupDal = new SetupDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        #region "Category"
        public async Task<ActionResult> Category(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            CategoryModel model = new CategoryModel();

            var objCategoryModel = await _objSetupDal.GetCategoryList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.CategoryList = objCategoryModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetCategory((int) id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveCategory(CategoryModel objCategoryModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objCategoryModel.UpdateBy = _strEmployeeId;
                objCategoryModel.HeadOfficeId = _strHeadOfficeId;
                objCategoryModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveCategory(objCategoryModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Category");
        }

        public async Task<ActionResult> DeleteCategory(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteCategory(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Category");
        }
        #endregion

        #region SubCategory
        public async Task<ActionResult> SubCategory(int? categoryId, int? subCategoryId)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            SubCategoryModel model = new SubCategoryModel();

            var objSubCategoryModels = await _objSetupDal.GetSubCategoryList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SubCategoryList = objSubCategoryModels;

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0)
            {
                model = await _objSetupDal.GetSubCategory((int)categoryId, (int)subCategoryId, _strHeadOfficeId, _strBranchOfficeId);
            }

            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(_objSetupDal.GetCategoryListDropdown(), "CATEGORY_ID", "CATEGORY_NAME");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSubCategory(SubCategoryModel objSubCategoryModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objSubCategoryModel.UpdateBy = _strEmployeeId;
                objSubCategoryModel.HeadOfficeId = _strHeadOfficeId;
                objSubCategoryModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveSubCategory(objSubCategoryModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("SubCategory");
        }

        public async Task<ActionResult> DeleteSubCategory(int categoryId, int subCategoryId)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteSubCategory(categoryId, subCategoryId, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("SubCategory");
        }
        #endregion

        #region SubSubCategory
        public async Task<ActionResult> SubSubCategory(int? categoryId, int? subCategoryId, int? subSubCategoryId)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            SubSubCategoryModel model = new SubSubCategoryModel();

            var objSubSubCategoryModels = await _objSetupDal.GetSubSubCategoryList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SubSubCategoryList = objSubSubCategoryModels;

            if (categoryId != null && categoryId != 0 && subCategoryId != null && subCategoryId != 0 && subSubCategoryId != null && subSubCategoryId != 0)
            {
                model = await _objSetupDal.GetSubSubCategory((int)categoryId, (int)subCategoryId, (int)subSubCategoryId, _strHeadOfficeId, _strBranchOfficeId);
                ViewBag.SubCategoryList = UtilityClass.GetSelectListByDataTable(_objSetupDal.GetSubCategoryListDropdown((int)categoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
            }

            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(_objSetupDal.GetCategoryListDropdown(), "CATEGORY_ID", "CATEGORY_NAME");

            return View(model);
        }

        public JsonResult GetSubCategoryList(int categoryId)
        {
            var list = UtilityClass.GetSelectListByDataTable(_objSetupDal.GetSubCategoryListDropdown(categoryId), "SUB_CATEGORY_ID", "SUB_CATEGORY_NAME");
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSubSubCategory(SubSubCategoryModel objSubSubCategoryModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objSubSubCategoryModel.UpdateBy = _strEmployeeId;
                objSubSubCategoryModel.HeadOfficeId = _strHeadOfficeId;
                objSubSubCategoryModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveSubSubCategory(objSubSubCategoryModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("SubSubCategory");
        }

        public async Task<ActionResult> DeleteSubSubCategory(int categoryId, int subCategoryId, int subSubCategoryId)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteSubSubCategory(categoryId, subCategoryId, subSubCategoryId, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("SubSubCategory");
        }
        #endregion

        #region "Size"
        public async Task<ActionResult> Size(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            SizeModel model = new SizeModel();

            var objSizeModel = await _objSetupDal.GetSizeList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.SizeList = objSizeModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetSize((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveSize(SizeModel objSizeModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objSizeModel.UpdateBy = _strEmployeeId;
                objSizeModel.HeadOfficeId = _strHeadOfficeId;
                objSizeModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveSize(objSizeModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Size");
        }

        public async Task<ActionResult> DeleteSize(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteSize(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Size");
        }
        #endregion

        #region "Ratio"
        public async Task<ActionResult> Ratio(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            RatioModel model = new RatioModel();

            var objRatioModel = await _objSetupDal.GetRatioList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.RatioList = objRatioModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetRatio((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveRatio(RatioModel ratioModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                ratioModel.UpdateBy = _strEmployeeId;
                ratioModel.HeadOfficeId = _strHeadOfficeId;
                ratioModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveRatio(ratioModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Ratio");
        }

        public async Task<ActionResult> DeleteRatio(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteRatio(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Ratio");
        }
        #endregion

        #region "Color"
        public async Task<ActionResult> Color(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            ColorModel model = new ColorModel();

            var objColorModel = await _objSetupDal.GetColorList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.ColorList = objColorModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetColor((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveColor(ColorModel colorModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                colorModel.UpdateBy = _strEmployeeId;
                colorModel.HeadOfficeId = _strHeadOfficeId;
                colorModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveColor(colorModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Color");
        }

        public async Task<ActionResult> DeleteColor(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteColor(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Color");
        }
        #endregion

        #region "Fabric"
        public async Task<ActionResult> Fabric(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            FabricModel model = new FabricModel();

            var objFabricModel = await _objSetupDal.GetFabricList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.FabricList = objFabricModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetFabric((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveFabric(FabricModel objFabricModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objFabricModel.UpdateBy = _strEmployeeId;
                objFabricModel.HeadOfficeId = _strHeadOfficeId;
                objFabricModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveFabric(objFabricModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Fabric");
        }

        public async Task<ActionResult> DeleteFabric(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteFabric(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Fabric");
        }
        #endregion

        #region "Measurement Unit"
        public async Task<ActionResult> Measurement(int? id)
        {
            ModelState.Clear();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            LoadSession();
            MeasurementUnitModel model = new MeasurementUnitModel();

            var objMeasurementUnitModel = await _objSetupDal.GetUnitList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.UnitList = objMeasurementUnitModel;

            if (id != null && id != 0)
            {
                model = await _objSetupDal.GetUnit((int)id, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveMeasurement(MeasurementUnitModel objMeasurementUnitModel)
        {
            LoadSession();
            if (ModelState.IsValid)
            {
                objMeasurementUnitModel.UpdateBy = _strEmployeeId;
                objMeasurementUnitModel.HeadOfficeId = _strHeadOfficeId;
                objMeasurementUnitModel.BranchOfficeId = _strBranchOfficeId;

                string strMessage = await _objSetupDal.SaveUnit(objMeasurementUnitModel);
                TempData["message"] = strMessage;
            }
            return RedirectToAction("Measurement");
        }

        public async Task<ActionResult> DeleteMeasurement(int id)
        {
            LoadSession();
            string message = await _objSetupDal.DeleteUnit(id, _strHeadOfficeId, _strBranchOfficeId);
            TempData["message"] = message;

            return RedirectToAction("Measurement");
        }
        #endregion
    }
}
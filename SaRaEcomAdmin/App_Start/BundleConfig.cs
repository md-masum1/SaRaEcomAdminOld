﻿using System.Web;
using System.Web.Optimization;

namespace SaRaEcomAdmin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
            "~/Scripts/jquery-migrate.min.js",
            "~/Scripts/jquery-ui/jquery-ui.min.js",
            "~/Content/Assets/bootstrap/js/bootstrap.min.js",
            "~/Scripts/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
            "~/Scripts/jquery-slimscroll/jquery.slimscroll.min.js",
            "~/Scripts/jquery.blockui.min.js",
            "~/Scripts/jquery.cokie.min.js",
            "~/Content/Assets/uniform/jquery.uniform.min.js",
            "~/Content/Assets/bootstrap-switch/js/bootstrap-switch.min.js",
            "~/Content/Assets/bootstrap-toastr/toastr.min.js",
            "~/Scripts/ui-toastr.js",
            "~/Scripts/jquery-confirm.js",
            "~/Scripts/wysihtml5-0.3.0.js",
            "~/Scripts/bootstrap-wysihtml5.js",
            //"~/Scripts/additional-methods.min.js",
            "~/Scripts/jquery.bootstrap.wizard.min.js",
            "~/Scripts/plugins/select2/select2.min.js",
            "~/Scripts/Plugins/DataTables/jquery.dataTables.min.js",
            "~/Scripts/Plugins/DataTables/dataTables.tableTools.min.js",
            "~/Scripts/Plugins/DataTables/dataTables.colReorder.min.js",
            "~/Scripts/Plugins/DataTables/dataTables.scroller.min.js",
            "~/Scripts/Plugins/DataTables/dataTables.bootstrap.js",
            "~/Scripts/metronic.js",
            "~/Scripts/layout.js",
            "~/Scripts/demo.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/Assets/css").Include(
                      "~/Content/Assets/font-awesome/css/font-awesome.min.css",
                      "~/Content/Assets/simple-line-icons/simple-line-icons.min.css",
                      "~/Content/Assets/bootstrap/css/bootstrap.min.css",
                      "~/Content/Assets/uniform/css/uniform.default.css",
                      "~/Content/Assets/bootstrap-switch/css/bootstrap-switch.min.css",
                      "~/Content/Assets/bootstrap-wysihtml5.css",
                      "~/Content/Assets/bootstrap-toastr/toastr.min.css",
                      "~/Content/jquery-confirm.css",
                      "~/Scripts/Plugins/select2/select2.css",
                      "~/Content/Assets/DataTables/dataTables.scroller.min.css",
                      "~/Content/Assets/DataTables/dataTables.colReorder.min.css",
                      "~/Content/Assets/DataTables/dataTables.bootstrap.css",
                      "~/Content/css/plugins.css",
                      "~/Content/css/layout.css",
                      "~/Content/css/custom.css",
                      "~/Content/site.css"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaRaEcomAdmin.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        [Required]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }
        [Required]
        [DisplayName("SKU")]
        public string Sku { get; set; }
        [Required]
        [DisplayName("Short Description")]
        public string ShortDescription { get; set; }
        [Required]
        [AllowHtml]
        [DisplayName("Long Description")]
        public string LongDescription { get; set; }

        [Required]
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        [Required]
        [DisplayName("Sub Category")]
        public int SubCategoryId { get; set; }
        [Required]
        [DisplayName("Sub Sub Category")]
        public int SubSubCategoryId { get; set; }
        [Required]
        [DisplayName("Measurement Unit")]
        public int UnitId { get; set; }
        [Required]
        [DisplayName("Fabric")]
        public int FabricId { get; set; }

        [Required]
        [DisplayName("Purchase Price")]
        public double? PurchaseAmount { get; set; }
        [Required]
        [DisplayName("Sell Price")]
        public double? SellAmount { get; set; }
        [Required]
        [DisplayName("Include Vat")]
        public bool IncludeVat { get; set; }

        [Required]
        [DisplayName("Product Image")]
        public HttpPostedFileBase ProductImage { get; set; }
        public string ImageString { get; set; }

        [Required]
        [DisplayName("Color")]
        public int[] ColorId { get; set; }

        [Required]
        public HttpPostedFileBase[] Image1 { get; set; }
        public string[] ImageString1 { get; set; }
        [Required]
        public HttpPostedFileBase[] Image2 { get; set; }
        public string[] ImageString2 { get; set; }
        [Required]
        public HttpPostedFileBase[] Image3 { get; set; }
        public string[] ImageString3 { get; set; }
        [Required]
        public HttpPostedFileBase[] Image4 { get; set; }
        public string[] ImageString4 { get; set; }

        public string UpdateBy { get; set; }
        public string HeadOfficeId { get; set; }
        public string BranchOfficeId { get; set; }
    }
}
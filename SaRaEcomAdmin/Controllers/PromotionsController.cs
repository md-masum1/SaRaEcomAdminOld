﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SaRaEcomAdmin.DAL;
using SaRaEcomAdmin.Models;
using SaRaEcomAdmin.Utility;

namespace SaRaEcomAdmin.Controllers
{
    public class PromotionsController : Controller
    {
        private  readonly PromotionsDal _promotionsDal = new PromotionsDal();

        #region "Common"

        private string _strEmployeeId = "";
        private string _strHeadOfficeId = "";
        private string _strBranchOfficeId = "";

        public void LoadSession()
        {
            var auth = Session["authentication"] as AuthModel;

            if (auth != null)
            {
                _strEmployeeId = auth.EmployeeId;
                _strHeadOfficeId = auth.HeadOfficeId;
                _strBranchOfficeId = auth.BranchOfficeId;
            }
            else
            {
                string url = Url.Action("Index", "Auth");
                if (url != null) Response.Redirect(url);
            }
        }
        #endregion

        public async Task<ActionResult> Index(int? discountId)
        {
            ModelState.Clear();
            LoadSession();
            Discount model = new Discount();

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"];
            }

            ViewBag.DiscountType = UtilityClass.DiscountType();
            ViewBag.DiscountList = await _promotionsDal.GetDiscountList(_strHeadOfficeId, _strBranchOfficeId);
            ViewBag.CategoryList = UtilityClass.GetSelectListByDataTable(await _promotionsDal.GetCategory(), "CATEGORY_ID", "CATEGORY_NAME");

            if (discountId != null && discountId != 0)
            {
                model = await _promotionsDal.GetDiscount((int)discountId, _strHeadOfficeId, _strBranchOfficeId);
            }

            return View(model);
        }

        public async Task<ActionResult> SaveDiscount(Discount objDiscount)
        {
            if (objDiscount != null)
            {
                LoadSession();
                objDiscount.UpdateBy = _strEmployeeId;
                objDiscount.HeadOfficeId = _strHeadOfficeId;
                objDiscount.BranchOfficeId = _strBranchOfficeId;

                string message = await _promotionsDal.DiscountSave(objDiscount);

                TempData["message"] = message;
            }
            
            return RedirectToAction("Index", "Promotions");
        }
    }
}